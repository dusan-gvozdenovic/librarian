/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_REF_H__
#define __LIBRARIAN_REF_H__

#include "compiler.h"

#include <stdatomic.h>
#include <stdio.h>

struct ref {
    atomic_size_t counter;
};

#define REF_INIT_FLOATING ((struct ref) { 0 })
#define REF_INIT ((struct ref) { 1 })

static inline void ref_inc(struct ref *ref) { ref->counter++; }

static inline void ref_dec(struct ref *ref) {
    if (unlikely(ref->counter == 0)) {
        fprintf(stderr,
            "Trying to decrease a reference that is already zero!.\n");
        return;
    }
    ref->counter--;
}

static inline void ref_sink(struct ref *ref) {
    if (ref->counter == 0) { ref->counter++; }
}

#define declare_ref_type(NAME, TYPE, MEMBER, CLEANUP)                          \
    attr_unused static inline void NAME##_ref(TYPE *type) {                    \
        ref_inc(&type->MEMBER);                                                \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_ref_sink(TYPE *type) {               \
        ref_sink(&type->MEMBER);                                               \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_unref(TYPE **type) {                 \
        ref_dec(&(**type).MEMBER);                                             \
        if ((**type).MEMBER.counter == 0) { CLEANUP(type); }                   \
    }

#endif

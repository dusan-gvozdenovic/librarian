/* SPDX-License-Identifier: Apache-2.0 */

#include "process.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include "core.h"

enum exec_status process_execute(struct process *proc, exit_code *ex_code) {
    pid_t pid = fork();
    if (pid == -1) { return PROCESS_FORK_FAIL; }
    if (pid == 0) {
        execve(proc->cmd, proc->argv, NULL);
        librarian_panic(EXIT_FAILURE);
    }
    int child_status;
    pid_t wpid = wait(&child_status);
    if (wpid == -1) {
        fprintf(stderr, "wait() failed in %s. Reason %s\n", __FUNCTION__,
            strerror(errno));
        return PROCESS_WAIT_FAIL;
    }
    if (WIFEXITED(child_status)) {
        exit_code child_exit_code = WEXITSTATUS(child_status);
        if (proc->on_child_exit_non_zero && child_exit_code != 0) {
            proc->on_child_exit_non_zero(proc, child_exit_code);
        }
        if (proc->on_child_exit) { proc->on_child_exit(proc, child_exit_code); }
    } else {
        return PROCESS_PREMATURE_HOOK_FAIL;
    }
    return PROCESS_OK;
}

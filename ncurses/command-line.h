/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __COMMAND_LINE_H__
#define __COMMAND_LINE_H__

#include <ncurses.h>
#include <wchar.h>

#include <librarian/types.h>

#define CMD_BUFFER_SIZE 1024

#define ESC_KEY 27
#define ENTER_KEY 10
#define BACKSPACE_KEY 127

typedef bool (*cmd_executor)(char *cmd_string);

struct command_line {
    WINDOW *window;
    char *buffer;

    /* Position in bytes */
    int pos;
    int char_pos;

    int length;
    int char_length;

    size_t capacity;

    bool has_message;

    /* Precalculated offset from prompt string (e.g. :) */
    int start_pos;

    /* Position from which a (COLS-long) part of the buffer is visible */
    int frame_pos;

    int width;

    cmd_executor executor;
};

extern struct command_line cmd_line;

void command_line_init(void);

void command_line_activate(void);

void command_line_handle_resize(void);

void command_line_loop(void);

bool command_line_handle_keys(wint_t ch);

void command_line_move_left(void);

void command_line_move_right(void);

void command_line_home(void);

void command_line_end(void);

void command_line_update(void);

void command_line_insert_char(wchar_t c);

void command_line_backspace(void);

void command_line_delete(void);

void command_line_execute(void);

void command_line_clear(void);

void command_line_clear_buffer(void);

void command_line_message(const char *message, int color_code);

void command_line_cleanup(void);

#endif

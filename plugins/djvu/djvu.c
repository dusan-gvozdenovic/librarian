/* SPDX-License-Identifier: Apache-2.0 */

#include <string.h>

#include <librarian/document.h>
#include <librarian/format.h>
#include <librarian/log.h>
#include <librarian/plugin.h>
#include <librarian/types.h>
#include <librarian/u-string.h>
#include <librarian/utils.h>
#include <librarian/xmalloc.h>

#define DJVU_MAGIC_BYTE_FORM "FORM"
#define DJVU_MAGIC_BYTE_DJVU "DJVU"
#define DJVU_MAGIC_BYTE_DJVM "DJVM"
#define DJVU_MAGIC_BYTE_PM44 "PM44"
#define DJVU_MAGIC_BYTE_BM44 "BM44"
#define DJVU_MAGIC_BYTE_DIRM "DIRM"

struct document *djvu_load_document(const char *path) {
    struct document *doc = document_new();
    u_string_set_c_str(&doc->path.str, path);
    doc->create_date = NO_DATE;
    return doc;
}

/**
 * The check is done according to
 * http://www.iana.org/assignments/media-types/image/vnd.djvu
 *
 * > 1. Magic number(s) :
 * > ASCII "FORM" at offset 4 and ASCII "DJVU" * > or "DJVM" or "PM44" or "BM44"
 * at offset 12.
 *
 * TODO: It seems that this is not completely accurate.
 * Find more accurate specs.
 */
bool djvu_file_is_djvu(const char *path, bool *success) {
    FILE *file = fopen(path, "rb");

    if (!file) {
        if (success != NULL) { *success = false; }
        return false;
    }

    int status = 0;

    status = fseek(file, 4, SEEK_CUR);

    if (status != 0) {
        fclose(file);
        if (success != NULL) { *success = false; }
        return false;
    }

    char bytes[4] = {};
    size_t length = fread(bytes, sizeof(char), 4, file);

    if (length != 4) {
        fclose(file);
        if (success != NULL) { *success = true; }
        return false;
    }

    if (strncmp(bytes, DJVU_MAGIC_BYTE_FORM, 4)) {
        fclose(file);
        if (success != NULL) { *success = true; }
        return false;
    }

    status = fseek(file, 8, SEEK_CUR);

    if (status != 0) {
        fclose(file);
        if (success != NULL) { *success = false; }
        return false;
    }

    length = fread(bytes, sizeof(char), 4, file);

    fclose(file);
    if (success) { *success = true; }

    if (length != 4) { return false; }

    return strncmp(bytes, DJVU_MAGIC_BYTE_DJVU, 4) == 0 ||
        strncmp(bytes, DJVU_MAGIC_BYTE_DJVM, 4) == 0 ||
        strncmp(bytes, DJVU_MAGIC_BYTE_PM44, 4) == 0 ||
        strncmp(bytes, DJVU_MAGIC_BYTE_BM44, 4) == 0 ||
        strncmp(bytes, DJVU_MAGIC_BYTE_DIRM, 4) == 0;
}

// clang-format off
struct format_provider djvu = {
    .check = djvu_file_is_djvu,
    .load = djvu_load_document
};
// clang-format on

exit_status djvu_load(struct librarian *librarian) {
    format_registry_add(&librarian->format_registry, &djvu);
    return 0;
}

exit_status djvu_unload(struct librarian *librarian) {
    (void) librarian;
    return 0;
}

// const struct plugin_dependency djvu_dependency_pdf = {
//     .name = "pdf",
//     // .max_version = lib_version_from_parts(0,0,1)
// };

// const struct plugin_dependency *djvu_plugin_dependencies[] = {
//     &djvu_dependency_pdf, NULL
// };

// clang-format off
struct plugin decl = {
    .name = "djvu plugin",
    .maintainer = "Dušan Gvozdenović",
    .email = "dusan.gvozdenovic.99@gmail.com",
    .version = lib_version_from_parts(0, 0, 2),
    .load = djvu_load,
    .unload = djvu_unload,
    .dependencies = NULL,
    // .dependencies = djvu_plugin_dependencies,
};
// clang-format on

plugin_export(decl);

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __EVENT_WRAPPER_H__
#define __EVENT_WRAPPER_H__

#include <librarian/types.h>
#include <librarian/utf8.h>

struct ui_events {
    void (*loop_func)(utf8_char ch);
    void (*paint_func)(void);
    void (*resize_func)(void);
    void (*interpret_number)(int64_t n);
};

bool ui_events_equal(const struct ui_events *ue1, const struct ui_events *ue2);

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#include "plugin-registry.h"
#include "config.h"
#include "core.h"
#include "file.h"
#include "hash.h"
#include "hashmap.h"
#include "hashset.h"
#include "limits.h"
#include "localization.h"
#include "log.h"
#include "option.h"
#include "plugin.h"
#include "ref.h"
#include "types.h"
#include "utils.h"
#include "vector.h"
#include "version.h"
#include "xmalloc.h"

#include <dlfcn.h>
#include <stdarg.h>
#include <string.h>

#ifndef LIBRARIAN_SHARED_LIBRARY_SUFFIX
#error LIBRARIAN_SHARED_LIBRARY_SUFFIX is not defined!
#endif

struct plugin_entry {
    struct plugin_registry *plugin_registry;
    struct ref ref;
    void *handle;
    struct u_string identifier;
    struct plugin *plugin;
    struct hashset loops; // <char *>
    bool loaded;
};

declare_hashset_type(plugin_entry, struct plugin_entry *);

declare_hashset_type(string, char *);

declare_vector_type_val(plugin_entry, struct plugin_entry *);

static void _plugin_entry_release(struct plugin_entry **entry);

declare_ref_type(plugin_entry, struct plugin_entry, ref, _plugin_entry_release);

static inline struct hashset_iterator
_plugin_entry_hashset_find_by_id(struct hashset *set, const char *id) {
    struct plugin_entry dummy = { .identifier = { .c_str = (char *) id } };
    struct plugin_entry *indirect = &dummy;
    return plugin_entry_hashset_find(set, &indirect);
}

static inline bool
_plugin_entry_hashset_contains_id(struct hashset *set, const char *id) {
    return !hashset_iterator_is_end(set,
        _plugin_entry_hashset_find_by_id(set, id));
}

static void _plugin_entry_destroy(struct plugin_entry **entry) {
    struct librarian *lbr = librarian_from_component((**entry).plugin_registry);
    struct log *log = &lbr->log;

    if ((**entry).loaded && (**entry).plugin->unload) {
        (**entry).plugin->unload(lbr);
    }

    if ((**entry).handle) {
        int status = dlclose((**entry).handle);
        if (status != 0) {
            log_warn(log,
                _("Failed to close library handle of plugin \"%s\". "
                  "Reason: %s"),
                (**entry).plugin->name, dlerror());
        }
    }

    u_string_cleanup(&(**entry).identifier);

    struct hashset_iterator it;
    hashset_for_each ((**entry).loops, it) {
        char **c_str = string_hashset_iterator_element(&(**entry).loops, it);
        xfree(*c_str);
    }
    hashset_cleanup(&(**entry).loops);

    xfree_ptr(entry);
}

static void _plugin_entry_release(struct plugin_entry **entry) {
    const struct plugin_dependency **it = NULL;
    const struct plugin_dependency **deps = (*entry)->plugin->dependencies;

    struct hashset *loaded_plugins = &(**entry).plugin_registry->loaded_plugins;

    for (it = deps; it != NULL && (*it) != NULL; it++) {
        struct hashset_iterator e_it =
            _plugin_entry_hashset_find_by_id(loaded_plugins, (*it)->identifier);

        // Already released or a loop.
        if (hashset_iterator_is_end(loaded_plugins, e_it) ||
            string_hashset_contains(&(**entry).loops,
                (char **) &(**it).identifier)) {
            continue;
        }

        struct plugin_entry **e =
            plugin_entry_hashset_iterator_element(loaded_plugins, e_it);
        plugin_entry_unref(e);
    }

    _plugin_entry_destroy(entry);

    // *entry is invalid here but removing the entry through the calculated
    // iterator should be fine.
    (void) hashset_remove(loaded_plugins,
        hashset_iterator_of(loaded_plugins, (uint8_t *) entry));
}

/* --------------------------- Private functions ---------------------------- */

static hash _plugin_entry_hash(const void *ptr) {
    return hash_str_djb2((*(struct plugin_entry **) ptr)->identifier.c_str);
}

static bool _plugin_entry_equal(const void *lhs, const void *rhs) {
    return u_string_compare(&(*(struct plugin_entry **) lhs)->identifier,
               &(*(struct plugin_entry **) rhs)->identifier) == 0;
}

static inline bool _plugin_satisfies_min_version(const struct plugin *plugin,
    lib_version version) {
    return version == LIBRARIAN_LIB_VERSION_UNSPECIFIED ||
        plugin->version >= version;
}

static inline bool _plugin_satisfies_max_version(const struct plugin *plugin,
    lib_version version) {
    return version == LIBRARIAN_LIB_VERSION_UNSPECIFIED ||
        plugin->version <= version;
}

static inline bool _plugin_satisfies_api_version(const struct plugin *plugin) {
    return plugin->api_version == LIBRARIAN_LIB_VERSION_UNSPECIFIED ||
        plugin->api_version >= LIBRARIAN_VERSION_MAJOR;
}

static bool _plugin_identifier_from_path(const struct path *path,
    struct u_string *identifier) {
    const char *basename;
    path_basename(path, &basename);

    if (strncmp(basename, "lib", 3) != 0) { return false; }

    const char *start = basename + 3, *end = start, *iter = start;

    while ((iter = strstr(iter, LIBRARIAN_SHARED_LIBRARY_SUFFIX)) != NULL) {
        end = iter;
        iter += c_str_static_length(LIBRARIAN_SHARED_LIBRARY_SUFFIX);
    }

    if (end == start) { return false; }

    return u_string_set_c_str_n(identifier, start, end - start);
}

// TODO: Need to recurse here instead.
static bool _plugin_find_path_from_runtime_path_list(
    struct plugin_registry *plugin_registry, struct path *path,
    const char *name) {
    char *iter = NULL;
    struct path current = PATH_INIT;
    bool found = false;

    struct librarian *librarian = librarian_from_component(plugin_registry);
    struct config *cfg = &librarian->config;

    c_str_list_for_each (iter, ":", option_get(cfg->runtime_path).c_str) {
        path_set_c_str(&current, iter, strnlen(iter, PATH_MAX));

        if (!path_expand(&current) || !path_real(&current) ||
            !path_is_directory(&current)) {
            continue;
        }

        path_append(&current, name);

        found = path_is_file(&current);
        if (found) { break; }
    }

    if (found) { path_copy(path, &current); }
    path_cleanup(&current);
    return found;
}

static bool _plugin_find_path(struct plugin_registry *plugin_registry,
    struct path *path, const char *name) {
    bool success = false;
    const size_t name_len = strlen(name);
    const char * const dll_ext = LIBRARIAN_SHARED_LIBRARY_SUFFIX;
    const size_t dll_ext_len =
        c_str_static_length(LIBRARIAN_SHARED_LIBRARY_SUFFIX);

    if (name[0] == LIBRARIAN_SYSTEM_PATH_SEPARATOR[0]) {
        struct path tmp = PATH_INIT;
        path_set_c_str(&tmp, name, name_len);
        success = path_is_file(&tmp);
        if (success) { path_copy(path, &tmp); }
        path_cleanup(&tmp);
    } else if (c_str_has_suffix(name, name_len, dll_ext, dll_ext_len)) {
        success = _plugin_find_path_from_runtime_path_list(plugin_registry,
            path, name);
    } else {
        struct u_string bname = U_STRING_INIT;
        const char *libfmt = "lib%s" LIBRARIAN_SHARED_LIBRARY_SUFFIX;
        (void) u_string_printf(&bname, libfmt, name);
        success = _plugin_find_path_from_runtime_path_list(plugin_registry,
            path, bname.c_str);
        u_string_cleanup(&bname);
    }

    return success;
}

static inline void
_set_details(struct plugin_load_details *details, uint32_t return_code,
    enum plugin_load_status status, const char *format, ...) {
    if (!details) { return; }

    details->return_code = return_code;
    details->status = status;
    details->message = U_STRING_INIT;

    va_list ap;
    va_start(ap, format);
    (void) u_string_vprintf(&details->message, format, ap);
    va_end(ap);
}

static struct plugin_entry *
_plugin_open(struct plugin_registry *plugin_registry, const struct path *path,
    struct plugin_load_details *details) {
    struct plugin_entry *e = xnew(struct plugin_entry);
    e->plugin_registry = plugin_registry;

    e->identifier = U_STRING_INIT;
    if (!_plugin_identifier_from_path(path, &e->identifier)) {
        _set_details(details, 0, PLUGIN_LOAD_ERROR,
            _("Failed to parse plugin identifier. Invalid library name: "
              "\"%s\"."),
            path->str.c_str);
        goto error;
    }

    e->handle = dlopen(path->str.c_str, RTLD_GLOBAL | RTLD_LAZY);

    if (!e->handle) {
        _set_details(details, 0, PLUGIN_LOAD_FAILED_LOAD,
            _("Failed to open library: %s"), path->str.c_str);
        goto error;
    }

    struct plugin **plugin = dlsym(e->handle, "__librarian_plugin_decl");
    if (!plugin) {
        _set_details(details, 0, PLUGIN_LOAD_INVALID_INTERFACE,
            _("Could not find symbol __librarian_plugin_decl. Reason: %s."),
            path->str.c_str, dlerror());
        goto error;
    }

    e->plugin = *plugin;

    if (!_plugin_satisfies_api_version(e->plugin)) {
        _set_details(details, 0, PLUGIN_LOAD_API_VERSION_LOW,
            _("\"%s\" requires API version " LIB_VERSION_NUMBER_FORMAT_SPECIFIER
              " greater than current " LIB_VERSION_NUMBER_FORMAT_SPECIFIER "."),
            e->plugin->name, e->plugin->api_version, LIBRARIAN_VERSION_MAJOR);
        goto error;
    }

    if (!e->plugin->load) {
        _set_details(details, 0, PLUGIN_LOAD_INVALID_INTERFACE,
            _("plugin does not implement load() function."), e->plugin->name);
        goto error;
    }

    e->ref = REF_INIT;
    string_hashset_init(&e->loops, 3);
    e->loops.parent.hash = hash_key_c_str;
    e->loops.parent.equal = hash_key_c_str_equal;
    e->loaded = false;

    return e;

error:
    if (e->handle) { dlclose(e->handle); }
    xfree(e);
    return NULL;
}

/**
 * Opens all new plugins across the dependency graph and check version
 * constraints. If any of the constraints are not met abort.
 */
static bool _plugin_entry_build_subtree_and_check_dependencies(
    struct plugin_entry *root, struct hashset *visited, struct vector *opened,
    struct plugin_load_details *details) {

    bool success = true;

    struct plugin_registry *pr = root->plugin_registry;
    struct hashset *loaded_plugins = &pr->loaded_plugins;
    struct log *log = &librarian_from_component(pr)->log;

    // Mark plugin as visited
    plugin_entry_hashset_insert(visited, &root);

    const struct plugin_dependency **dep_it = root->plugin->dependencies;
    struct path dep_path = PATH_INIT;

    if (!root->plugin->dependencies) { goto cleanup; }

    // Iterate over dependencies, check if they are valid and open new ones.
    for (; (*dep_it) != NULL; ++dep_it) {

        // 1. If dependency is in the visited set, report and record loop
        struct hashset_iterator circ_it =
            _plugin_entry_hashset_find_by_id(visited, (**dep_it).identifier);

        if (!hashset_iterator_is_end(visited, circ_it)) {
            log_warn(log,
                _("Circular reference detected between plugins "
                  "\"%s\" and \"%s\"."),
                root->identifier.c_str, (**dep_it).identifier);
            char *key = xstrdup((const char *) (**dep_it).identifier);
            string_hashset_insert(&root->loops, &key);
            continue;
        }

        struct plugin_entry *dep_entry;
        struct hashset_iterator it = _plugin_entry_hashset_find_by_id(
            loaded_plugins, (**dep_it).identifier);

        bool already_loaded = false;

        // 2. Get the dependency's plugin_entry.
        if (!hashset_iterator_is_end(loaded_plugins, it)) {
            // The dependency is already loaded.
            already_loaded = true;
            dep_entry =
                *plugin_entry_hashset_iterator_element(loaded_plugins, it);
        } else {
            // The dependency is a new one - find it and open its plugin.
            if (!_plugin_find_path(pr, &dep_path, (**dep_it).identifier)) {
                _set_details(details, 0, PLUGIN_LOAD_UNSATISFIABLE_DEPENDENCY,
                    _("Could not find the dependency \"%s\"."),
                    (**dep_it).identifier);
                success = false;
                goto cleanup;
            }

            dep_entry = _plugin_open(pr, &dep_path, details);

            if (!dep_entry) {
                success = false;
                goto cleanup;
            }

            plugin_entry_vector_push_back_val(opened, dep_entry);
        }

        char found_version[LIB_VERSION_STRING_LENGTH_MAX];
        char expected_version[LIB_VERSION_STRING_LENGTH_MAX];

        if (!_plugin_satisfies_min_version(dep_entry->plugin,
                (**dep_it).min_version)) {
            lib_version_to_string(dep_entry->plugin->version, found_version);
            lib_version_to_string((**dep_it).min_version, expected_version);

            _set_details(details, 0, PLUGIN_LOAD_DEPENDENCY_VERSION_LOW,
                _("Dependency \"%s\" (%s) is too old. Expected version "
                  "%s."),
                dep_entry->plugin->name, found_version, expected_version);

            success = false;
            goto cleanup;
        }

        if (!_plugin_satisfies_max_version(dep_entry->plugin,
                (**dep_it).max_version)) {
            lib_version_to_string(dep_entry->plugin->version, found_version);
            lib_version_to_string((**dep_it).max_version, expected_version);

            _set_details(details, 0, PLUGIN_LOAD_DEPENDENCY_VERSION_HIGH,
                _("Dependency \"%s\" (%s) is too recent. Expected version "
                  "%s."),
                dep_entry->plugin->name, found_version, expected_version);
            success = false;
            goto cleanup;
        }

        if (!already_loaded) {
            if (!_plugin_entry_build_subtree_and_check_dependencies(dep_entry,
                    visited, opened, details)) {
                success = false;
                goto cleanup;
            }
        }
    }

cleanup:
    path_cleanup(&dep_path);

    return success;
}

/**
 * Traverses the reference tree to:
 * 1. call plugin->load() bottom-up
 * 2. Correct the reference counts of already loaded plugins.
 */
static bool _plugin_entry_load_subtree(struct plugin_entry *root,
    struct hashset *visited, struct plugin_load_details *details) {

    struct hashset *loaded_plugins = &root->plugin_registry->loaded_plugins;

    if (root->plugin->dependencies != NULL) {
        const struct plugin_dependency **dep_it = root->plugin->dependencies;

        for (; (*dep_it) != NULL; ++dep_it) {
            if (string_hashset_contains(&root->loops,
                    (char **) &(**dep_it).identifier)) {
                continue;
            }

            struct hashset_iterator h_it = _plugin_entry_hashset_find_by_id(
                loaded_plugins, (**dep_it).identifier);

            struct plugin_entry **entry;
            // Every dependency that is not a loop is either visited or in
            // loaded_plugins.
            if (!hashset_iterator_is_end(loaded_plugins, h_it)) {
                entry =
                    plugin_entry_hashset_iterator_element(loaded_plugins, h_it);
                plugin_entry_ref(*entry);
                continue;
            }

            entry = plugin_entry_hashset_iterator_element(visited,
                _plugin_entry_hashset_find_by_id(visited,
                    (**dep_it).identifier));

            if (!_plugin_entry_load_subtree(*entry, visited, details)) {
                return false;
            }
        }
    }

    struct librarian *lbr = librarian_from_component(root->plugin_registry);

    exit_status load_status = root->plugin->load(lbr);

    if (details != NULL) { details->return_code = load_status; }
    if (load_status != 0) {
        _set_details(details, load_status, PLUGIN_LOAD_FAILED_LOAD,
            _("Plugin's load() function returned non-zero exit code (%d): "
              "%s."),
            load_status, root->identifier.c_str);

        return false;
    }

    root->loaded = true;
    plugin_entry_hashset_insert(loaded_plugins, &root);

    return true;
}

/* ----------------------- Plugin registry interface ------------------------ */

void plugin_load_details_init(struct plugin_load_details *details) {
    details->status = 0;
    details->return_code = 0;
    details->message = U_STRING_INIT;
}

void plugin_load_details_cleanup(struct plugin_load_details *details) {
    u_string_cleanup(&details->message);
}

struct plugin_registry *plugin_registry_new(void) {
    struct plugin_registry *pr = xnew(struct plugin_registry);
    memset(&pr->loaded_plugins, 0, sizeof(struct hashset));
    plugin_entry_hashset_init(&pr->loaded_plugins, 8);
    pr->loaded_plugins.parent.hash = _plugin_entry_hash;
    pr->loaded_plugins.parent.equal = _plugin_entry_equal;
    return pr;
}

void plugin_registry_destroy(struct plugin_registry **registry) {
    plugin_registry_unload_all(*registry);
    hashset_cleanup(&(**registry).loaded_plugins);
}

void plugin_registry_init(struct plugin_registry *plugin_registry) {
    plugin_entry_hashset_init(&plugin_registry->loaded_plugins, 8);
    plugin_registry->loaded_plugins.parent.hash = _plugin_entry_hash;
    plugin_registry->loaded_plugins.parent.equal = _plugin_entry_equal;
}

void plugin_registry_cleanup(struct plugin_registry *plugin_registry) {
    plugin_registry_unload_all(plugin_registry);
    hashset_cleanup(&plugin_registry->loaded_plugins);
}

bool plugin_registry_load(struct plugin_registry *plugin_registry,
    const char *c_str, struct plugin_load_details *details) {
    bool success = false;

    struct path root = PATH_INIT;
    struct u_string id = U_STRING_INIT;

    success = _plugin_find_path(plugin_registry, &root, c_str);
    if (!success) {
        _set_details(details, 0, PLUGIN_LOAD_ERROR,
            _("Failed to find plugin \"%s\"."), c_str);
        goto exit;
    }

    if (!_plugin_identifier_from_path(&root, &id)) {
        _set_details(details, 0, PLUGIN_LOAD_ERROR,
            _("Failed to parse plugin identifier. Invalid library name: "
              "\"%s\"."),
            root.str.c_str);
        success = false;
        goto exit;
    }

    // Skip everything if the plugin is already loaded
    if (!hashset_iterator_is_end(&plugin_registry->loaded_plugins,
            _plugin_entry_hashset_find_by_id(&plugin_registry->loaded_plugins,
                id.c_str))) {
        _set_details(details, 0, PLUGIN_LOAD_ALREADY_LOADED,
            _("Plugin \"%s\" is already loaded."), id.c_str);
        success = true;
        goto exit;
    }

    struct hashset visited; // <struct plugin_entry *>

    // Used to call dlclose() on opened plugins in case of an error.
    struct vector opened; // <struct plugin_entry *>

    plugin_entry_hashset_init(&visited, 10);
    visited.parent.hash = _plugin_entry_hash;
    visited.parent.equal = _plugin_entry_equal;
    plugin_entry_vector_init_default(&opened, 10);

    struct plugin_entry *root_entry =
        _plugin_open(plugin_registry, &root, details);

    if (!root_entry) {
        success = false;
        goto exit;
    }

    plugin_entry_vector_push_back_val(&opened, root_entry);

    success = _plugin_entry_build_subtree_and_check_dependencies(root_entry,
        &visited, &opened, details);
    if (!success) { goto cleanup; }

    success = _plugin_entry_load_subtree(root_entry, &visited, details);
    if (!success) { goto cleanup; }

    if (details != NULL) { details->status = PLUGIN_LOAD_OK; }

cleanup:
    if (!success) {
        struct plugin_entry **it;
        vector_for_each_entry_reverse(it, struct plugin_entry *, opened) {
            if (_plugin_entry_hashset_contains_id(
                    &plugin_registry->loaded_plugins,
                    (**it).identifier.c_str)) {
                plugin_entry_unref(it);
            } else { // One of ours, we can directly call destroy
                _plugin_entry_destroy(it);
            }
        }
    }

    hashset_cleanup(&visited);
    vector_cleanup(&opened);
exit:
    path_cleanup(&root);
    u_string_cleanup(&id);
    return success;
}

enum plugin_unload_status plugin_registry_unload(
    struct plugin_registry *plugin_registry, const char *identifier) {

    struct hashset_iterator it = _plugin_entry_hashset_find_by_id(
        &plugin_registry->loaded_plugins, identifier);

    if (hashset_iterator_is_end(&plugin_registry->loaded_plugins, it)) {
        return PLUGIN_UNLOAD_NOT_FOUND;
    }

    struct plugin_entry **entry = plugin_entry_hashset_iterator_element(
        &plugin_registry->loaded_plugins, it);

    if ((**entry).ref.counter > 1) { return PLUGIN_UNLOAD_REFERENCED; }

    const struct plugin_dependency **dep_it = (**entry).plugin->dependencies;

    if (dep_it) {
        for (; *dep_it != NULL; dep_it++) {
            if (string_hashset_contains(&(**entry).loops,
                    (char **) (**dep_it).identifier)) {
                continue;
            }

            struct plugin_entry **dep = plugin_entry_hashset_iterator_element(
                &plugin_registry->loaded_plugins,
                _plugin_entry_hashset_find_by_id(
                    &plugin_registry->loaded_plugins, (**dep_it).identifier));

            plugin_entry_unref(dep);
        }
    }

    plugin_entry_unref(entry);

    return PLUGIN_UNLOAD_OK;
}

// Will not necessarily release all plugins starting from root but will release
// all plugin subtrees in a DFS manner.
void plugin_registry_unload_all(struct plugin_registry *plugin_registry) {
    struct hashset_iterator it;

    hashset_for_each (plugin_registry->loaded_plugins, it) {
        struct plugin_entry **entry = plugin_entry_hashset_iterator_element(
            &plugin_registry->loaded_plugins, it);
        // This may invalidate the iterator but that should be fine as we are
        // requesting the next one afterwards.
        plugin_entry_unref(entry);
    }
}

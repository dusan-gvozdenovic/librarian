/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>
#include <stdlib.h>

#include <librarian/core.h>
#include <librarian/observer.h>

#include "librarian.h"

struct librarian librarian;

int main(int argc, char *argv[]) {
    librarian_init(&librarian);
    init_main_window();

    // TODO: Implement graceful exit here
    for (;;) { librarian_ncurses.mode_func(); }

    librarian_exit(&librarian, EXIT_SUCCESS);
}

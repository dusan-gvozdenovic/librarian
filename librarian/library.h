/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_LIBRARY_H__
#define __LIBRARIAN_LIBRARY_H__

#include "document.h"
#include "file.h"
#include "observer.h"
#include "rbtree.h"
#include "types.h"
#include "vector.h"
#include "version.h"

struct library {
    struct path path;
    struct rbtree lib_tree;
    struct subject subject;
    struct subject cleared;
    lib_version version;
    int size;
};

struct library_args {
    enum {
        LIBRARY_ARGS_OP_ADD,
        LIBRARY_ARGS_OP_REMOVE,
    } operation_type;
    struct vector docs;
};

/** @related library
 * @{
 */

void library_init(struct library *library);

void library_load(struct library *library);

/**
 * Tests if the library contains a document.
 */
bool library_contains_document(struct library *library, struct document *doc);

/**
 * Adds the document into the library.
 */
void library_add(struct library *library, struct document *doc);

/**
 * Recursively adds new documents from a directory.
 *
 * This functions walks the directory tree, adds new documents and writes their
 * paths to the library file.
 *
 * @param dir Directory path.
 * @remark Document will be added to the library only if its path is unique.
 */
void library_add_from_dir(struct library *library, const struct path *dir);

void library_remove(struct library *library, struct document *doc);

void library_remove_many(struct library *library, struct vector *docs);

/**
 * Saves the library.
 *
 * This function performs a level-order traversal of the library and serializes
 * documents. This way when the library is loaded no rotations will be required.
 */
void library_save(struct library *library);

/**
 * Clears the library and saves the library file.
 *
 * @remark The library.cleared observers will be notified afterwards.
 */
void library_clear(struct library *library);

/**
 * @brief Traverses the library and invokes the callback on any document that
 * satisfies the predicate.
 *
 * @param predicate a predicate on a document.
 * @param callback callback to be invoked.
 * @param args context to be passed to the callback (up to user to define).
 */
void library_filter(struct library *library,
    bool (*predicate)(struct document *doc),
    void (*callback)(struct document *doc, void *args), void *args);

/**
 * Clears the library and releases all the references to the contained
 * documents.
 *
 * @remark Observers of library.cleared will be notified afterwards.
 */
void library_cleanup(struct library *library);

/** @} */

#endif

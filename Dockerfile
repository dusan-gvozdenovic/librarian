FROM ubuntu:jammy

LABEL AUTHOR "Dušan Gvozdenović"

RUN apt-get update -qq &&                                                      \
       apt-get install -y -qq                                                  \
       cmake ninja-build pkg-config gettext libc6-dev                          \
       libncursesw5 libncursesw5-dev                                           \
       libexempi-dev libexempi8                                                \
       libcunit1 libcunit1-dev                                                 \
       llvm clang-tools iwyu                                                   \
       git tmux imagemagick

RUN git clone --branch 3.2.7 https://gitlab.com/cunity/cunit.git &&            \
    cd cunit && mkdir build && cd build &&                                     \
    cmake -G Ninja -DCMAKE_INSTALL_PREFIX=/usr .. &&                           \
    ninja -j$(nproc) &&                                                        \
    ninja install

/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>
#include <string.h>

#include <librarian/document.h>
#include <librarian/file.h>
#include <librarian/library.h>
#include <librarian/limits.h>
#include <librarian/types.h>
#include <librarian/u-string.h>
#include <librarian/utils.h>

#include "command-line.h"
#include "librarian.h" // For struct librarian librarian
#include "list-box.h"
#include "list-store.h"
#include "list-view.h"
#include "search-mode.h"

static bool search_backwards = false;

void search_mode_activate(void) {
    command_line_activate();
    wprintw(cmd_line.window, "%s", search_backwards ? "?" : "/");
    cmd_line.start_pos = 1;
    cmd_line.executor = search_mode_search;
}

static char search_query[PATH_MAX];

static bool search_predicate(struct document *doc) {
    return (doc->title.c_str && !!strstr(doc->title.c_str, search_query)) ||
        (doc->author.c_str && !!strstr(doc->author.c_str, search_query)) ||
        (doc->path.str.c_str && !!strstr(doc->path.str.c_str, search_query));
}

// TODO: Exact clone as in list_view.c
static void _populate_list_view_callback(struct document *doc, void *args) {
    list_store_add(args, list_view_document_to_list_item(doc));
}

// XXX: We should not touch the list_view from here but it should work
// temporarily.
bool search_mode_search(char *cmd_buffer) {
    struct list_store *store = &list_view.document_list_box.store;
    list_store_clear(store);

    bool (*predicate)(struct document *doc) = NULL;

    if (!c_str_null_or_blank(cmd_buffer)) {
        strncpy(search_query, cmd_buffer, PATH_MAX - 1);
        predicate = search_predicate;
    }

    library_filter(&librarian.library, predicate, _populate_list_view_callback,
        store);
    list_box_top(&list_view.document_list_box);
    list_view_paint();

    return true;
}

void search_mode_set_search_backwards(bool backwards) {
    search_backwards = backwards;
}

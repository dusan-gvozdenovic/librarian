/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>
#include <stdio.h>

#include <librarian/localization.h>
#include <librarian/types.h>
#include <librarian/utf8.h>
#include <librarian/version.h>

#include "librarian.h"
#include "normal-mode.h"
#include "welcome.h"

#define WELCOME_LINES 8

#define COMMAND_SPACING 2

#define X_POS(LEN) (COLS / 2 - (LEN) / 2)

#define Y_POS(K) (LINES / 2 - WELCOME_LINES / 2 + (K))

#define INC_POS (welcome.pos++)

// clang-format off
struct welcome welcome = {
    .ui_events = {
        .paint_func = welcome_paint,
        .resize_func = welcome_paint,
        .interpret_number = NULL
    },
    .pos = 0,
    .command_line_width = 0
};
// clang-format on

void welcome_write_empty_line(void) { INC_POS; }

void welcome_write_line(char *str) {
    mvwprintw(normal_mode.window, Y_POS(INC_POS), X_POS(u8_strlen(str)), "%s",
        str);
}

void welcome_write_line_bold(char *str) {
    wattron(normal_mode.window, WA_BOLD);
    welcome_write_line(str);
    wattroff(normal_mode.window, WA_BOLD);
}

void welcome_writeln_command(char *cmd, char *desc) {
    char *type_str = _("type");
    size_t length = u8_strlen(type_str) + welcome.command_line_width;
    wmove(normal_mode.window, Y_POS(INC_POS), X_POS(length));
    wprintw(normal_mode.window, "%s%*s", type_str, COMMAND_SPACING, "");

    int32_t width = welcome.command_line_width;
    char *curr = cmd;

    while (*curr) {
        if (*curr == '<') {
            wattron(normal_mode.window, COLOR_PAIR(TEXT_PRIMARY_COLOR));
            do {
                wprintw(normal_mode.window, "%c", *curr);
                curr++;
            } while (*curr && *curr != '>');
            wprintw(normal_mode.window, ">");

            wattroff(normal_mode.window, COLOR_PAIR(TEXT_PRIMARY_COLOR));
        } else {
            wprintw(normal_mode.window, "%c", *curr);
        }
        curr++;
    }

    width -= curr - cmd + COMMAND_SPACING + u8_strlen(desc);

    wprintw(normal_mode.window, "%*s", width, "");

    wprintw(normal_mode.window, "%s", desc);
}

void welcome_init(void) {
    welcome.commands[0][0] = ":q<Enter>";
    welcome.commands[0][1] = _("to quit");
    welcome.commands[1][0] = ":help<Enter>";
    welcome.commands[1][1] = _("for help");
    welcome.commands[2][0] = ":add {path}<Enter>";
    welcome.commands[2][1] = _("to add documents to the library");

#define CMD_LENGTH(X)                                                          \
    (COMMAND_SPACING + u8_strlen((X)[0]) + COMMAND_SPACING + u8_strlen((X)[1]))

    welcome.command_line_width = CMD_LENGTH(welcome.commands[0]);

    for (int32_t i = 1; i < WELCOME_N_COMMANDS; i++) {
        int32_t current = CMD_LENGTH(welcome.commands[i]);
        if (current > welcome.command_line_width) {
            welcome.command_line_width = current;
        }
    }

#undef CMD_LENGTH
}

void welcome_paint(void) {
    welcome.pos = 0;

    wattron(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));

    for (int32_t i = 0; i < COLS; i++) { wprintw(normal_mode.window, " "); }

    wattroff(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));

    welcome_write_line_bold(_("Librarian"));
    welcome_write_empty_line();
    char version[50];
    sprintf(version, "%s %s", _("version"), LIBRARIAN_VERSION_NAME);
    welcome_write_line(version);
    welcome_write_line(_("by Dusan Gvozdenovic"));
    welcome_write_empty_line();

    for (int32_t i = 0; i < WELCOME_N_COMMANDS; i++) {
        welcome_writeln_command(welcome.commands[i][0], welcome.commands[i][1]);
    }

    wattron(normal_mode.window, COLOR_PAIR(LINE_COLOR));

    wmove(normal_mode.window, LINES - 2, 0);
    whline(normal_mode.window, ACS_HLINE, COLS);

    wattroff(normal_mode.window, COLOR_PAIR(LINE_COLOR));
}

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_VECTOR_H__
#define __LIBRARIAN_VECTOR_H__

#include <assert.h>
#include <memory.h>
#include <stdlib.h>

#include "compiler.h"
#include "types.h"
#include "xmalloc.h"

struct vector {
    size_t size;
    size_t element_size;
    size_t capacity;
    size_t threshold;
    float scale_factor;
    void *data;
};

#define VECTOR_DEFAULT_THRESHOLD(CAPACITY) ((size_t) (0.8 * (CAPACITY)))
#define VECTOR_DEFAULT_SCALE_FACTOR 0.41

/**
 * Initializes a vector structure.
 *
 * @param vec Vector container.
 * @param element_size Size of the vector elements.
 * @param capacity Desired capacity.
 * @param threshold Initial threshold at which a vector tries to resize.
 * @param scale_factor ratio of current capacity for which a vector will be
 * increased when resized.
 * @related vector
 */
static inline void vector_init(struct vector *vec, size_t element_size,
    size_t capacity, size_t threshold, float scale_factor) {
    // TODO: Revisit the consequences of this.
    // assert(threshold > 0 && threshold <= capacity);
    assert(0.0 <= scale_factor);
    vec->size = 0;
    vec->element_size = element_size;
    vec->capacity = capacity;
    vec->threshold = threshold;
    vec->scale_factor = scale_factor;
    vec->data = xmalloc(capacity * vec->element_size);
    (void) memset(vec->data, 0, capacity * vec->element_size);
}

/**
 * Initializes a vector structure with default threshold and scale factor.
 * @see vector_init
 *
 * @param vec Vector container.
 * @param element_size Size of the vector elements.
 * @param capacity Desired capacity.
 * @related vector
 */
static inline void
vector_init_default(struct vector *vec, size_t element_size, size_t capacity) {
    vector_init(vec, element_size, capacity, VECTOR_DEFAULT_THRESHOLD(capacity),
        VECTOR_DEFAULT_SCALE_FACTOR);
}

/**
 * releases the underlying buffer.
 *
 * @related vector
 */
static inline void vector_cleanup(struct vector *vec) { xfree_ptr(&vec->data); }

/**
 * clear a vector container.
 *
 * @memberof vector
 */
static inline void vector_clear(struct vector *vec) {
    vec->size = 0;
    (void) memset(vec->data, 0, vec->capacity * vec->element_size);
}

/**
 * Tests if a vector is empty.
 * @memberof vector
 */
static inline bool vector_empty(struct vector *vec) { return vec->size == 0; }

/**
 * Returns the element at nth position.
 *
 * @param vec Vector container.
 * @param n Index of the requested element.
 * @return a pointer to the n-th element in the vector.
 * @memberof vector
 */
static inline void *vector_at(const struct vector *vec, size_t n) {
    return (uint8_t *) vec->data + n * vec->element_size;
}

/**
 * Returns the first element.
 *
 * @param vec Vector container.
 * @return a pointer to the first element in the vector.
 * @pre vector must contain at least one element.
 * @memberof vector
 */
static inline void *vector_front(const struct vector *vec) {
    return (uint8_t *) vec->data;
}

/**
 * Returns the last element.
 *
 * @param vec Vector container.
 * @return a pointer to the last element in the vector.
 * @pre vector must contain at least one element.
 * @memberof vector
 */
static inline void *vector_back(const struct vector *vec) {
    return (uint8_t *) vec->data + (vec->size - 1) * vec->element_size;
}

/**
 * Internally used to grow a vector container.
 *
 * @private
 * @memberof vector
 */
static inline void _vector_grow(struct vector *vec) {
    size_t new_capacity =
        vec->capacity + 1 + (size_t) (vec->scale_factor * vec->capacity);
    assert(new_capacity > vec->capacity);
    vec->data = xrealloc(vec->data, new_capacity * vec->element_size);
    vec->capacity = new_capacity;
    vec->threshold += (size_t) (vec->threshold * vec->scale_factor);
}

/**
 * Push the given element at the end of a vector.
 * @memberof vector
 */
static inline void vector_push_back(struct vector *vec, void *data) {
    if (vec->size >= vec->threshold) { _vector_grow(vec); }
    void *begin = vector_at(vec, vec->size);
    (void) memcpy(begin, data, vec->element_size);
    vec->size++;
}

/**
 * Pop an element from the end of a vector.
 *
 * @param[out] data Pointer to a buffer where the requested element will be
 * copied if it is not NULL.
 * @pre vector must contain at least one element.
 * @memberof vector
 */
static inline void vector_pop_back(struct vector *vec, void *data) {
    vec->size--;
    if (data != NULL) {
        void *begin = vector_at(vec, vec->size);
        (void) memcpy(data, begin, vec->element_size);
    }
}

/**
 * Insert an element at the desired position.
 *
 * @memberof vector
 */
static inline void
vector_insert(struct vector *vec, size_t position, void *data) {
    // TODO: Handle out of capacity error (position >= vec->capacity)
    if (vec->size >= vec->threshold) { _vector_grow(vec); }
    if (position < vec->size) {
        const size_t diff = (vec->size - position) * vec->element_size;
        (void) memmove(vector_at(vec, position + 1), vector_at(vec, position),
            diff);
        vec->size++;
    } else {
        vec->size = position + 1;
    }
    (void) memcpy(vector_at(vec, position), data, vec->element_size);
}

/**
 * Erase an element from the provided position.
 *
 * @param vec Vector container.
 * @param position Index of the element to be erased.
 * @pre position must be in range [0, vec->size) and the vector must contain at
 * least one element. Bounds are not checked.
 * @memberof vector
 */
static inline void vector_erase(struct vector *vec, size_t position) {
    assert(vec->size >= 0);
    assert(position < vec->size);
    const size_t diff = (vec->size - 1 - position) * vec->element_size;
    (void) memmove(vector_at(vec, position), vector_at(vec, position + 1),
        diff);
    vec->size--;
}

/* ------------------------ Type-specific functions ------------------------- */

/**
 * Declares a type-specific interface that wraps the generic vector functions.
 *
 * @tparam NAME The prefix of the new type-specific vector functions (e.g. if
 * NAME is document then the generated interfaces will start with document_.
 * @tparam TYPE The type of the contained element.
 * @related vector
 */
#define declare_vector_type(NAME, TYPE)                                        \
                                                                               \
    attr_unused static inline void NAME##_vector_init(struct vector *vec,      \
        size_t capacity, size_t threshold, float scale_factor) {               \
        vector_init(vec, sizeof(TYPE), capacity, threshold, scale_factor);     \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_init_default(                 \
        struct vector *vec, size_t capacity) {                                 \
        vector_init_default(vec, sizeof(TYPE), capacity);                      \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_cleanup(struct vector *vec) { \
        vector_cleanup(vec);                                                   \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_clear(struct vector *vec) {   \
        vector_clear(vec);                                                     \
    }                                                                          \
                                                                               \
    attr_unused static inline bool NAME##_vector_empty(struct vector *vec) {   \
        return vector_empty(vec);                                              \
    }                                                                          \
                                                                               \
    attr_unused static inline TYPE *NAME##_vector_at(const struct vector *vec, \
        size_t n) {                                                            \
        return (TYPE *) vector_at(vec, n);                                     \
    }                                                                          \
                                                                               \
    attr_unused static inline TYPE *NAME##_vector_back(                        \
        const struct vector *vec) {                                            \
        return (TYPE *) vector_back(vec);                                      \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_push_back(struct vector *vec, \
        TYPE *element) {                                                       \
        vector_push_back(vec, element);                                        \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_pop_back(struct vector *vec,  \
        TYPE *element) {                                                       \
        vector_pop_back(vec, element);                                         \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_insert(struct vector *vec,    \
        size_t position, TYPE *element) {                                      \
        vector_insert(vec, position, element);                                 \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_erase(struct vector *vec,     \
        size_t position) {                                                     \
        vector_erase(vec, position);                                           \
    }

/**
 * A variant of @ref declare_vector_type meant to be used for storing value
 * types.
 *
 * @related vector
 */
#define declare_vector_type_val(NAME, TYPE)                                    \
                                                                               \
    declare_vector_type(NAME, TYPE);                                           \
                                                                               \
    attr_unused static inline TYPE NAME##_vector_at_val(struct vector *vec,    \
        size_t position) {                                                     \
        return *(TYPE *) (vector_at(vec, position));                           \
    }                                                                          \
                                                                               \
    attr_unused static inline TYPE NAME##_vector_back_val(                     \
        struct vector *vec) {                                                  \
        return *(TYPE *) (vector_back(vec));                                   \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_push_back_val(                \
        struct vector *vec, TYPE element) {                                    \
        vector_push_back(vec, &element);                                       \
    }                                                                          \
                                                                               \
    attr_unused static inline void NAME##_vector_insert_val(                   \
        struct vector *vec, size_t position, TYPE element) {                   \
        vector_insert(vec, position, &element);                                \
    }

/* ---------------------------- Iteration macros ---------------------------- */

/**
 * A construct for iterating over a vector.
 *
 * @param pos A (type *) that holds the current element.
 * @param type The type of elements the vector holds.
 * @param vector Vector container.
 * @related vector
 */
#define vector_for_each_entry(pos, type, vector)                               \
    for ((pos) = (type *) (vector).data;                                       \
        (pos) - (type *) (vector).data != (vector).size; ++(pos))

/**
 * A construct for iterating over a vector in reverse order.
 *
 * @param pos A (type *) that holds the current element.
 * @param type The type of elements the vector holds.
 * @param vector Vector container.
 * @related vector
 */
#define vector_for_each_entry_reverse(pos, type, vector)                       \
    for ((pos) = (type *) (vector).data + (vector).size - 1;                   \
        (pos) >= (type *) (vector).data; --(pos))

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_UTILS_H__
#define __LIBRARIAN_UTILS_H__

#include <ctype.h>
#include <string.h>

#include "types.h"

/* ------------------------------ String utils ------------------------------ */

int c_str_cmp_case(const char *s1, const char *s2);

int c_str_cmp_null_safe(const char *s1, const char *s2);

void c_str_strip_ws(const char *c_str, const char **begin, const char **end);

static inline void skip_spaces(char **str) {
    if (!str) { return; }
    while (*str && isspace(**str)) { (*str)++; }
}

/**
 * @brief Decides whether a given C-style string is NULL or it consists of
 * zero-or-more blank characters.
 */
bool c_str_null_or_blank(const char *c_str);

static inline bool c_str_has_suffix(const char *c_str, size_t str_length,
    const char *suffix, size_t suffix_length) {
    if (!c_str || !suffix || str_length < suffix_length) { return false; }
    const char *begin = c_str + str_length - suffix_length;
    return strncmp(begin, suffix, suffix_length) == 0;
}

int c_str_to_int(const char *str, long int *val);

#define INVALID_TERM_COLOR -2

/**
 * @brief Converts string to xterm color.
 *
 * @return @ref INVALID_TERM_COLOR if parsing failed, -1 to 255 otherwise.
 */
int c_str_to_term_color(const char *str);

#define array_static_length(array) (sizeof(array) / sizeof((array)[0]))

#define c_str_static_length(c_str) (array_static_length(c_str) - 1)

#define c_str_list_for_each(current, separators, c_str)                        \
    for (char *_state,                                                         \
        *_tmp = (current) = strtok_r((c_str), (separators), &_state);          \
        (current) != NULL;                                                     \
        (current) = strtok_r(NULL, (separators), &_state), (void) _tmp)

#define null_terminated_array_seek_last(array, it)                             \
    do {                                                                       \
        for ((it) = (array); *(it) != NULL; ++(it)) { }                        \
        --(it);                                                                \
    } while (0);

/* ------------------------------- Math utils ------------------------------- */

#define max(a, b) ((a) > (b) ? (a) : (b))

#define min(a, b) ((a) < (b) ? (a) : (b))

#define sgn(x) (((x) > 0) - ((x) < 0))

// A bug waiting to happen
// clang-format off
#define uint_digits(x)                                                         \
   ((x) < 10 ? 1:                                                              \
    (x) < 100 ? 2:                                                             \
    (x) < 1000 ? 3:                                                            \
    (x) < 10000 ? 4:                                                           \
    (x) < 100000 ? 5:                                                          \
    (x) < 1000000 ? 6:                                                         \
    (x) < 10000000 ? 7:                                                        \
    (x) < 100000000 ? 8:                                                       \
    (x) < 1000000000 ? 9: 10)
// clang-format on

#define swap(lhs, rhs)                                                         \
    do {                                                                       \
        (lhs) ^= (rhs);                                                        \
        (rhs) ^= (lhs);                                                        \
        (lhs) ^= (rhs);                                                        \
    } while (0);

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>
#include <string.h>
#include <wctype.h>

#include <librarian/localization.h>
#include <librarian/log.h>
#include <librarian/types.h>
#include <librarian/utf8.h>
#include <librarian/xmalloc.h>

#include "command-line.h"
#include "librarian.h"

// clang-format off
struct command_line cmd_line = {
    .window = NULL,
    .buffer = NULL,
    .pos = 0,
    .char_pos = 0,
    .length = 0,
    .char_length = 0,
    .capacity = CMD_BUFFER_SIZE,
    .has_message = false,
    .start_pos = 1,
    .frame_pos = 0,
    .width = 0,
    .executor = NULL
};
//clang-format on

/*------------------------- Command line operations ------------------------- */

void command_line_init(void) {
    cmd_line.window = newwin(1, COLS, LINES - 1, 0);
    cmd_line.buffer = xnew_n(char, CMD_BUFFER_SIZE);
    cmd_line.buffer[0] = '\0';
    keypad(cmd_line.window, true);
    set_escdelay(0);
}

void command_line_activate(void) {
    werase(cmd_line.window);
    mvwin(cmd_line.window, LINES - 1, 0);
    cmd_line.width = COLS;
    curs_set(1);
}

void command_line_handle_resize(void) {
    mvwin(cmd_line.window, LINES - 1, 0);

    if (cmd_line.width == COLS) { goto update; }

    if (cmd_line.width > COLS) { // shrinks
        register int dx = cmd_line.char_pos - cmd_line.frame_pos - COLS + 2;
        if (dx > 0) { cmd_line.frame_pos += dx; }
    } else { // grows
        register int dx = cmd_line.frame_pos + COLS - 2 - cmd_line.char_length;
        if (dx > 0) {
            if (cmd_line.frame_pos > dx) {
                cmd_line.frame_pos -= dx;
            } else {
                cmd_line.frame_pos = 0;
            }
        }
    }
    cmd_line.width = COLS;

update:
    command_line_update();
}

void command_line_loop(void) {
    wint_t ch;
    int key = wget_wch(cmd_line.window, &ch);
    if (!ch) { return; }

    if (iswprint(ch) && key != KEY_CODE_YES) {
        command_line_insert_char(ch);
        command_line_update();
    } else {
        command_line_handle_keys(ch);
    }
}

bool command_line_handle_keys(wint_t ch) {
    switch (ch) {
        case KEY_LEFT:
            command_line_move_left();
            command_line_update();
            break;
        case KEY_RIGHT:
            command_line_move_right();
            command_line_update();
            break;
        case KEY_HOME:
            command_line_home();
            command_line_update();
            break;
        case KEY_END:
            command_line_end();
            command_line_update();
            break;
        case KEY_UP:
        case KEY_DOWN:
            break;
        case BACKSPACE_KEY:
        case KEY_BACKSPACE:
        case '\b': // Handles xterm
            if (!cmd_line.length) {
                command_line_clear();
                set_mode(NORMAL);
                break;
            }
            if (!cmd_line.char_pos) { break; }
            command_line_backspace();
            command_line_update();
            break;
        case KEY_DC:
            if (!cmd_line.length) {
                command_line_clear();
                set_mode(NORMAL);
                break;
            }
            if (cmd_line.char_pos < cmd_line.char_length) {
                command_line_delete();
            } else if (cmd_line.char_pos) {
                command_line_backspace();
            }
            command_line_update();
            break;
        case ESC_KEY:
            command_line_clear();
            set_mode(NORMAL);
            break;
        case ENTER_KEY:
            command_line_execute();
            set_mode(NORMAL);
            break;
        default:
            return false;
    }
    return true;
}

void command_line_move_left(void) {
    if (cmd_line.length && cmd_line.char_pos > 0) {
        cmd_line.char_pos--;
        u8_dec(cmd_line.buffer, &cmd_line.pos);

        if (getcurx(cmd_line.window) == cmd_line.start_pos) {
            cmd_line.frame_pos--;
        }
    }
}

void command_line_move_right(void) {
    if (cmd_line.char_pos < cmd_line.char_length) {
        cmd_line.char_pos++;
        u8_nextchar(cmd_line.buffer, &cmd_line.pos);

        if (getcurx(cmd_line.window) == COLS - 1) { cmd_line.frame_pos++; }
    }
}

void command_line_home(void) {
    cmd_line.pos = 0;
    cmd_line.char_pos = 0;
    cmd_line.frame_pos = 0;
}

void command_line_end(void) {
    cmd_line.pos = cmd_line.length;
    cmd_line.char_pos = cmd_line.char_length;
    cmd_line.frame_pos = cmd_line.start_pos + cmd_line.char_length > COLS
        ? cmd_line.char_length - COLS + cmd_line.start_pos + 1
        : 0;
}

void command_line_update(void) {
    wmove(cmd_line.window, 0, cmd_line.start_pos);
    wclrtoeol(cmd_line.window);
    size_t offset = u8_strnwidth(cmd_line.buffer, cmd_line.frame_pos);
    size_t width = u8_strnwidth(cmd_line.buffer + offset,
        COLS - cmd_line.start_pos); // -1 or no?

    // BUG: Narrowing conversion here, probably fine
    wprintw(cmd_line.window, "%.*s", (int) width, cmd_line.buffer + offset);

    wmove(cmd_line.window, 0,
        cmd_line.start_pos + cmd_line.char_pos - cmd_line.frame_pos);
}

void command_line_insert_char(wchar_t c) {
    int size = u8_wc_len(c);

    if (cmd_line.length + size > cmd_line.capacity) {
        cmd_line.capacity *= 2;
        cmd_line.buffer = xrenew(char, cmd_line.buffer, cmd_line.capacity);
    }

    memmove(cmd_line.buffer + cmd_line.pos + size,
        cmd_line.buffer + cmd_line.pos, cmd_line.length - cmd_line.pos + 1);
    cmd_line.pos += u8_wc_toutf8(cmd_line.buffer + cmd_line.pos, c);
    cmd_line.char_pos++;
    cmd_line.length += size;
    cmd_line.char_length++;

    if (getcurx(cmd_line.window) == COLS - 1) { cmd_line.frame_pos++; }
}

void command_line_backspace(void) {
    int pos, size;

    pos = cmd_line.pos;
    u8_dec(cmd_line.buffer, &pos);
    size = cmd_line.pos - pos;

    memmove(cmd_line.buffer + pos, cmd_line.buffer + cmd_line.pos,
        cmd_line.length - cmd_line.pos + 1);

    cmd_line.char_pos--;
    cmd_line.pos -= size;
    cmd_line.length -= size;
    cmd_line.char_length--;
}

void command_line_delete(void) {
    int size = u8_seqlen(cmd_line.buffer + cmd_line.pos);
    cmd_line.length -= size;
    cmd_line.char_length--;

    memmove(cmd_line.buffer + cmd_line.pos,
        cmd_line.buffer + cmd_line.pos + size,
        cmd_line.length - cmd_line.pos + 1);
}

void command_line_execute(void) {
    werase(cmd_line.window);
    wrefresh(cmd_line.window);

    if (!cmd_line.executor) {
        command_line_clear_buffer();
        return;
    }

    bool valid_command = cmd_line.executor(cmd_line.buffer);

    if (!valid_command) {
        log_error(&librarian.log, "%s", _("Unknown command specified!"));
    }

    command_line_clear_buffer();
}

void command_line_message(const char * const message, int color_code) {
    cmd_line.has_message = true;
    command_line_clear_buffer();
    wattron(cmd_line.window, COLOR_PAIR(color_code));
    wprintw(cmd_line.window, "%s", message);
    wattroff(cmd_line.window, COLOR_PAIR(color_code));
    wrefresh(cmd_line.window);
}

void command_line_clear(void) {
    command_line_clear_buffer();
    werase(cmd_line.window);
    wrefresh(cmd_line.window);
}

void command_line_clear_buffer(void) {
    cmd_line.buffer[0] = '\0';
    cmd_line.pos = 0;
    cmd_line.char_pos = 0;
    cmd_line.frame_pos = 0;
    cmd_line.length = 0;
    cmd_line.char_length = 0;
}

void command_line_cleanup(void) {
    xfree_ptr(&cmd_line.buffer);
    delwin(cmd_line.window);
}

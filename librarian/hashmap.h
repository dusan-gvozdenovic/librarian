/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_HASHMAP_H__
#define __LIBRARIAN_HASHMAP_H__

#include "compiler.h"
#include "types.h"

struct hashmap {
    size_t size;
    size_t capacity;
    size_t key_size;
    size_t value_size;
    float load_factor;
    uint8_t *data;
    hash (*hash)(const void *element);
    bool (*equal)(const void *lhs, const void *rhs);
};

struct hashmap_iterator {
    uint8_t *current;
};

/* --------------------------- Hashmap interface ---------------------------- */

/**
 * Initializes the hashmap structure.
 * @related hashmap
 */
void hashmap_init(struct hashmap *map, size_t initial_capacity, size_t key_size,
    size_t value_size);

/**
 * Returns the segment of memory where the key is stored.
 * @memberof hashmap
 */
void *hashmap_iterator_key(const struct hashmap *map,
    struct hashmap_iterator iterator);

/**
 * Returns the segment of memory where the value is stored.
 * @memberof hashmap
 */
void *hashmap_iterator_value(const struct hashmap *map,
    struct hashmap_iterator iterator);

/**
 * Returns the iterator pointing to the containing entry of the provided key
 * segment.
 * @param key_segment - key segment that belongs to the hashmap.
 * @memberof hashmap
 */
struct hashmap_iterator
hashmap_iterator_of_key(const struct hashmap *map, const uint8_t *key_segment);

/**
 * Returns the iterator pointing to the containing entry of the provided value
 * segment.
 * @param value_segment - value segment that belongs to the hashmap.
 * @memberof hashmap
 */
struct hashmap_iterator hashmap_iterator_of_value(const struct hashmap *map,
    const uint8_t *value_segment);

/**
 * @return map.size > 0 - the iterator pointing to the first non-empty entry.
 * @return map.size == 0 - @see hashmap_end().
 * @memberof hashmap
 */
struct hashmap_iterator hashmap_begin(const struct hashmap *map);

/**
 * Returns the address marking the end of hashmap.
 * @memberof hashmap
 */
struct hashmap_iterator hashmap_end(const struct hashmap *map);

/**
 * Positions the iterator to point to the next occupied entry or end if it is
 * the last.
 * @memberof hashmap
 */
struct hashmap_iterator
hashmap_next(const struct hashmap *map, struct hashmap_iterator it);

/**
 * Checks if the supplied iterator is the end iterator.
 */
static inline bool hashmap_iterator_is_end(const struct hashmap *map,
    const struct hashmap_iterator iterator) {
    return iterator.current == hashmap_end(map).current;
}

/**
 * Reserves at least @ref entries in the hashmap, resizing it if necessary.
 * @memberof hashmap
 */
void hashmap_reserve(struct hashmap *map, size_t entries);

/**
 * Clears the contents.
 * @memberof hashmap
 */
void hashmap_clear(struct hashmap *map);

/**
 * Associates the provided value with the key in the hashmap. In case a
 * key already exists in the map, its value will be overwritten with the
 * provided value.
 * @memberof hashmap
 */
void hashmap_set(struct hashmap *map, void *key, void *value);

/**
 * Searches for the given key in the hashmap and returns the associated value.
 * @return If successful, pointer to the value stored in the map. NULL
 * otherwise.
 * @memberof hashmap
 */
struct hashmap_iterator hashmap_find(const struct hashmap *map, void *key);

/**
 * Checks if the given key exists in the hashmap.
 * @memberof hashmap
 */
static inline bool hashmap_contains_key(const struct hashmap *map, void *key) {
    return !hashmap_iterator_is_end(map, hashmap_find(map, key));
}

/**
 * Removes the entry identified by key from the hashmap.
 * @return boolean value indicating whether the key was removed.
 * @memberof hashmap
 */
struct hashmap_iterator
hashmap_remove(struct hashmap *map, struct hashmap_iterator it);

/**
 * Releases the underlying buffer and resets the structure.
 * @related hashmap
 */
void hashmap_cleanup(struct hashmap *map);

/* ------------------------ Type-specific functions ------------------------- */

#define declare_hashmap_type(NAME, KEY, VALUE)                                 \
                                                                               \
    attr_unused static inline void NAME##_hashmap_init(struct hashmap *map,    \
        size_t initial_capacity) {                                             \
        hashmap_init(map, initial_capacity, sizeof(KEY), sizeof(VALUE));       \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashmap_iterator_key                                           \
     */                                                                        \
    attr_unused static inline KEY *NAME##_hashmap_iterator_key(                \
        const struct hashmap *map, struct hashmap_iterator iterator) {         \
        return hashmap_iterator_key(map, iterator);                            \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashmap_iterator_value                                         \
     */                                                                        \
    attr_unused VALUE *NAME##_hashmap_iterator_value(                          \
        const struct hashmap *map, struct hashmap_iterator iterator) {         \
        return hashmap_iterator_value(map, iterator);                          \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashmap_set                                                    \
     */                                                                        \
    attr_unused static inline void NAME##_hashmap_set(struct hashmap *map,     \
        KEY *key, VALUE *value) {                                              \
        hashmap_set(map, key, value);                                          \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashmap_find                                                   \
     */                                                                        \
    attr_unused static inline struct hashmap_iterator NAME##_hashmap_find(     \
        struct hashmap *map, KEY *key) {                                       \
        return hashmap_find(map, key);                                         \
    }                                                                          \
    /**                                                                        \
     * @copydoc hashmap_contains_key                                           \
     */                                                                        \
    attr_unused static inline bool NAME##_hashmap_contains_key(                \
        struct hashmap *map, KEY *key) {                                       \
        return hashmap_contains_key(map, key);                                 \
    }

/**
 * A construct for iterating over a hashmap.
 *
 * @param map Hashmap container.
 * @param it A pointer to a hashmap_iterator.
 * @related hashmap
 */
#define hashmap_for_each(map, it)                                              \
    for ((it) = hashmap_begin(&(map)); !hashmap_iterator_is_end(&(map), (it)); \
        (it) = hashmap_next(&(map), (it)))

#endif

project(librarian-ncurses
    LANGUAGES C
    VERSION 0.0.${PROJECT_VERSION_PATCH}
    DESCRIPTION "Librarian's ncurses frontend.")

include(Librarian)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS ${LIBRARIAN_RECOMMENDED_C_FLAGS})
set(CMAKE_LINK_FLAGS ${LIBRARIAN_RECOMMENDED_LINK_FLAGS})

set(CURSES_NEED_NCURSES TRUE)

# Seems to be correct on FreeBSD and macOS.
if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
    set(CURSES_NEED_WIDE TRUE)
endif()

find_package(Curses REQUIRED)

# librarian does not use the ncurses form library.
list(REMOVE_ITEM CURSES_LIBRARIES ${CURSES_FORM_LIBRARY})

# --------------------------------- librarian -------------------------------- #

set(LIBRARIAN_NCURSES_EXEC_NAME librarian)

# TODO: Move this somewhere better to avoid duplication
set(LIBRARIAN_CORE_LIBRARY_NAME librarian-core)

file(GLOB LIBRARIAN_NCURSES_SOURCES *.c)
file(GLOB LIBRARIAN_NCURSES_HEADERS *.h)

doxygen_include_headers(LIBRARIAN_NCURSES_HEADERS)
translate_files(${LIBRARIAN_NCURSES_SOURCES})

add_executable(${LIBRARIAN_NCURSES_EXEC_NAME}
    ${LIBRARIAN_NCURSES_SOURCES})

target_compile_definitions(${LIBRARIAN_NCURSES_EXEC_NAME}
    PUBLIC _XOPEN_SOURCE=700
    PUBLIC _XOPEN_SOURCE_EXTENDED=700
    PUBLIC LIBRARIAN_VERSION_MAJOR=${PROJECT_VERSION_MAJOR}
    PUBLIC LIBRARIAN_VERSION_MINOR=${PROJECT_VERSION_MINOR}
    PUBLIC LIBRARIAN_VERSION_PATCH=${PROJECT_VERSION_PATCH})

if(ENABLE_LOCALIZATION)
    target_compile_definitions(${LIBRARIAN_CORE_LIBRARY_NAME}
        PUBLIC LIBRARIAN_LOCALIZATION_ENABLED)
endif()

target_include_directories(${LIBRARIAN_NCURSES_EXEC_NAME}
    PUBLIC ${LIBRARIAN_LOCALIZATION_DIRS} ${CURSES_INCLUDE_DIRS})
target_include_directories(${LIBRARIAN_NCURSES_EXEC_NAME}
    PRIVATE ${CMAKE_SOURCE_DIR})

target_link_libraries(${LIBRARIAN_NCURSES_EXEC_NAME}
    ${LIBRARIAN_CORE_LIBRARY_NAME}
    ${CURSES_LIBRARIES}
    ${LIBRARIAN_LOCALIZATION_LIBS})

# ---------------------------------- Install --------------------------------- #

install(TARGETS ${LIBRARIAN_NCURSES_EXEC_NAME}
    DESTINATION bin
)

if(NOT CMAKE_INSTALL_RPATH)
    if (APPLE)
        set_target_properties(${LIBRARIAN_NCURSES_EXEC_NAME} PROPERTIES
            INSTALL_RPATH "@loader_path/../lib")
    else()
        # Assuming Linux, *BSD or something similar that provides $ORIGIN.
        set_target_properties(${LIBRARIAN_NCURSES_EXEC_NAME} PROPERTIES
            INSTALL_RPATH "$ORIGIN/../lib")
    endif()
endif()

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_CORE_H__
#define __LIBRARIAN_CORE_H__

#include "compiler.h"
#include "config.h"
#include "format.h"
#include "library.h"
#include "log.h"
#include "observer.h"
#include "plugin-registry.h"

#ifndef LIBRARIAN_SYSTEM_PREFIX
#error "Expected LIBRARIAN_SYSTEM_PREFIX to be defined!"
#endif

struct librarian {
    /* Components */
    struct log log;
    struct config config;
    struct library library;
    struct plugin_registry plugin_registry;
    struct format_registry format_registry;

    /* Lifecycle */
    struct subject on_init;
    struct subject on_exit;
};

struct lifecycle_exit_args {
    int status;
};

#define librarian_from_component(ptr)                                          \
    _Generic(*(ptr),                                                           \
        struct log: container_of((void *) (ptr), struct librarian, log),       \
        struct config: container_of((void *) (ptr), struct librarian, config), \
        struct library: container_of((void *) (ptr), struct librarian,         \
            library),                                                          \
        struct plugin_registry: container_of((void *) (ptr), struct librarian, \
            plugin_registry),                                                  \
        struct format_registry: container_of((void *) (ptr), struct librarian, \
            format_registry))

void librarian_init(struct librarian *librarian);

attr_noreturn void librarian_panic(int status);

void librarian_exit(struct librarian *librarian, int status);

#endif

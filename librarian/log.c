/* SPDX-License-Identifier: Apache-2.0 */

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "localization.h"
#include "log.h"
#include "xmalloc.h"

#define LOG_BUFFER_SIZE_DEFAULT 1024

static void _flush_default(struct log *log, enum log_mode mode) {
    FILE *out = stdout;
    if (mode == LOG_ERROR) {
        log->empty = false;
        out = stderr;
    }
    fprintf(out, "%s\n", log->buffer);
}

void log_init(struct log *log) {
    log->buffer = xnew_n(char, LOG_BUFFER_SIZE_DEFAULT);
    log->empty = true;
    log->flush = _flush_default;
}

void log_cleanup(struct log *log) { xfree_ptr(&log->buffer); }

void log_print(struct log *log, enum log_mode mode, const char *format, ...) {
    va_list ap;
    va_start(ap, format);

    if (mode == LOG_WARN) {
        sprintf(log->buffer, "%s: ", _("Warning"));
    } else if (mode == LOG_ERROR) {
        sprintf(log->buffer, "%s: ", _("Error"));
    }

    char *end = (char *) log->buffer + strlen(log->buffer);

    vsnprintf(end, LOG_BUFFER_SIZE_DEFAULT, format, ap);

    va_end(ap);

    log->flush(log, mode);

    log->buffer[0] = '\0';
}

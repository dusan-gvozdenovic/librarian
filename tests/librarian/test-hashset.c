/* SPDX-License-Identifier: Apache-2.0 */

#include <string.h>

#include <test.h>

#include <librarian/hash.h>
#include <librarian/hashset.h>
#include <librarian/types.h>
#include <librarian/utils.h>

#define assert_hashset_iterator_end(set, it)                                   \
    assert(hashset_iterator_is_end(&(set), (it)))

#define assert_hashset_iterator_not_end(set, it)                               \
    assert_false(hashset_iterator_is_end(&(set), (it)))

struct hashset set;

const size_t INITIAL_CAPACITY = 4;

// clang-format off
char *WORDLIST[] = {
    "The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog",
    "To", "be", "or", "not", "to", "that", "is", "question",
    NULL
};
// clang-format on

hash str_hash(const void *element) { return hash_str_djb2(element); }
bool str_equal(const void *lhs, const void *rhs) {
    return strcmp(lhs, rhs) == 0;
}

void test_hashset_setup(void) {
    hashset_init(&set, INITIAL_CAPACITY, sizeof(char *));
    set.parent.hash = str_hash;
    set.parent.equal = str_equal;
}

void test_hashset_teardown(void) { hashset_cleanup(&set); }

void test_insert_one(void) {
    char *value = "Hello";
    hashset_insert(&set, value);
    assert(hashset_contains(&set, value));
}

void test_insert(void) {
    for (char **word = WORDLIST; *word != NULL; word++) {
        hashset_insert(&set, *word);
    }
    for (char **word = WORDLIST; *word != NULL; word++) {
        assert(hashset_contains(&set, *word));
    }
}

void test_delete(void) {
    for (char **word = WORDLIST; *word != NULL; word++) {
        hashset_insert(&set, *word);
    }

    assert_equal(set.parent.size, array_static_length(WORDLIST) - 1);

    for (char **word = WORDLIST; *word != NULL; word++) {
        assert(hashset_contains(&set, *word));
    }
    for (char **word = WORDLIST; *word != NULL; word++) {
        struct hashset_iterator it = hashset_find(&set, *word);
        assert_hashset_iterator_not_end(set, it);
        hashset_remove(&set, it);
        assert_false(hashset_contains(&set, *word));
    }
    assert(set.parent.size == 0);
}

initialize_runner("test-hashset");

register_test_suite(hashset, test_hashset_setup, test_hashset_teardown);

register_test(hashset, test_insert_one);
register_test(hashset, test_insert);
register_test(hashset, test_delete);

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_HASH_H__
#define __LIBRARIAN_HASH_H__

#include "types.h" // hash

hash hash_str_djb2(const void *ptr);

// XXX: Is 32 always appropriate here?
// A macro for defining concrete integer hash functions.
#define hash_int(value) ((value) ^ (value) >> 32);

// Predefined hash function for storing (char *) in a hashmap.
hash hash_key_c_str(const void *ptr);

// Predefined equal function for storing (char *) in a hashmap.
bool hash_key_c_str_equal(const void *lhs, const void *rhs);

// Predefined hash function for storing a key of type (void *) in a hashmap.
hash hash_key_ptr(const void *ptr);

// Predefined equal function for storing a key of type (void *) in a hashmap.
bool hash_key_ptr_equal(const void *lhs, const void *rhs);

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_LIMITS_H__
#define __LIBRARIAN_LIMITS_H__

#ifdef __linux__
#include <linux/limits.h> // IWYU pragma: export
#elif defined(__FreeBSD__)
#include <limits.h> // IWYU pragma: export
#elif defined(__unix__) || defined(__UNIX__) ||                                \
    (defined(__APPLE__) && defined(__MACH__))
#include <sys/syslimits.h> // IWYU pragma: export
#else
#define PATH_MAX 1024
#endif

#endif

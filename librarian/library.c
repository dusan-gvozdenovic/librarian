/* SPDX-License-Identifier: Apache-2.0 */

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "compiler.h"
#include "core.h"
#include "document-types.h"
#include "document.h"
#include "file.h"
#include "format.h"
#include "library.h"
#include "localization.h"
#include "log.h"
#include "observer.h"
#include "rbtree.h"
#include "serializer.h"
#include "u-string.h"
#include "vector.h"

/* -------------------- Internal function declarations ---------------------- */

/**
 * Passed to walk_directory callback to provide a reference to format_registry
 * and collect new documents.
 */
struct library_add_context {
    struct library *library;
    struct vector new_docs;
};

/**
 * This function is passed to @ref walk_directory in @ref library_add_from_dir.
 */
static void _library_add_from_dir_impl(const struct path *path, void *args);

static void
_library_notify_observer(struct observer *observer, struct library_args *args);

static void _library_notify_cleared(struct observer *observer);

static void _library_write_headers(struct serializer *sz);

/**
 * Reads the library headers.
 *
 * @return true if headers are valid and successfully read, false otherwise.
 * @remark This is a good place to check whether the library file and current
 * version of librarian are compatible. If not, @ref library_load can be stopped
 * and appropriate error message can be issued. Alternatively, we can implement
 * migrations between library versions.
 */
static bool
_library_read_headers(struct library *library, struct serializer *sz);

/**
 * Internal function to free the library tree depth-first.
 *
 * @param rbnode Root node of the tree to be freed.
 */
static void _library_free(struct rbnode **rbnode);

/* ---------------------------- Implementation ------------------------------ */

void library_init(struct library *library) {
    library->path = PATH_INIT;
    library->lib_tree = RBTREE_INIT;
    library->size = 0;
    struct u_string err = U_STRING_INIT;
    (void) path_base_get(&library->path, PATH_BASE_DATA, "library");
    u_string_cleanup(&err);
    subject_init(&library->subject, (notifier) _library_notify_observer);
    subject_init(&library->cleared, (notifier) _library_notify_cleared);
    library->version = LIBRARIAN_VERSION;
}

void library_load(struct library *library) {
    FILE *fp = fopen(library->path.str.c_str, "rb");
    struct log *log = &librarian_from_component(library)->log;

    if (!fp) {
        if (errno != ENOENT) {
            log_error(log,
                _("Failed to open file for reading: \"%s\". Reason: %s."),
                library->path.str.c_str, strerror(errno));
        }
        return;
    }

    struct serializer sz;
    serializer_init(&sz, fp, false);

    bool success = _library_read_headers(library, &sz);

    if (!success) { goto exit; }

    while (serializer_has_more(&sz)) {
        struct document *doc = document_new();
        bool success = document_deserialize(doc, &sz);
        if (success && !library_contains_document(library, doc)) {
            library_add(library, doc);
        }
        document_unref(&doc);
    }
exit:
    fclose(fp);
}

bool _library_read_headers(struct library *library, struct serializer *sz) {
    bool success =
        serializer_read_int(sz, sizeof(lib_version), &library->version);

    if (!success) { return true; } // Library file is empty

    if (lib_version_significant(library->version) >
        lib_version_significant(LIBRARIAN_VERSION)) {
        struct log *log = &librarian_from_component(library)->log;
        /* XXX: We can issue an appropriate migration here later, but for now we
         * only show warning. */
        log_warn(log,
            _("The library version %llu is greater than librarian "
              "version %llu. Consider updating librarian."),
            library->version, LIBRARIAN_VERSION);
        return false;
    }

    return true;
}

bool library_contains_document(struct library *library, struct document *doc) {
    struct rbnode **link = &library->lib_tree.root;
    while (*link != RBTREE_NULL) {
        struct document *curr = rbtree_entry(*link, struct document, tree_node);
        if (doc->path.str.byte_length == curr->path.str.byte_length &&
            path_compare(&doc->path, &curr->path) == 0) {
            return true;
        }
        link = document_compare_by_author(doc, curr) > 0 ? &(**link).right
                                                         : &(**link).left;
    }
    return false;
}

// TODO: Notify observers
void library_add(struct library *library, struct document *doc) {
    struct rbnode **link = &library->lib_tree.root, *parent = RBTREE_NULL;

    while (*link != RBTREE_NULL) {
        parent = *link;
        struct document *curr =
            rbtree_entry(parent, struct document, tree_node);
        link = document_compare_by_author(doc, curr) > 0 ? &(**link).right
                                                         : &(**link).left;
    }

    rbnode_link(&doc->tree_node, parent, link);

    rbtree_insert_fixup(&library->lib_tree, &doc->tree_node);
    document_ref(doc);
    library->size++;
}

void library_add_from_dir(struct library *library, const struct path *dir) {
    struct library_add_context ctx;
    ctx.library = library;

    document_vector_init_default(&ctx.new_docs, 10);

    // TODO: Propagate the reason upwards.
    bool success = walk_directory(dir, _library_add_from_dir_impl, &ctx);
    if (!success) { return; }

    if (ctx.new_docs.size > 0) {
        // XXX: This is dirty as we are copying the data ptr. Fix this sometime
        struct library_args args = {
            .operation_type = LIBRARY_ARGS_OP_ADD,
            .docs = ctx.new_docs,
        };
        subject_notify_observers(&library->subject, &args);
    }

    struct document **doc;
    vector_for_each_entry (doc, struct document *, ctx.new_docs) {
        document_unref(doc);
    }

    document_vector_cleanup(&ctx.new_docs);

    library_save(library);
}

void _library_add_from_dir_impl(const struct path *path, void *args) {
    struct library_add_context *ctx = args;
    struct format_registry *fr =
        &librarian_from_component(ctx->library)->format_registry;
    struct format_provider *fmt = format_registry_detect(fr, path->str.c_str);

    if (!fmt) { return; }

    struct document *doc = fmt->load(path->str.c_str);
    if (!library_contains_document(ctx->library, doc)) {
        library_add(ctx->library, doc);
        document_vector_push_back_val(&ctx->new_docs, doc);
        document_ref(doc);
    }

    document_unref(&doc);
}

void _library_notify_observer(struct observer *observer,
    struct library_args *args) {
    ((void (*)(struct library_args *)) observer->notify)(args);
}

void _library_notify_cleared(struct observer *observer) {
    ((void (*)()) observer->notify)();
}

void library_remove(struct library *library, struct document *doc) {
    rbtree_delete(&library->lib_tree, &doc->tree_node);
    library->size--;
    document_unref(&doc);
}

void library_remove_many(struct library *library, struct vector *docs) {
    struct document **doc;

    vector_for_each_entry (doc, struct document *, *docs) {
        document_ref(*doc);
        library_remove(library, *doc);
    }

    // XXX: This is dirty as we are copying the data ptr. Fix this sometime
    subject_notify_observers(&library->subject,
        &(struct library_args) {
            .operation_type = LIBRARY_ARGS_OP_REMOVE,
            .docs = *docs,
        });

    vector_for_each_entry (doc, struct document *, *docs) {
        struct document *tmp = *doc;
        document_unref(&tmp);
    }
}

void library_save(struct library *library) {
    // Don't destroy old library file
    if (lib_version_significant(library->version) >
        lib_version_significant(LIBRARIAN_VERSION)) {
        return;
    }

    struct u_string err = U_STRING_INIT;
    struct log *log = &librarian_from_component(library)->log;

    if (!path_base_ensure(PATH_BASE_DATA, &err)) {
        log_error(log, "%s");
        u_string_cleanup(&err);
        return;
    }

    u_string_cleanup(&err);

    FILE *fp = fopen(library->path.str.c_str, "wb+");

    if (!fp) {
        log_error(log,
            _("Failed to open file for writing: \"%s\". Reason: %s."),
            library->path.str.c_str, strerror(errno));
        return;
    }

    struct serializer sz;
    serializer_init(&sz, fp, false);

    _library_write_headers(&sz);

    if (library->size == 0) { goto exit; }

    struct vector stack;
    document_vector_init_default(&stack, library->size);

#define push_rbnode_doc(rbnode)                                                \
    document_vector_push_back_val(&stack,                                      \
        container_of((rbnode), struct document, tree_node))

    push_rbnode_doc(library->lib_tree.root);

    while (!vector_empty(&stack)) {
        struct document *doc;
        document_vector_pop_back(&stack, &doc);

        document_serialize(doc, &sz);

        if (doc->tree_node.right != RBTREE_NULL) {
            push_rbnode_doc(doc->tree_node.right);
        }
        if (doc->tree_node.left != RBTREE_NULL) {
            push_rbnode_doc(doc->tree_node.left);
        }
    }

#undef push_rbnode_doc

    document_vector_cleanup(&stack);

exit:
    fclose(fp);
}

void _library_write_headers(struct serializer *sz) {
    serializer_write_int(sz, sizeof(lib_version), LIBRARIAN_VERSION);
}

void _library_free(struct rbnode **rbnode) {
    if (*rbnode == RBTREE_NULL) { return; }
    if ((*rbnode)->left != RBTREE_NULL) { _library_free(&(*rbnode)->left); }
    if ((*rbnode)->right != RBTREE_NULL) { _library_free(&(*rbnode)->right); }
    struct document *doc = rbtree_entry(*rbnode, struct document, tree_node);
    document_unref(&doc);
}

void library_clear(struct library *library) {
    _library_free(&library->lib_tree.root);
    library->lib_tree = RBTREE_INIT;
    library->size = 0;
    library_save(library);
    subject_notify_observers(&library->cleared, NULL);
}

void library_filter(struct library *library,
    bool (*predicate)(struct document *doc),
    void (*callback)(struct document *doc, void *args), void *args) {
    struct document *doc;
    rbtree_for_each_entry (doc, &library->lib_tree, tree_node) {
        if (!predicate || predicate(doc)) { callback(doc, args); }
    }
}

void library_cleanup(struct library *library) {
    subject_unsubscribe_all(&library->subject);
    subject_unsubscribe_all(&library->cleared);
    _library_free(&library->lib_tree.root);
    path_cleanup(&library->path);
}

/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>

#include <librarian/compiler.h>
#include <librarian/config.h>
#include <librarian/hash.h>
#include <librarian/hashmap.h>
#include <librarian/hashset.h>
#include <librarian/observer.h>
#include <librarian/option.h>
#include <librarian/types.h>
#include <librarian/utils.h>
#include <librarian/vector.h>

#include "librarian.h"
#include "list-box.h"
#include "list-store.h"

declare_hashset_type(list_item, list_item);

static void _list_box_model_updated(struct list_model_updated_args *args);

void list_box_init(struct list_box *lb, struct list_item_properties props) {
    list_store_init(&lb->store, props);
    observer_init(&lb->store_updated, _list_box_model_updated);
    observer_subscribe(&lb->store_updated, &lb->store.parent.updated);

    list_item_hashset_init(&lb->selected_items, 16);
    lb->selected_items.parent.hash = hash_key_ptr;
    lb->selected_items.parent.equal = hash_key_ptr_equal;

    lb->top = lb->current = lb->store.parent.first(&lb->store.parent);
    lb->sel_pos = lb->top_pos = 0;
}

void list_box_paint(struct list_box *lb) {
    if (lb->top == NULL) { list_box_top(lb); }

    struct list_model *lm = &lb->store.parent;

    list_item *curr = lb->top;
    uint32_t line = 0;
    uint32_t position = lb->top_pos;
    uint32_t size = lm->size(lm);
    uint32_t pad_max = uint_digits(size);

    struct config *cfg = &librarian.config;

    for (; curr && line < lb->height; curr = lm->next(lm, curr), line++) {
        wmove(lb->window, lb->pos_y + line, lb->pos_x);

        const int choice = list_item_color_choose(curr == lb->current,
            list_box_selected(lb, *curr));

        wattron(lb->window, COLOR_PAIR(choice));

        if (lb->line_numbers && option_get(cfg->show_line_numbers)) {
            wprintw(lb->window, " %*s%d. ", pad_max - uint_digits(position + 1),
                "", position + 1);
        } else {
            wprintw(lb->window, " ");
        }

        lb->write_line(lb, *curr);

        position++;

        const int x = getcurx(lb->window);
        if (x < lb->width) { wprintw(lb->window, "%*s", lb->width - x, " "); }

        wattroff(lb->window, COLOR_PAIR(choice));
    }
}

void list_box_handle_resize(struct list_box *lb, uint32_t height,
    uint32_t width) {
    if (lb->width != width) { lb->width = width; }
    if (lb->height == height) { return; }
    if (!lb->top) { return; }

    struct list_model *lm = &lb->store.parent;

    if (lb->height > height) { // shrinks
        register int delta = lb->sel_pos + 2 - height;
        list_item *top_next = lm->next(lm, lb->top);
        while (top_next && delta-- > 0) {
            lb->top = top_next;
            lb->sel_pos--;
            lb->top_pos++;
            top_next = lm->next(lm, top_next);
        }
    } else { // grows
        // register int delta = height - lb->height;
        // register int delta = height - lb->sel_pos - 2;
        register int delta = lb->top_pos + height - 1 - lm->size(lm);
        void *top_prev = lm->prev(lm, lb->top);
        while (top_prev && delta-- > 0) {
            lb->top = top_prev;
            lb->sel_pos++;
            lb->top_pos--;
            top_prev = lm->prev(lm, top_prev);
        }
    }
    lb->height = height;
    lb->width = width;
}

/* ---------------------------- Selection Logic ----------------------------- */

void list_box_up(struct list_box *lb) {
    if (!lb->current) { return; }
    struct list_model *lm = &lb->store.parent;
    list_item *prev = lm->prev(lm, lb->current);
    if (!prev) { return; }
    list_item *top_prev = lm->prev(lm, lb->top);
    if (top_prev && lb->sel_pos <= 2) {
        lb->top = top_prev;
        lb->top_pos--;
    } else {
        lb->sel_pos--;
    }
    lb->current = prev;
}

void list_box_down(struct list_box *lb) {
    if (!lb->current) { return; }
    struct list_model *lm = &lb->store.parent;
    list_item *next = lm->next(lm, lb->current);
    if (!next) { return; }
    list_item *top_next = lm->next(lm, lb->top);

    if (top_next && lb->sel_pos >= lb->height - lb->pos_y - 3 &&
        lb->top_pos + lb->height <= lm->size(lm)) {
        lb->top = top_next;
        lb->top_pos++;
    } else {
        lb->sel_pos++;
    }
    lb->current = next;
}

void list_box_top(struct list_box *lb) {
    lb->top = lb->store.parent.first(&lb->store.parent);
    lb->current = lb->top;
    lb->sel_pos = 0;
    lb->top_pos = 0;
}

// FIX ME - convoluted logic
void list_box_bottom(struct list_box *lb) {
    struct list_model *lm = &lb->store.parent;
    if (lm->size(lm) == 0) {
        lb->top = lb->current = NULL;
        lb->sel_pos = lb->top_pos = 0;
        return;
    }

    lb->current = lm->last(lm);

    list_item *top = lb->current;
    uint32_t n = 0;

    while (n++ < (int64_t) lb->height - 2) {
        list_item *prev = lm->prev(lm, top);
        if (!prev) { break; }
        top = prev;
    }

    lb->top_pos = lm->size(lm) - 1 + 1 - n;
    lb->sel_pos = min(lb->height - 2, n - 1);
    lb->top = top;
}

void list_box_goto_line(struct list_box *lb, int64_t line) {
    // Current line can be expressed as lb->top_pos + lb->sel_pos.
    uint64_t lb_line = lb->top_pos + lb->sel_pos;
    int64_t k = lb_line - line;
    if (!k) { return; }

    list_item *curr = lb->current;

    while (k < 0) {
        list_box_down(lb);
        if (curr == lb->current) { break; } // Detect end
        curr = lb->current;
        k++;
    }

    while (k > 0) {
        list_box_up(lb);
        if (curr == lb->current) { break; } // Detect start
        curr = lb->current;
        k--;
    }
}

void list_box_select_all(struct list_box *lb) {
    struct list_model *lm = &lb->store.parent;
    struct hashset *sel = &lb->selected_items;

    for (list_item *it = lm->first(lm); it != NULL; it = lm->next(lm, it)) {
        struct hashset_iterator h_it = list_item_hashset_find(sel, it);

        if (hashset_iterator_is_end(sel, h_it)) {
            list_item_hashset_insert(sel, it);
            list_item_ref(*it);
        }
    }
}

void list_box_toggle_select(struct list_box *lb, list_item item) {
    struct hashset_iterator it =
        list_item_hashset_find(&lb->selected_items, &item);

    if (hashset_iterator_is_end(&lb->selected_items, it)) {
        list_item_hashset_insert(&lb->selected_items, &item);
        list_item_ref(item);
    } else {
        list_item tmp =
            *list_item_hashset_iterator_element(&lb->selected_items, it);
        hashset_remove(&lb->selected_items, it);
        list_item_unref(&lb->store.parent, &tmp);
    }
}

bool list_box_selected(const struct list_box *lb, list_item item) {
    return list_item_hashset_contains(&lb->selected_items, &item);
}

void list_box_clear_selection(struct list_box *lb) {
    struct hashset_iterator it;
    list_item *item;
    hashset_for_each (lb->selected_items, it) {
        item = list_item_hashset_iterator_element(&lb->selected_items, it);
        list_item_unref(&lb->store.parent, item);
    }
    hashset_clear(&lb->selected_items);
}

/* ------------------------------ View Updates ------------------------------ */

static void _list_box_model_updated(struct list_model_updated_args *args) {
    struct list_model *lm = args->self;
    struct list_store *store =
        container_of(args->self, struct list_store, parent);
    struct list_box *lb = container_of(store, struct list_box, store);

    // Case 1: Items are added.
    // We want to keep the selected item in the same sel_pos and choose top and
    // top_pos accordingly:
    // a. If the list shorter than the list box height, we do not want to move
    //    top and top_pos.
    //    Invariants:
    //    1. (lb->top == lm->first(lm)) || (lb->top == NULL).
    //    2. lb->top_pos == 0.
    // b. Otherwise, we want to keep current in the same place and find the new
    //    top element.
    if (args->operation == LIST_MODEL_UPDATE_ITEMS_ADDED) {
        uint64_t size = lm->size(lm);
        // 1.a: List is empty, just set first.
        if (lb->current == NULL) {
            lb->top_pos = lb->sel_pos = 0;
            lb->top = lb->current = lm->first(lm);
        }
        // 1.b: Underflowed - current needs to move.
        else if (size <= lb->height) {
            lb->top = lm->first(lm);
            lb->top_pos = 0;
            lb->sel_pos = lm->index_of(lm, lb->current);
        }
        // 1.a: As described above.
        else {
            lb->top_pos = lm->index_of(lm, lb->current) - lb->sel_pos;
            // lb->top := lm->select(lm, new_top_pos);
            // Iteratively select new top because the screen size is usually
            // small.
            list_item *new_top = lb->current;
            for (uint32_t i = 0; i < lb->sel_pos; i++) {
                new_top = lm->prev(lm, new_top);
            }
            lb->top = new_top;
        }
    }
    // Case 2: Items are re-ordered
    // We want to keep the selected item in the same sel_pos (or less if it is
    // closer to beginning) and choose top and top_pos accordingly.
    else if (args->operation == LIST_MODEL_UPDATE_ORDER_CHANGED) {
        uint64_t size = lm->size(lm);
        // Case a: Model is empty, do nothing.
        if (lb->current == NULL) {
        }
        // Case b: we are generally underflowed, therefore we just set the
        //         index of current as the new sel_pos.
        else if (size <= lb->height) {
            lb->top = lm->first(lm);
            lb->top_pos = 0;
            lb->sel_pos = lm->index_of(lm, lb->current);
        } else {
            uint64_t new_sel_abs = lm->index_of(lm, lb->current);
            if (lb->sel_pos + size - new_sel_abs < lb->height) {
                lb->sel_pos +=
                    lb->height - 1 - lb->sel_pos - size + new_sel_abs;
            }
            // In case there are not lb->sel_pos elements before current, we
            // take as many as we can.
            list_item *new_top = lb->current;
            uint32_t new_sel_pos = 0;
            for (new_sel_pos = 0; new_sel_pos < lb->sel_pos; new_sel_pos++) {
                list_item *it = lm->prev(lm, new_top);
                if (it == NULL) { break; }
                new_top = it;
            }
            lb->sel_pos = new_sel_pos;
            lb->top = new_top;
            lb->top_pos = new_sel_abs - lb->sel_pos;
        }
    }
    // Case 3.a: Items are about to be removed
    // We want to simulate the rest of elements being _pulled up_ after
    // deletion, keeping in mind that both top and current could be deleted.
    //
    // 1. If current is marked to be removed, we move it to its first
    // non-marked predecessor.
    // 2. If lb->top is marked to be removed, we move it to its first
    // non-marked predecessor.
    //
    // If lb->top or lb->current become NULL that means that there are no
    // predecessors that are not marked for removal. In that case we must
    // make the first non-marked successor as the new lb->top/lb->current after
    // deletion. If there is no successor as well, that means that the list is
    // empty.
    else if (args->operation == LIST_MODEL_UPDATE_ITEMS_ON_REMOVE) {
        struct hashset s;
        list_item_hashset_init(&s, args->items.size);
        s.parent.hash = hash_key_ptr;
        s.parent.equal = hash_key_ptr_equal;

        // We temporarily take a reference for the duration of this function.
        list_item *item;
        vector_for_each_entry (item, list_item, args->items) {
            list_item_hashset_insert(&s, item);
        }

        // Optimization: if we cross lb->top while choosing new lb->current then
        // there were no non-marked items between them so both should be the
        // same (no need to iterate over the same items again).
        bool top_reached = false;

        // Find first non-selected predecessor to lb->current.
        while (lb->current != NULL &&
            list_item_hashset_contains(&s, lb->current)) {
            lb->current = lm->prev(lm, lb->current);
            if (lb->current == NULL) { break; }
            top_reached |= lb->current == lb->top;
        }

        if (top_reached) {
            lb->top = lb->current;
        } else {
            // Find first non-selected predecessor to lb->top.
            while (lb->top != NULL && list_item_hashset_contains(&s, lb->top)) {
                lb->top = lm->prev(lm, lb->top);
            }
        }

        hashset_cleanup(&s);
    }
    // Case 3.b: Items are removed - based on what lb->top and lb->current
    // were set to before removal, update lb->top_pos and lb->sel_pos.
    else if (args->operation == LIST_MODEL_UPDATE_ITEMS_REMOVED) {
        uint64_t size = lm->size(lm);

        if (lb->top == NULL) { lb->top = lm->first(lm); }
        if (lb->current == NULL) { lb->current = lb->top; }

        // Model is empty, do nothing.
        if (lb->top == NULL) {
            lb->sel_pos = lb->top_pos = 0;
        } else {
            lb->top_pos = lm->index_of(lm, lb->top);

            // Deal with overscroll after remove.
            for (uint64_t diff = size - lb->top_pos; diff + 1 < lb->height;
                diff++) {
                list_item *tmp = lm->prev(lm, lb->top);
                if (tmp == NULL) { break; }
                lb->top = tmp;
                lb->top_pos--;
            }

            // Recalculate sel_pos.
            lb->sel_pos = 0;
            for (list_item *it = lb->current; it != lb->top;
                it = lm->prev(lm, it)) {
                lb->sel_pos++;
            }
        }
    } else if (args->operation == LIST_MODEL_UPDATE_ITEMS_CLEARED) {
        lb->top = lb->current = NULL;
        lb->sel_pos = lb->top_pos = 0;
    }
}

void list_box_cleanup(struct list_box *lb) {
    lb->top = lb->current = NULL;
    lb->sel_pos = lb->top_pos = 0;
    observer_unsubscribe(&lb->store_updated);
    list_box_clear_selection(lb);
    hashset_cleanup(&lb->selected_items);
    list_store_cleanup(&lb->store);
}

/* SPDX-License-Identifier: Apache-2.0 */

#include <curses.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ncurses.h>

#include "command.h"
#include "config.h"
#include "core.h"
#include "file.h"
#include "limits.h"
#include "localization.h"
#include "log.h"
#include "observer.h"
#include "option.h"
#include "types.h"
#include "u-string.h"
#include "utils.h"
#include "vector.h"

// Maybe find something more adequate?
#define CONFIG_BUFFER_SIZE (NAME_MAX + 255)

#if defined(__APPLE__) && defined(__MACH__)
#define FMT_STR "open \"%s\" 2>&1"
#elif defined(__linux__) || defined(__unix__) || defined(__UNIX__)
/* We assume that most other UNIX-es will use xdg-open. */
#define FMT_STR "xdg-open \"%s\" 2>&1"
#endif

#define RUNTIME_PATH LIBRARIAN_SYSTEM_PREFIX "/lib/librarian"

// static const char *default_view = DEFAULT_VIEW;

static const char *ORDER_VALUES[] = { "author", "title", "date", NULL };

#define CONFIG_ORDER_DEFAULT &ORDER_VALUES[0]

/* -------------------- Internal function declarations ---------------------- */

void _config_notify_observer(struct observer *observer,
    struct config_args *config_args);

static void _config_load(struct config *config);

declare_vector_type_val(option, struct option *);

// TODO: Remove ncurses dependency on color constants.
static void _config_init_fields(struct config *cfg) {
    cfg->path = PATH_INIT;
    option_vector_init_default(&cfg->options, 23);

#define init_bool_opt(ID, NAME, ABBR, DEFAULT)                                 \
    cfg->ID = (struct bool_option) { { NAME, ABBR, BOOL_OPTION }, DEFAULT };   \
    option_vector_push_back_val(&cfg->options, &cfg->ID.option);

#define init_int_opt(ID, NAME, ABBR, DEFAULT)                                  \
    cfg->ID = (struct int_option) { { NAME, ABBR, INT_OPTION }, DEFAULT };     \
    option_vector_push_back_val(&cfg->options, &cfg->ID.option);

#define init_color_opt(ID, NAME, ABBR, DEFAULT)                                \
    cfg->ID = (struct int_option) { { NAME, ABBR, COLOR_OPTION }, DEFAULT };   \
    option_vector_push_back_val(&cfg->options, &cfg->ID.option);

#define init_string_opt(ID, NAME, ABBR, DEFAULT, VALIDATOR)                    \
    cfg->ID = (struct string_option) { { NAME, ABBR, STRING_OPTION },          \
        U_STRING_INIT, VALIDATOR };                                            \
    u_string_set_c_str(&cfg->ID.value, DEFAULT);                               \
    option_vector_push_back_val(&cfg->options, &cfg->ID.option);

#define init_enum_opt(ID, NAME, ABBR, VALUES_NT, DEFAULT)                      \
    cfg->ID = (struct enum_option) { { NAME, ABBR, ENUM_OPTION }, VALUES_NT,   \
        DEFAULT };                                                             \
    option_vector_push_back_val(&cfg->options, &cfg->ID.option);

    init_bool_opt(show_line_numbers, "number", "nu", true);
    init_bool_opt(list_view_debug, "list_view_debug", "lvdbg", false);
    init_color_opt(color_highlight_bg, "color_highlight_bg", NULL, COLOR_BLUE);
    init_color_opt(color_highlight_fg, "color_highlight_fg", NULL, COLOR_WHITE);
    init_color_opt(color_list_item_bg, "color_list_item_bg", NULL, -1);
    init_color_opt(color_list_item_fg, "color_list_item_fg", NULL, 7);
    init_color_opt(color_list_item_selected_bg, "color_list_item_selected_bg",
        NULL, -1);
    init_color_opt(color_list_item_selected_fg, "color_list_item_selected_fg",
        NULL, COLOR_YELLOW);
    init_color_opt(color_list_item_active_bg, "color_list_item_active_bg", NULL,
        COLOR_WHITE);
    init_color_opt(color_list_item_active_fg, "color_list_item_active_fg", NULL,
        COLOR_BLACK);
    init_color_opt(color_list_item_selected_active_bg,
        "color_list_item_selected_active_bg", NULL, COLOR_YELLOW);
    init_color_opt(color_list_item_selected_active_fg,
        "color_list_item_selected_active_fg", NULL, COLOR_BLACK);
    init_color_opt(color_info_bg, "color_info_bg", NULL, COLOR_WHITE);
    init_color_opt(color_info_fg, "color_info_fg", NULL, COLOR_BLACK);
    init_color_opt(color_warning_bg, "color_warning_bg", NULL, COLOR_YELLOW);
    init_color_opt(color_warning_fg, "color_warning_fg", NULL, COLOR_BLACK);
    init_color_opt(color_error_bg, "color_error_bg", NULL, COLOR_RED);
    init_color_opt(color_error_fg, "color_error_fg", NULL, COLOR_WHITE);
    init_color_opt(color_text_primary, "color_text_primary", NULL, COLOR_GREEN);
    init_string_opt(command_line_token, "command_line_token", "cmdtok", ":",
        NULL);
    /*// .default_view = ENUM_OPT("default_view", NULL, views, &default_view)*/
    init_string_opt(open_command_format, "open_command_format", "opfmt",
        FMT_STR, NULL);
    init_string_opt(runtime_path, "runtime_path", "rtp", RUNTIME_PATH, NULL);
    init_enum_opt(order, "order", NULL, ORDER_VALUES, CONFIG_ORDER_DEFAULT);

#undef init_bool_opt
#undef init_int_opt
#undef init_color_opt
#undef init_string_opt
#undef init_enum_opt
}

static void _config_cleanup_fields(struct config *config) {
    u_string_cleanup(&config->command_line_token.value);
    u_string_cleanup(&config->open_command_format.value);
    u_string_cleanup(&config->runtime_path.value);
}

/* ---------------------------- Implementation ------------------------------ */

void config_init(struct config *config) {
    _config_init_fields(config);
    subject_init(&config->on_change, (notifier) _config_notify_observer);
    subject_init(&config->on_color_change, (notifier) _config_notify_observer);
    bool exists = path_base_get(&config->path, PATH_BASE_CONFIG, "config");
    if (exists && path_is_file(&config->path)) { _config_load(config); }
}

void config_cleanup(struct config *config) {
    subject_unsubscribe_all(&config->on_change);
    subject_unsubscribe_all(&config->on_color_change);
    _config_cleanup_fields(config);
    option_vector_cleanup(&config->options);
    path_cleanup(&config->path);
}
void _config_notify_observer(struct observer *observer,
    struct config_args *config_args) {
    ((void (*)(struct config_args *))(observer->notify))(config_args);
}

static void _config_load(struct config *config) {
    struct log *log = &librarian_from_component(config)->log;

    FILE *fp = fopen(config->path.str.c_str, "r");

    if (!fp) {
        const char *msg = _("Failed to open config file \"%s\" for reading.");
        log_error(log, msg, config->path);
        return;
    }

    struct command_parser cfg_parser;
    command_parser_init(&cfg_parser, librarian_from_component(config));
    commands_config_register(&cfg_parser);

    char buffer[CONFIG_BUFFER_SIZE];

    while (fgets(buffer, CONFIG_BUFFER_SIZE, fp)) {
        buffer[strcspn(buffer, "\n")] = '\0';
        // Parse config
        bool success = command_parser_execute(&cfg_parser, buffer);
        if (!success) { log_error(log, _("Failed to parse \"%s\"."), buffer); }
    }

    fclose(fp);
    command_parser_cleanup(&cfg_parser);
}

struct option *config_find_option(struct config *config, const char *name) {
    struct option **opt;
    bool found = false;
    vector_for_each_entry (opt, struct option *, config->options) {
        if (option_named(*opt, name)) {
            found = true;
            break;
        }
    }
    return found ? *opt : NULL;
}

enum config_result
config_set(struct config *config, const char *key, const char *value) {
    struct option *opt = config_find_option(config, key);

    if (!opt) { return STATUS_NO_KEY; }

    enum config_result result;

    switch (opt->type) {
        case BOOL_OPTION:
            result = config_set_bool(config, opt, value);
            break;
        case STRING_OPTION:
            result = config_set_string(config, opt, value);
            break;
        case COLOR_OPTION:
            result = config_set_color(config, opt, value);
            break;
        case INT_OPTION:
            result = config_set_int(config, opt, value);
            break;
        default: // ENUM_OPTION
            result = config_set_enum(config, opt, value);
            break;
    }

    return result;
}

enum config_result
config_set_color(struct config *config, struct option *opt, const char *value) {
    int color = c_str_to_term_color(value);
    if (color == INVALID_TERM_COLOR) { return STATUS_INVALID_COLOR; }
    int_option_set(opt, color);
    struct config_args *args = &(struct config_args) { opt };
    subject_notify_observers(&config->on_color_change, args);
    return STATUS_OK;
}

enum config_result
config_set_int(struct config *config, struct option *opt, const char *value) {
    int64_t val;
    if (sscanf(value, "%" SCNd64, &val) != 1) { return STATUS_INVALID_INT; }
    int_option_set(opt, val);
    subject_notify_observers(&config->on_change, &(struct config_args) { opt });
    return STATUS_OK;
}

enum config_result
config_set_bool_val(struct config *config, struct option *opt, bool value) {
    bool_option_set(opt, value);
    subject_notify_observers(&config->on_change, &(struct config_args) { opt });
    return STATUS_OK;
}

enum config_result
config_set_bool(struct config *config, struct option *opt, const char *value) {
    if (strcmp(value, "true") && strcmp(value, "false")) {
        return STATUS_INVALID_BOOL;
    }
    return config_set_bool_val(config, opt, *value == 't');
}

enum config_result config_set_string(struct config *config, struct option *opt,
    const char *value) {
    // TODO: Optimize this
    struct u_string tmp = U_STRING_INIT;
    u_string_set_c_str(&tmp, value);
    string_option_set(opt, &tmp);
    u_string_cleanup(&tmp);

    subject_notify_observers(&config->on_change, &(struct config_args) { opt });
    return STATUS_OK;
}

enum config_result
config_set_enum(struct config *config, struct option *opt, const char *value) {
    enum config_result result = STATUS_INVALID_ENUM;
    bool success = enum_option_set(opt, value);
    if (success) {
        subject_notify_observers(&config->on_change,
            &(struct config_args) { opt });
        result = STATUS_OK;
    }
    return result;
}

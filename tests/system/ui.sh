#!/bin/bash

source "$(dirname ${BASH_SOURCE[0]})/test-lib.sh"

set -e

readonly TEST_STATUS="/tmp/librarian-test-status"
readonly LIBRARIAN_CMD=${LIBRARIAN_CMD:-librarian}
readonly LIBRARIAN_DEFAULT_CONFIG="\
set runtime_path=\"$(realpath $(dirname ${BASH_SOURCE[0]})/../../plugins/)/pdf\"
load_plugin pdf\
"

# $1 - test name
test_dir() { echo "/tmp/librarian-test/$1"; }

# $1 - test name
test_docs_dir() { echo "$(test_dir "$1")/dummy-docs/"; }

# $1 - test name
librarian_hook() {
    printf "bash -c '%s %s ${LIBRARIAN_CMD}; echo \$? > ${TEST_STATUS}'" \
        "LIBRARIAN_CONFIG_DIR=\"$(test_dir "$1")\"" \
        "LIBRARIAN_DATA_DIR=\"$(test_dir "$1")\""
}

require "${LIBRARIAN_CMD}"

ui_setup() {
    tmux_start
    local test_dir_="$(test_dir "$1")"
    local docs_dir="$(test_docs_dir "$1")"
    mkdir -p "${test_dir_}" "${docs_dir}"
    echo "${LIBRARIAN_DEFAULT_CONFIG}" > "${test_dir_}/config"
    mkfifo -m 755 "${TEST_STATUS}"
    local dd="$1_MAKE_DUMMY_DOCS"
    if [ -n "${!dd}" ]; then create_pdf_test_dir "${docs_dir}" "${!dd}"; fi
}

ui_teardown() {
    tmux_end
    rm -f "${TEST_STATUS}"
    rm -rf "$(test_docs_dir "$1")"
}

ui_cleanup() { rm -rf "$(test_dir)"; }

# TODO: Detect when any of librarian's commands fail.

ui_start_exit() {
    tmux_send                          \
        "$(librarian_hook "$1")" ENTER \
        :q                       ENTER
    return "$(cat ${TEST_STATUS})"
}

ui_clear_library() {
    tmux_send                          \
        "$(librarian_hook "$1")" ENTER \
        :clear                   ENTER \
        :q                       ENTER
    return "$(cat ${TEST_STATUS})"
}

ui_add_documents_MAKE_DUMMY_DOCS=500
ui_add_documents() {
    tmux_send                               \
        "$(librarian_hook "$1")"      ENTER \
        :clear                        ENTER \
        ":add $(test_docs_dir "$1")"  ENTER \
        :q                            ENTER
    return "$(cat ${TEST_STATUS})"
}

ui_list_view_navigation_MAKE_DUMMY_DOCS=200
ui_list_view_navigation() {
    tmux_send                               \
        "$(librarian_hook "$1")"      ENTER \
        :clear                        ENTER \
        ":add $(test_docs_dir "$1")"  ENTER \
        G :20                         ENTER \
        gg                                  \
        :q                            ENTER
    return "$(cat ${TEST_STATUS})"
}

register_suite ui
register_suite_test ui ui_start_exit
register_suite_test ui ui_clear_library
register_suite_test ui ui_add_documents
register_suite_test ui ui_list_view_navigation

test_runner "$(basename $0)" $@

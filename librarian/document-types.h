/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_DOCUMENT_TYPES_H__
#define __LIBRARIAN_DOCUMENT_TYPES_H__

#include "document.h"
#include "hashset.h"
#include "vector.h"

declare_vector_type_val(document, struct document *);

declare_hashset_type(document, struct document *);

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#define XMP_INCLUDE_XMPFILES 1

#include <exempi-2.0/exempi/xmp.h>
#include <exempi-2.0/exempi/xmpconsts.h>

#include <librarian/core.h>
#include <librarian/document.h>
#include <librarian/format.h>
#include <librarian/log.h>
#include <librarian/plugin.h>
#include <librarian/types.h>
#include <librarian/u-string.h>
#include <librarian/utils.h>
#include <librarian/xmalloc.h>

/**
 * XMP pdf Tags
 *
 * | Tag Name     | Writable | Values / Notes      |
 * |--------------|----------|---------------------|
 * | Author       | string   |                     |
 * | Copyright    | string/  |                     |
 * | CreationDate | date     |                     |
 * | Creator      | string/  |                     |
 * | Keywords     | string   |                     |
 * | Marked       | boolean/ |                     |
 * | ModDate      | date     |                     |
 * | PDFVersion   | string   |                     |
 * | Producer     | string   |                     |
 * | Subject      | string/  |                     |
 * | Title        | string/  |                     |
 * | Trapped      | string   | 'False' = False     |
 * |              |          | 'True' = True       |
 * |              |          | 'Unknown' = Unknown |
 *
 * XMP pdfx Tags
 *
 * Extension tags, used to store application-defined PDF information.
 * User-defined tags must be created to enable writing of XMP-pdfx information.
 *
 * Taken from: https://exiftool.org/TagNames/XMP.html
 */

#define MAGIC_BYTES_PDF "%PDF-"

struct document *pdf_load_document(const char *path) {
    struct document *doc = document_new();
    u_string_set_c_str(&doc->path.str, path);

    XmpOpenFileOptions opts = XMP_OPEN_READ | XMP_OPEN_USEPACKETSCANNING;
    XmpFilePtr file = xmp_files_open_new(path, opts);

    // TODO: Handle properly later
    if (!file) { return doc; }

    XmpPtr f_xmp = xmp_files_get_new_xmp(file);
    XmpStringPtr prop = xmp_string_new();

    const char *tmp, *begin, *end;
    bool exists;

    exists = xmp_get_array_item(f_xmp, NS_DC, "creator", 1, prop, NULL);
    if (exists) {
        tmp = xmp_string_cstr(prop);
        c_str_strip_ws(tmp, &begin, &end);
        u_string_set_c_str_range(&doc->author, begin, end);
    }

    exists = xmp_get_array_item(f_xmp, NS_DC, "title", 1, prop, NULL);
    if (exists) {
        tmp = xmp_string_cstr(prop);
        c_str_strip_ws(tmp, &begin, &end);
        u_string_set_c_str_range(&doc->title, begin, end);
    }

    XmpDateTime date;
    exists = xmp_get_property_date(f_xmp, NS_XAP, "CreateDate", &date, NULL);
    if (exists) {
        doc->create_date.day = date.day;
        doc->create_date.month = date.month;
        doc->create_date.year = date.year;
    } else {
        doc->create_date = NO_DATE;
    }

    XmpCloseFileOptions close_opts = XMP_CLOSE_NOOPTION;
    xmp_string_free(prop);
    xmp_free(f_xmp);
    xmp_files_close(file, close_opts);
    xmp_files_free(file);

    return doc;
}

bool pdf_file_is_pdf(const char *path, bool *success) {
    FILE *file = fopen(path, "rb");

    if (!file) {
        if (success != NULL) { *success = false; }
        return false;
    }

    char bytes[5] = {};
    size_t length = fread(bytes, sizeof(char), 5, file);

    fclose(file);

    if (success != NULL) { *success = true; }

    return length != 5 ? false : strncmp(bytes, MAGIC_BYTES_PDF, 5) == 0;
}

// XXX: Not implemented yet
// bool pdf_update(const struct document *doc) {
//     return true;
// }

// clang-format off
struct format_provider pdf = {
    .check = pdf_file_is_pdf,
    .load = pdf_load_document,
    // .update = pdf_update,
};
// clang-format on

exit_status pdf_load(struct librarian *librarian) {
    // TODO: Provide more context.
    if (!xmp_init()) { return -1; }
    format_registry_add(&librarian->format_registry, &pdf);
    return 0;
}

exit_status pdf_unload(struct librarian *librarian) {
    (void) librarian;
    xmp_terminate();
    return 0;
}

// const struct plugin_dependency pdf_dependency_djvu = {
//     .name = "djvu",
//     // .max_version = lib_version_from_parts(0,0,1)
// };

// const struct plugin_dependency *pdf_plugin_dependencies[] = {
//     &pdf_dependency_djvu, NULL
// };

// clang-format off
struct plugin pdf_plugin = {
    .name = "PDF Plugin",
    .maintainer = "Dušan Gvozdenović",
    .email = "dusan.gvozdenovic.99@gmail.com",
    .website = "https://gvozdenovic.io",
    .version = lib_version_from_parts(0, 0, 1),
    .load = pdf_load,
    .unload = pdf_unload,
    .dependencies = NULL,
    // .dependencies = pdf_plugin_dependencies,
};
// clang-format on

plugin_export(pdf_plugin);

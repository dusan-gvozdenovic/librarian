/* SPDX-License-Identifier: Apache-2.0 */

#include <string.h>

#include "hash.h"
#include "types.h"

hash hash_str_djb2(const void *ptr) {
    hash val = 5381;
    for (const char *str = ptr; *str; str++) { val = val * 33 ^ *str; }
    return val;
}

hash hash_key_c_str(const void *ptr) {
    return hash_str_djb2(*(const char **) ptr);
}

bool hash_key_c_str_equal(const void *lhs, const void *rhs) {
    return strcmp(*(const char **) lhs, *(const char **) rhs) == 0;
}

hash hash_key_ptr(const void *ptr) {
    hash value = (hash) (*(const void **) ptr);
    return hash_int(value);
}

bool hash_key_ptr_equal(const void *lhs, const void *rhs) {
    return *(const void **) lhs == *(const void **) rhs;
}

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_VERSION_H__
#define __LIBRARIAN_VERSION_H__

#ifndef LIBRARIAN_VERSION_MAJOR
#define LIBRARIAN_VERSION_MAJOR 0x3a8ef11e
#endif

#ifndef LIBRARIAN_VERSION_MINOR
#define LIBRARIAN_VERSION_MINOR 0x3a8ef11e
#endif

#ifndef LIBRARIAN_VERSION_PATCH
#define LIBRARIAN_VERSION_PATCH 0x3a8ef11e
#endif

#include <stdio.h>

#include "types.h"
#include "utils.h"

// clang-format off

#define LIBRARIAN_VERSION_NAME                                                 \
    defer_stringize(LIBRARIAN_VERSION_MAJOR) "."                               \
    defer_stringize(LIBRARIAN_VERSION_MINOR) "."                               \
    defer_stringize(LIBRARIAN_VERSION_PATCH)

// clang-format on

typedef uint16_t lib_version_number;
typedef uint64_t lib_version;

#define LIB_VERSION_NUMBER_FORMAT_SPECIFIER "%u"

// Accounts for three lib_version_numbers
#define LIB_VERSION_STRING_LENGTH_MAX (uint_digits(UINT16_MAX) * 3 + 2 + 1)

#define LIBRARIAN_VERSION_MAJOR_MASK                                           \
    ((lib_version) ((lib_version) ((lib_version_number) - 1)                   \
        << 2 * sizeof(lib_version_number) * 8))

#define LIBRARIAN_VERSION_MINOR_MASK                                           \
    ((lib_version) (((lib_version_number) - 1)                                 \
        << sizeof(lib_version_number) * 8))

#define LIBRARIAN_VERSION_PATCH_MASK ((lib_version) ((lib_version_number) - 1))

#define LIBRARIAN_LIB_VERSION_UNSPECIFIED ((lib_version) 0)

#define lib_version_from_parts(major, minor, patch)                            \
    (lib_version)(                                                             \
        ((lib_version) (major) << 2 * sizeof(lib_version_number) * 8 |         \
            (lib_version) (minor) << sizeof(lib_version_number) * 8 |          \
            (lib_version) (patch)))

#define LIBRARIAN_VERSION                                                      \
    lib_version_from_parts(LIBRARIAN_VERSION_MAJOR, LIBRARIAN_VERSION_MINOR,   \
        LIBRARIAN_VERSION_PATCH)

static inline lib_version_number lib_version_major(const lib_version version) {
    return version & LIBRARIAN_VERSION_MAJOR_MASK;
}

static inline lib_version_number lib_version_minor(const lib_version version) {
    return version & LIBRARIAN_VERSION_MINOR_MASK;
}

static inline lib_version_number lib_version_patch(const lib_version version) {
    return version & LIBRARIAN_VERSION_PATCH_MASK;
}

static inline lib_version lib_version_significant(const lib_version version) {
    return (version &
               (LIBRARIAN_VERSION_MAJOR_MASK | LIBRARIAN_VERSION_MINOR_MASK)) >>
        (sizeof(lib_version_number) * 8);
}

static inline void lib_version_to_string(lib_version version, char *buffer) {
    sprintf(buffer,
        LIB_VERSION_NUMBER_FORMAT_SPECIFIER
        "." LIB_VERSION_NUMBER_FORMAT_SPECIFIER
        "." LIB_VERSION_NUMBER_FORMAT_SPECIFIER,
        lib_version_major(version), lib_version_minor(version),
        lib_version_patch(version));
}

#endif

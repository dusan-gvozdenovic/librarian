/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_CONFIG_H__
#define __LIBRARIAN_CONFIG_H__

#include "file.h"
#include "observer.h"
#include "option.h"
#include "types.h"
#include "vector.h"

#ifndef LIBRARIAN_CONFIG_PATH
#define LIBRARIAN_CONFIG_PATH "$HOME/.config/librarian/"
#endif

struct config {
    struct path path;
    struct subject on_change;
    struct subject on_color_change;

    struct int_option color_highlight_bg;
    struct int_option color_highlight_fg;
    struct int_option color_list_item_bg;
    struct int_option color_list_item_fg;
    struct int_option color_list_item_selected_bg;
    struct int_option color_list_item_selected_fg;
    struct int_option color_list_item_active_bg;
    struct int_option color_list_item_active_fg;
    struct int_option color_list_item_selected_active_bg;
    struct int_option color_list_item_selected_active_fg;
    struct int_option color_info_bg;
    struct int_option color_info_fg;
    struct int_option color_warning_bg;
    struct int_option color_warning_fg;
    struct int_option color_error_bg;
    struct int_option color_error_fg;
    struct int_option color_text_primary;
    struct bool_option show_line_numbers;
    struct bool_option list_view_debug;
    struct string_option colorscheme;
    struct string_option command_line_token;
    struct enum_option default_view;
    struct string_option open_command_format;
    struct string_option runtime_path;
    struct enum_option order;

    // Same options as above, but iterable.
    struct vector options; // <const struct option *>
};

struct config_args {
    const struct option *option;
};

enum config_result {
    STATUS_OK,
    STATUS_NO_KEY,
    STATUS_NOT_A_BOOL,
    STATUS_INVALID_COLOR,
    STATUS_INVALID_INT,
    STATUS_INVALID_BOOL,
    STATUS_INVALID_ENUM
};

/**
 * @related config
 * @{
 */

void config_init(struct config *config);

void config_cleanup(struct config *config);

struct option *config_find_option(struct config *config, const char *name);

/* ----------------------------- Option setters ----------------------------- */

enum config_result
config_set(struct config *config, const char *key, const char *value);

enum config_result
config_set_color(struct config *config, struct option *opt, const char *value);

enum config_result
config_set_int(struct config *config, struct option *opt, const char *value);

enum config_result
config_set_bool_val(struct config *config, struct option *opt, bool value);

enum config_result
config_set_bool(struct config *config, struct option *opt, const char *value);

enum config_result
config_set_string(struct config *config, struct option *opt, const char *value);

enum config_result
config_set_enum(struct config *config, struct option *opt, const char *value);

/**
 * @}
 */

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_PLUGIN_REGISTRY_H__
#define __LIBRARIAN_PLUGIN_REGISTRY_H__

#include "hashset.h"
#include "types.h"
#include "u-string.h"

enum plugin_load_status {
    PLUGIN_LOAD_OK,
    PLUGIN_LOAD_ALREADY_LOADED,
    PLUGIN_LOAD_API_VERSION_LOW,
    PLUGIN_LOAD_DEPENDENCY_VERSION_LOW,
    PLUGIN_LOAD_DEPENDENCY_VERSION_HIGH,
    PLUGIN_LOAD_UNSATISFIABLE_DEPENDENCY,
    PLUGIN_LOAD_INVALID_INTERFACE,
    PLUGIN_LOAD_FAILED_LOAD,
    PLUGIN_LOAD_ERROR
};

struct plugin_load_details {
    uint32_t return_code;
    enum plugin_load_status status;
    struct u_string message;
};

enum plugin_unload_status {
    PLUGIN_UNLOAD_OK,
    PLUGIN_UNLOAD_NOT_FOUND,
    PLUGIN_UNLOAD_REFERENCED,
};

void plugin_load_details_init(struct plugin_load_details *details);

void plugin_load_details_cleanup(struct plugin_load_details *details);

struct plugin_registry {
    struct hashset loaded_plugins; // <struct plugin_entry *>
};

void plugin_registry_init(struct plugin_registry *plugin_registry);

void plugin_registry_cleanup(struct plugin_registry *plugin_registry);

/**
 * Loads a plugin according to the following schema.
 *
 * Treats the c_str as a path first, expands it and takes realpath().
 * If it is successful and the file exists it tries to load it. If it does
 * not, it checks whether it has a matching extension for the platform's
 * dynamically loadable libraries and if it does, it tries to find it in the
 * runtime path. If none of this is true, it tries to find
 * lib<c_str>.<PLATFORM_DYN_EXTENSION> in the runtime path.
 *
 * @param c_str The name of the plugin to be loaded, according to the
 * described schema.
 * @return true if successful, false otherwise.
 */
bool plugin_registry_load(struct plugin_registry *plugin_registry,
    const char *c_str, struct plugin_load_details *details);

enum plugin_unload_status plugin_registry_unload(
    struct plugin_registry *plugin_registry, const char *identifier);

void plugin_registry_unload_all(struct plugin_registry *plugin_registry);

#endif

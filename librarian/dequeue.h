/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_DEQUEUE_H__
#define __LIBRARIAN_DEQUEUE_H__

#include "compiler.h"
#include "types.h"

struct dequeue_head {
    struct dequeue_head *previous, *next;
};

static inline void dequeue_init(struct dequeue_head *head) {
    head->next = head->previous = head;
}

static inline struct dequeue_head *dequeue_next(struct dequeue_head *head) {
    return head->next;
}

static inline struct dequeue_head *dequeue_previous(struct dequeue_head *head) {
    return head->previous;
}

static inline bool dequeue_empty(struct dequeue_head *head) {
    return head->next == head;
}

static inline void
dequeue_insert_front(struct dequeue_head *head, struct dequeue_head *_new) {
    _new->previous = head;
    _new->next = head->next;
    head->next->previous = _new;
    head->next = _new;
}

static inline void
dequeue_insert_back(struct dequeue_head *head, struct dequeue_head *_new) {
    _new->previous = head->previous;
    _new->previous->next = _new;
    _new->next = head;
    head->previous = _new;
}

static inline void dequeue_remove(struct dequeue_head *head) {
    head->previous->next = head->next;
    head->next->previous = head->previous;
    head->next = head->previous = NULL;
}

#define dequeue_entry(ptr, type, member) container_of(ptr, type, member)

#define dequeue_for_each_entry(pos, head, member)                              \
    for (pos = dequeue_entry((head)->next, __typeof__(*pos), member);          \
        &pos->member != (head);                                                \
        pos = dequeue_entry(pos->member.next, __typeof__(*pos), member))

#define dequeue_for_each_entry_safe(pos, tmp, head, member)                    \
    for (pos = dequeue_entry((head)->next, __typeof__(*pos), member),          \
        tmp = dequeue_entry(pos->member.next, __typeof__(*pos), member);       \
        &pos->member != (head); pos = tmp,                                     \
        tmp = dequeue_entry(pos->member.next, __typeof__(*pos), member))

#endif

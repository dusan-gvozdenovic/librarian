/* SPDX-License-Identifier: Apache-2.0 */

#include <stdlib.h>

#include "config.h"
#include "core.h"
#include "file.h"
#include "format.h"
#include "library.h"
#include "localization.h"
#include "log.h"
#include "observer.h"
#include "plugin-registry.h"
#include "xmalloc.h"

static void _librarian_init_notifier(struct observer *observer) {
    ((void (*)()) observer->notify)();
}

static void _librarian_exit_notifier(struct observer *observer,
    struct lifecycle_exit_args *args) {
    ((void (*)(struct lifecycle_exit_args *)) observer->notify)(args);
}

void librarian_init(struct librarian *librarian) {
    subject_init(&librarian->on_init, (notifier) _librarian_init_notifier);
    subject_init(&librarian->on_exit, (notifier) _librarian_exit_notifier);
    path_base_init_all();
    log_init(&librarian->log);
    format_registry_init(&librarian->format_registry);
    plugin_registry_init(&librarian->plugin_registry);
    locale_init();
    config_init(&librarian->config);
    library_init(&librarian->library);
    library_load(&librarian->library);
    subject_notify_observers(&librarian->on_init, NULL);
}

// TODO: Implement a better panicking mechanism.
void librarian_panic(int status) { exit(status); }

void librarian_exit(struct librarian *librarian, int status) {
    struct lifecycle_exit_args args = { .status = status };
    subject_notify_observers(&librarian->on_exit, &args);
    library_cleanup(&librarian->library);
    config_cleanup(&librarian->config);
    plugin_registry_cleanup(&librarian->plugin_registry);
    format_registry_cleanup(&librarian->format_registry);
    log_cleanup(&librarian->log);
    path_base_cleanup();
    exit(status);
}

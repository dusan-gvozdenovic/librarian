/* SPDX-License-Identifier: Apache-2.0 */

#include "format.h"
#include "vector.h"

declare_vector_type_val(format_provider, struct format_provider *);

void format_registry_init(struct format_registry *format_registry) {
    format_provider_vector_init_default(&format_registry->formats, 7);
}

void format_registry_cleanup(struct format_registry *format_registry) {
    format_provider_vector_cleanup(&format_registry->formats);
}

void format_registry_add(struct format_registry *format_registry,
    struct format_provider *provider) {
    format_provider_vector_push_back_val(&format_registry->formats, provider);
}

struct format_provider *format_registry_detect(
    struct format_registry *format_registry, const char *path) {
    struct format_provider **curr;
    vector_for_each_entry (curr, struct format_provider *,
        format_registry->formats) {
        if (!(*curr)) { continue; }
        bool success = false;
        if ((*curr)->check(path, &success)) { return *curr; }
        // We could issue a diagnostic log in the future (when we implement
        // support for such logs) but for now we will just ignore this value.
        (void) success;
    }
    return NULL;
}

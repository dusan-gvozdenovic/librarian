/* SPDX-License-Identifier: Apache-2.0 */

#include "serializer.h"
#include "u-string.h"
#include "utils.h"

void serializer_init(struct serializer *sz, FILE *file, bool big_endian) {
    sz->file = file;
    sz->big_endian = big_endian;
    // sz->file_end = fseek(file, 0, SEEK_END);
    // rewind(file);
}

bool serializer_write_int(struct serializer *sz, size_t size, intmax_t value) {
#ifdef BIG_ENDIAN
#define uses_big_endian __builtin_expect(sz->big_endian, true)
#else
#define uses_big_endian __builtin_expect(sz->big_endian, false)
#endif

    if (uses_big_endian) {
        char *bytes = (char *) &value;
        const size_t end = size - 1;

        for (size_t i = 0; i < size / 2; i++) {
            swap(bytes[i], bytes[end - 1]);
        }
    }

    size_t n = fwrite(&value, size, 1, sz->file);
    return n != 0;

#undef uses_big_endian
}

bool serializer_write_string(struct serializer *sz,
    const struct u_string *str) {
    return serializer_write_int(sz, sizeof(size_t), str->byte_length) &&
        (fwrite(str->c_str, 1, str->byte_length, sz->file) == str->byte_length);
}

bool serializer_read_int(struct serializer *sz, size_t size, void *value) {
    size_t n = fread(value, size, 1, sz->file);

    if (n == 0) { return false; }

    if (!sz->big_endian) { return true; }

    char *bytes = (char *) value;
    const size_t end = size - 1;

    for (size_t i = 0; i < size / 2; i++) { swap(bytes[i], bytes[end - i]); }

    return true;
}

bool serializer_write_bool(struct serializer *sz, bool value) {
    return serializer_write_int(sz, sizeof(bool), value);
}

bool serializer_read_bool(struct serializer *sz, bool *value) {
    return serializer_read_int(sz, sizeof(bool), value);
}

bool serializer_read_string(struct serializer *sz, struct u_string *str) {
    size_t byte_length = 0;
    return serializer_read_int(sz, sizeof(size_t), &byte_length) &&
        ((byte_length == 0) || u_string_read(str, sz->file, byte_length));
}

bool serializer_has_more(struct serializer *sz) {
    return !feof(sz->file);
    // return ftell(sz->file) == sz->file_end;
}

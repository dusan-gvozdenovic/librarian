/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_DATE_H__
#define __LIBRARIAN_DATE_H__

// BUG: For some reason IWYU wants to remove it although uints are used below.
#include "types.h" // IWYU pragma: keep

struct date {
    uint8_t day;
    uint8_t month;
    uint16_t year;
};

static const struct date NO_DATE = { 0U, 0U, 0U };

#define is_no_date(x) (!(x).day && !(x).month && !(x).year)

static inline int date_compare(const struct date *lhs, const struct date *rhs) {
    int cmp;
    cmp = lhs->year - rhs->year;
    if (cmp) { return cmp; }

    cmp = lhs->month - rhs->month;
    if (cmp) { return cmp; }

    cmp = lhs->day - rhs->day;
    return cmp;
}

#endif

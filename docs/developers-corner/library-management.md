Library Management {#library-management}
========================================

Currently librarian manages one central repository of documents called the
library.

We would want want to look at multiple libraries, hiding some of the irrelevant
documents.

Things to be considered:
- Searching/Filtering
- Feature interoperability

# Views

- One central library
- Views that reference central library's documents

# Multiple libraries

# Document Tagging

e.g. _The Art of Computer Programming [Mathematics] [Computer Science]_

# Archive libraries

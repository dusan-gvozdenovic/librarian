/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __TEST_H__
#define __TEST_H__

#include <stdio.h>

#include <CUnit/Automated.h>
#include <CUnit/Basic.h>
#include <CUnit/Console.h>
#include <CUnit/CUnit.h>

// https://gitlab.com/cunity/cunit/-/wikis/CUnit-JUnit
static inline int test_main(char *runner_name, int argc, char **argv) {
    if (argc == 1) {
        CU_basic_run_tests();
        goto end;
    }
    if (strcmp(argv[1], "--console") == 0) {
        CU_console_run_tests();
    } else if (strcmp(argv[1], "--junit") == 0) {
        CU_set_output_filename(runner_name);
        CU_automated_enable_junit_xml(CU_TRUE);
        CU_automated_run_tests();
    } else {
        fprintf(stderr, "Usage: %s [--junit|--console]", argv[0]);
    }
end:
    CU_cleanup_registry();
    return CU_get_error();
}

/* ----------------------------- Utility macros ----------------------------- */

#define suite_name(name) __suite_##name

// Needed because VS Code is complaining about constructor attribute not taking
// parameters.
#ifdef __INTELLISENSE__
#define attr_constructor(priority)
#else
#define attr_constructor(priority) __attribute__((__constructor__(priority)))
#endif

#define initialize_runner(runner)                                              \
    static CU_pSuite suite_name(def);                                          \
    static void attr_constructor(101) __setup_cunit(void) {                    \
        CU_initialize_registry();                                              \
    };                                                                         \
                                                                               \
    int main(int argc, char *argv[]) { return test_main((runner), argc, argv); }

#define register_test_suite(name, setup, teardown)                             \
    static CU_pSuite suite_name(name);                                         \
    void attr_constructor(102) __register_test_##name(void) {                  \
        suite_name(name) = CU_add_suite_with_setup_and_teardown(#name, 0, 0,   \
            (setup), (teardown));                                              \
    };

#define register_test(suite, test)                                             \
    void attr_constructor(103)                                                 \
        __register_test_##test##_with_suite_##suite(void) {                    \
        CU_add_test(suite_name(suite), #test, test);                           \
    }

#define assert(prop) CU_ASSERT(prop)
#define assert_false(prop) CU_ASSERT_FALSE(prop)
#define assert_equal(lhs, rhs) CU_ASSERT_EQUAL((lhs), (rhs))
#define assert_c_str_equal(lhs, rhs) CU_ASSERT_STRING_EQUAL((lhs), (rhs))

#endif

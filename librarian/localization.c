/* SPDX-License-Identifier: Apache-2.0 */

#include <string.h>

#include <libintl.h>
#include <locale.h>

#include "localization.h"
#include "types.h"
#include "utf8.h"

#define MONTH_LENGTH 21

static bool _initialized = false;

/**
 * @todo Issue a warning here if it is disabled.
 */
void locale_init(void) {
#ifdef LIBRARIAN_LOCALIZATION_ENABLED
    if (_initialized) { return; }
    setlocale(LC_ALL, "");
    bindtextdomain("librarian", LIBRARIAN_TEXTDOMAIN);
    textdomain("librarian");
    _initialized = true;
#endif
}

static char months[12][MONTH_LENGTH] = { [0 ... 11] = "" };

char *month_name(size_t month) {
    if (month == 0 || month > 12) { return NULL; }
    char *name = months[month - 1];
    if (name[0] != '\0') { return name; }
    // clang-format off
    strncpy(name,
              month == 1  ? _("January")
            : month == 2  ? _("February")
            : month == 3  ? _("March")
            : month == 4  ? _("April")
            : month == 5  ? _("May")
            : month == 6  ? _("June")
            : month == 7  ? _("July")
            : month == 8  ? _("August")
            : month == 9  ? _("September")
            : month == 10 ? _("October")
            : month == 11 ? _("November")
                          : _("December"),
        MONTH_LENGTH - 1);
    // clang-format on
    return name;
}

static int32_t _m_len[12] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
static int32_t _m_wth[12] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
static int32_t _lm_len = 0;
static int32_t _lm_wth = 0;

int32_t month_name_length(size_t month) {
    if (month == 0 || month > 12) { return -1; }
    if (_m_len[month - 1] == -1) {
        _m_len[month - 1] = u8_strlen(month_name(month));
    }
    return _m_len[month - 1];
}

int32_t month_name_width(size_t month) {
    if (month == 0 || month > 12) { return -1; }
    if (_m_wth[month - 1] == -1) {
        _m_wth[month - 1] = (int32_t) strlen(month_name(month));
    }
    return _m_wth[month - 1];
}

int32_t longest_month_name_length(void) {
    if (_lm_len) { return _lm_len; }
    for (size_t i = 0; i < 12; i++) {
        size_t len = month_name_length(i + 1);
        if (len > _lm_len) { _lm_len = len; }
    }
    return _lm_len;
}

int32_t longest_month_name_width(void) {
    if (_lm_wth) { return _lm_wth; }
    for (size_t i = 0; i < 12; i++) {
        size_t wth = u8_strlen(month_name(i + 1));
        if (wth > _lm_wth) { _lm_wth = wth; }
    }
    return _lm_wth;
}

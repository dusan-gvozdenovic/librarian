# Applies command $1 over the rest of the arguments.
apply() { for x in ${*:2}; do $1 $x; done }

run_if_defined() { if [ "$(type -t $1)" = "function" ]; then $1; fi; }

# Exits if the given command $1 cannot be executed.
require() {
    _req_one() {
        if ! command -v "$1" &>/dev/null; then
            printf "Error: program '%s' is not found!\n" "$1" >&2
            exit 1
        fi
    }
    apply _req_one "$@"
}

# Places a key-value pair $2:$3 into a map $1.
# $1 - map id
# $2 - key
# $3 - value
m_put() { eval "__bash_3_map_$1_$2=$3"; }

# Retrieves a value associated with key $2 from a map $1.
# $1 - map id
# $2 - key
m_get() { local tmp_="__bash_3_map_$1_$2"; echo "${!tmp_}"; }

# Increments a numeric value associated with key $2 in map $1.
m_inc() {
    local tmp_="__bash_3_map_$1_$2"
    eval "${tmp_}=$(( ${!tmp_:-0} + 1 ))"
}

# m_clr() { set - o posix; set | grep -Eo "(__bash_3_map.*)="; }

# ---------------------------- testing utilities ----------------------------- #

SUITES=()
declare -i TESTS_EXECUTED

# $1 - suite
register_suite() { SUITES+=("$1"); }

# $1 - suite
# $2 - test
register_suite_test() {
    local suite_tests_="$1_TESTS"
    eval "${suite_tests_}+=($2)"
}

# $1 - suite
suite_tests() { eval "echo $(echo \${$1_TESTS[@]})"; }

suite_succeeded() { :; }

# $1 - dmd.ds
time_to_secs() {
    echo "$1" | sed "s/m/ /g" | sed "s/s//g" | \
        awk '{ printf "%f", $1 * 60 + $2; }'
}

# $1 - test name
# $2 - setup
# $3 - teardown
run_test() {
    [ -n "$2" ] && $2 "$1"
    { time $1 $1 1>/tmp/test_stdout_ 2>/tmp/test_stderr_; } 2> /tmp/time_
    local status="$?"
    [ -n "$3" ] && $3 "$1"
    local rtime=$(cat /tmp/time_ | grep real | sed 's/\t/ /g' | cut -d ' ' -f 2)
    eval "$1_STATUS=${status}"
    eval "$1_TIME=\$(time_to_secs ${rtime})"
    eval "$1_STDOUT='$(cat /tmp/test_stdout_)'"
    eval "$1_STDERR='$(cat /tmp/test_stderr_)'"
    rm /tmp/time_
    rm /tmp/test_stdout_
    rm /tmp/test_stderr_
    TESTS_EXECUTED=$(( TESTS_EXECUTED + 1 ))
    if [ "${status}" -eq 0 ]; then
        echo -ne "PASS\t"
    else
        echo -ne "FAIL\t"
    fi
    echo "$1"
    return ${status}
}

# $1 - suite name
run_suite() {
    local suite="$1"
    local init="${suite}_init" cleanup="${suite}_cleanup"
    local setup="${suite}_setup" teardown="${suite}_teardown"
    run_if_defined "${init}"
    _run_impl() {
        if ! run_test "$1" "${setup}" "${teardown}"; then
            m_inc "SUITE_FAILED" "${suite}"
        fi
    }
    apply _run_impl $(suite_tests "${suite}")
    run_if_defined "${cleanup}"
}

run_all_tests() {
    TESTS_EXECUTED=0
    run_if_defined initialize
    apply run_suite "${SUITES[@]}"
    run_if_defined cleanup
}

# ------------------------- junit report generation -------------------------- #

declare -i JUNIT_XML_INDENT

junit_xml_header() {
    JUNIT_XML_INDENT=0
    echo '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>' \
        > "${JUNIT_OUTPUT}"
}

junit_xml_print_indent() {
    printf "%*.0s" "$(( JUNIT_XML_INDENT * 4 ))" "" >> "${JUNIT_OUTPUT}"
}

junit_xml_single() {
    local args="${*:2}"
    if [ -n "${args}" ]; then args=" ${args}"; fi
    junit_xml_print_indent
    echo $"<$1${args:-}/>" >> "${JUNIT_OUTPUT}"
}

junit_xml_start() {
    local args="${*:2}"
    if [ -n "${args}" ]; then args=" ${args}"; fi
    junit_xml_print_indent
    if [ "${inline}" = "yes" ]; then
        local echo_args="-n"
    fi
    echo "${echo_args}" $"<$1${args:-}>" >> "${JUNIT_OUTPUT}"
    JUNIT_XML_INDENT=$(( JUNIT_XML_INDENT + 1 ))
}

junit_xml_end() {
    JUNIT_XML_INDENT=$(( JUNIT_XML_INDENT - 1 ))
    if [ "${inline}" != "yes" ]; then
        junit_xml_print_indent
    fi
    echo $"</$1>" >> "${JUNIT_OUTPUT}";
}

# $1 - name
# $2 - content
junit_print_output_if_not_empty() {
    if [ -n "$2" ]; then
        inline="yes" junit_xml_start "$1"
        echo -n "$2" >> "${JUNIT_OUTPUT}"
        inline="yes" junit_xml_end "$1"
    fi
}

# $1 - suite
# $2 - test name
junit_report_test() {
    local status_var="$2_STATUS" time_var="$2_TIME"
    local stdout_var="$2_STDOUT" stderr_var="$2_STDERR"
    if [ -z "${!status_var}" ]; then return; fi

    if [ -z "${!stdout_var}" ] && [ -z "${!stderr_var}" ]; then
        junit_xml_single "testcase" name="\"$2\"" time="\"${!time_var}\""
    else
        junit_xml_start "testcase" name="\"$2\""
        junit_print_output_if_not_empty "system-out" "${!stdout_var}"
        junit_print_output_if_not_empty "system-err" "${!stderr_var}"
        junit_xml_end "testcase"
    fi
}

junit_report() {
    junit_xml_header
    junit_xml_start "testsuites"      \
        name="\"System tests\""       \
        tests="\"${TESTS_EXECUTED}\""

    for suite in "${SUITES[@]}"; do
        local failures="$(m_get "SUITE_FAILED" ${suite})"
        junit_xml_start "testsuite" \
            name="\"${suite}\""     \
            failures="\"${failures:-0}\""

        for test in $(suite_tests "${suite}"); do
            junit_report_test "${suite}" "${test}"
        done
        junit_xml_end "testsuite"
    done
    junit_xml_end "testsuites"
}

test_runner() {
    SCRIPT_NAME=$1
    shift
    local OPTIND o a
    while getopts "f:hj:" opt; do
        case "${opt}" in
          j)
            JUNIT_OUTPUT="${OPTARG}"
            mkdir -p "$(dirname ${JUNIT_OUTPUT})"
            ;;
          f)
            TEST_FILTER="${OPTARG}"
            ;;
          :)
            echo "Error: -${OPTARG} requires an argument." >&2
            exit
            ;;
          h|*)
            echo "Usage: ${SCRIPT_NAME} [-f=FILTER_REGEX] [-j=JUNIT_OUTPUT]" >&2
            exit
            ;;
        esac
    done
    run_all_tests
    if [ -n "${JUNIT_OUTPUT}" ]; then junit_report; fi
}

# ------------------------------ tmux utilities ------------------------------ #

require tmux

_TMUX_SESSION="librarian-test"

tmux_start() { tmux -u new-session -d -x 800 -y 600 -s "${_TMUX_SESSION}"; }

tmux_end() { tmux kill-session -t "${_TMUX_SESSION}"; }

tmux_send() { IFS=$"\n" tmux send -t "${_TMUX_SESSION}" "$@"; }

# ---------------------------- document utilities ---------------------------- #

require convert

# $1 - dir
create_pdf() { convert xc:none -page Letter "$1"; }

# Creates a directory containing $2-number of (dummy) pdfs
# named i.pdf.
# $1 - dir
# $2 - count
create_pdf_test_dir() {
    local dir="$1"
    mkdir -p "${dir}";
    _create_pdf() { create_pdf "${dir}/$1.pdf"; }
    apply _create_pdf $(seq 1 "$2")
}

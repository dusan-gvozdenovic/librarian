/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __WELCOME_H__
#define __WELCOME_H__

#include <librarian/types.h>

#include "ui-events.h"

#define WELCOME_NAME "welcome"
#define WELCOME_N_COMMANDS 3

struct welcome {
    struct ui_events ui_events;
    int32_t pos;
    int32_t command_line_width;
    char *commands[WELCOME_N_COMMANDS][2];
};

extern struct welcome welcome;

void welcome_init(void);

void welcome_write_empty_line(void);

void welcome_write_line(char *str);

void welcome_write_line_bold(char *str);

void welcome_writeln_command(char *cmd, char *desc);

void welcome_paint(void);

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_FORMAT_H__
#define __LIBRARIAN_FORMAT_H__

#include "types.h"
#include "vector.h"

struct format_provider {
    bool (*check)(const char *file, bool *success);
    struct document *(*load)(const char *file);
    // bool (*update)(const struct document *doc);
};

// XXX: Can we make this private?
struct format_registry {
    struct vector formats;
};

void format_registry_init(struct format_registry *format_registry);

void format_registry_cleanup(struct format_registry *format_registry);

void format_registry_add(struct format_registry *format_registry,
    struct format_provider *provider);

struct format_provider *format_registry_detect(
    struct format_registry *format_registry, const char *path);

#endif

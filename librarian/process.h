/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_PROCESS_H__
#define __LIBRARIAN_PROCESS_H__

typedef int exit_code;

struct process {
    char *cmd;
    char * const *argv; // null-terminated list of args
    void (*on_child_exit)(struct process *self, exit_code ec);
    void (*on_child_exit_non_zero)(struct process *self, exit_code ec);
};

enum exec_status {
    PROCESS_OK,
    PROCESS_FORK_FAIL,
    PROCESS_WAIT_FAIL,
    PROCESS_PREMATURE_HOOK_FAIL
};

enum exec_status process_execute(struct process *proc, exit_code *ex_code);

#endif

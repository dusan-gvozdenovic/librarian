/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_H__
#define __LIBRARIAN_H__

#include <librarian/config.h>
#include <librarian/core.h>
#include <librarian/log.h>
#include <librarian/types.h>

#define HIGHLIGHT_COLOR 1
#define LIST_ITEM_COLOR 2
#define LIST_ITEM_SELECTED_COLOR 3
#define LIST_ITEM_ACTIVE_COLOR 4
#define LIST_ITEM_SELECTED_ACTIVE_COLOR 5
#define INFO_COLOR 6
#define WARNING_COLOR 7
#define ERROR_COLOR 8
#define TEXT_PRIMARY_COLOR 9
#define LINE_COLOR 10

#define list_item_color_choose(active, selected)                               \
    (LIST_ITEM_COLOR + (((active) << 1) | (selected)))

enum mode { COMMAND, NORMAL, SEARCH };

struct librarian_ncurses {
    enum mode mode;
    void (*mode_func)(void);
    bool ui_initialized;
};

extern struct librarian librarian;
extern struct librarian_ncurses librarian_ncurses;

void init_main_window(void);

void init_colors(void);

void color_changed(struct config_args *args);

enum mode get_mode(void);

void set_mode(enum mode mode);

void handle_sigint(int signum);

void handle_sighup(int signum);

void handle_sigwinch(int signum);

void handle_log_flush(struct log *log, enum log_mode mode);

#endif

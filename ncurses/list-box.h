/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIST_BOX_H__
#define __LIST_BOX_H__

#include <ncurses.h>

#include <librarian/hashset.h>
#include <librarian/observer.h>
#include <librarian/types.h>

#include "list-store.h"

struct list_box {
    uint32_t width, height;
    uint32_t pos_y, pos_x;
    uint64_t top_pos;
    uint32_t sel_pos;
    bool line_numbers;

    list_item *top;
    list_item *current;

    WINDOW *window;

    struct list_store store;

    struct observer store_updated;

    struct hashset selected_items;

    void (*write_line)(struct list_box *lb, list_item current);
};

void list_box_init(struct list_box *lb, struct list_item_properties props);

void list_box_paint(struct list_box *lb);

void list_box_handle_resize(struct list_box *lb, uint32_t height,
    uint32_t width);

void list_box_up(struct list_box *lb);

void list_box_down(struct list_box *lb);

void list_box_top(struct list_box *lb);

void list_box_bottom(struct list_box *lb);

void list_box_goto_line(struct list_box *lb, int64_t line);

void list_box_select_all(struct list_box *lb);

void list_box_toggle_select(struct list_box *lb, list_item item);

bool list_box_selected(const struct list_box *lb, list_item item);

void list_box_clear_selection(struct list_box *lb);

void list_box_cleanup(struct list_box *lb);

#endif

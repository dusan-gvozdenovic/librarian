/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_COMPILER_H__
#define __LIBRARIAN_COMPILER_H__

#include <stddef.h>

/* Optimization: Condition @x is likely */
#define likely(x) __builtin_expect(!!(x), 1)

/* Optimization: Condition @x is unlikely */
#define unlikely(x) __builtin_expect(!!(x), 0)

#define container_of_portable(ptr, type, member)                               \
    ((type *) (void *) ((char *) (ptr) - offsetof(type, member)))

#undef container_of

#if defined(__GNUC__)
/**
 * @brief Casts a member of a structure out to the containing structure.
 *
 * @param ptr The pointer to the member.
 * @param type The type of the container struct this is embedded in.
 * @param member The name of the member within the struct.
 */
#define container_of(ptr, type, member)                                        \
    __extension__({                                                            \
        const __typeof__(((type *) 0)->member) *_mptr = (ptr);                 \
        container_of_portable(_mptr, type, member);                            \
    })
#else
/// @copydoc container_of
#define container_of(ptr, type, member) container_of_portable(ptr, type, member)
#endif

#define container_of_safe(ptr, type, member)                                   \
    __extension__({                                                            \
        const __typeof__(((type *) 0)->member) *____mptr = (ptr);              \
        ____mptr ? container_of(ptr, type, member) : NULL;                     \
    })

#define stringize(X) #X

#define defer_stringize(X) stringize(X)

#define attr_format(archetype, format_index, first_to_check)                   \
    __attribute__((format(archetype, format_index, first_to_check)))

#define attr_noreturn __attribute__((__noreturn__))

#define attr_unused __attribute__((__unused__))

#define export_symbol __attribute__((__visibility__("default")))

#endif

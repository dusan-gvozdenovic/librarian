/* SPDX-License-Identifier: Apache-2.0 */

#include <stdarg.h>
#include <string.h>

#include "types.h"
#include "u-string.h"
#include "utf8.h"
#include "utils.h"
#include "xmalloc.h"

int u_string_compare(const struct u_string *str1, const struct u_string *str2) {
    return c_str_cmp_null_safe(str1->c_str, str2->c_str);
}

bool u_string_empty(const struct u_string *str) { return str->length == 0; }

bool u_string_set_c_str_n(struct u_string *str, const char *c_str,
    size_t byte_length) {
    if (!str || !c_str) { return false; }

    const size_t size = byte_length + 1;

    if (!str->c_str) {
        str->capacity = size;
        str->c_str = xnew_n(char, size);
    } else if (str->capacity < size) {
        str->capacity = size;
        str->c_str = xrealloc(str->c_str, size * sizeof(char));
    }

    (void) strncpy(str->c_str, c_str, size);
    str->c_str[byte_length] = '\0';

    str->byte_length = size - 1;
    str->length = u8_strlen(str->c_str);

    return true;
}

bool u_string_set_c_str(struct u_string *str, const char *c_str) {
    return u_string_set_c_str_n(str, c_str, strlen(c_str));
}

bool u_string_set_c_str_range(struct u_string *str, const char *begin,
    const char *end) {
    return u_string_set_c_str_n(str, begin, end - begin);
}

void u_string_set(struct u_string *destination, const struct u_string *source) {
    if (destination->c_str == NULL) {
        destination->c_str = xnew_n(char, source->capacity);
        destination->capacity = source->capacity;
    } else if (source->capacity > destination->capacity) {
        destination->c_str = xrenew(char, destination->c_str, source->capacity);
        destination->capacity = source->capacity;
    }
    strcpy(destination->c_str, source->c_str);
    destination->byte_length = source->byte_length;
    destination->length = source->length;
}

void u_string_append_c_str(struct u_string *str, const char *c_str,
    size_t length) {
    const size_t required = str->byte_length + length + 1;

    if (str->c_str == NULL) {
        u_string_set_c_str_n(str, c_str, length);
        return;
    }

    if (required > str->capacity) {
        str->capacity = required;
        str->c_str = xrenew(char, str->c_str, required);
    }

    strncpy(str->c_str + str->byte_length, c_str, length + 1);
    str->byte_length += length;
    str->length += u8_strlen(c_str);
}

ssize_t
u_string_vprintf(struct u_string *str, const char *format, va_list arg) {
    va_list tmp;
    va_copy(tmp, arg);
    ssize_t length = vsnprintf(NULL, 0, format, tmp);
    va_end(tmp);

    if (length < 0) { goto exit; }

    str->byte_length = length;
    str->capacity = length + 1;

    if (str->c_str == NULL) {
        str->c_str = xnew_n(char, str->capacity);
    } else {
        str->c_str = xrenew(char, str->c_str, str->capacity);
    }

    length = vsprintf(str->c_str, format, arg);

    if (length < 0) { goto exit; }

    str->c_str[str->byte_length] = '\0';
    str->length = u8_strlen(str->c_str);

exit:
    return length;
}

ssize_t u_string_printf(struct u_string *str, const char *format, ...) {
    va_list ap;
    va_start(ap, format);
    ssize_t length = u_string_vprintf(str, format, ap);
    va_end(ap);
    return length;
}

bool u_string_read(struct u_string *str, FILE *file, size_t byte_length) {
    str->byte_length = byte_length;
    const size_t size = byte_length + 1;

    if (!str->c_str) {
        str->capacity = size;
        str->c_str = xnew_n(char, size);
    } else if (str->capacity < size) {
        str->capacity = size;
        str->c_str = xrealloc(str->c_str, size * sizeof(char));
    }

    size_t n = fread(str->c_str, sizeof(char), str->byte_length, file);
    bool success = (ferror(file) == 0);
    if (!success) { return false; }
    str->c_str[n] = '\0';
    str->length = u8_strlen(str->c_str);
    return n == str->byte_length;
}

void u_string_cleanup(struct u_string *str) {
    xfree_ptr(&str->c_str);
    str->capacity = 0;
    str->byte_length = 0;
    str->length = 0;
}

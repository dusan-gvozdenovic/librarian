/* SPDX-License-Identifier: Apache-2.0 */

#include <librarian/compiler.h>
#include <librarian/log.h>
#include <librarian/observer.h>
#include <librarian/rbtree.h>
#include <librarian/types.h>
#include <librarian/vector.h>
#include <librarian/xmalloc.h>

#include "librarian.h"
#include "list-store.h"

/* ------------------------- Private implementation ------------------------- */

struct _item_node {
    struct rbnode node;
    list_item item;
};

static inline struct _item_node *_item_node_new_from_list_item(list_item item) {
    struct _item_node *node = xnew(struct _item_node);
    node->node = RBNODE_INIT;
    node->item = item;
    return node;
}

static inline void _item_node_destroy(struct _item_node **node) {
    xfree_ptr(node);
}

struct list_store_priv {
    struct rbtree root;
    size_t size;
};

static void _list_model_updated_notifier(struct observer *observer,
    struct list_model_updated_args *args) {
    ((void (*)(struct list_model_updated_args *)) observer->notify)(args);
}

static void _list_store_tree_add_node(struct rbtree *tree,
    struct _item_node *node, int (*ord)(const void *lhs, const void *rhs)) {
    struct rbnode **ln = &tree->root, *parent = RBTREE_NULL;

    while (*ln != RBTREE_NULL) {
        parent = *ln;
        struct _item_node *curr = rbtree_entry(parent, struct _item_node, node);
        ln = ord(node->item, curr->item) > 0 ? &(**ln).right : &(**ln).left;
    }

    rbnode_link(&node->node, parent, ln);
    rbtree_insert_fixup(tree, &node->node);
}

static void _list_store_reorder(struct list_store *store,
    struct rbtree *tree_new, struct rbnode **node) {
    if (*node == RBTREE_NULL) { return; }
    if ((*node)->left != RBTREE_NULL) {
        _list_store_reorder(store, tree_new, &(*node)->left);
    }
    if ((*node)->right != RBTREE_NULL) {
        _list_store_reorder(store, tree_new, &(*node)->right);
    }
    // We can safely do this as we are destroying the old tree anyway.
    (**node).parent = (**node).left = (**node).right = RBTREE_NULL;
    // Add the existing node into the new tree.
    _list_store_tree_add_node(tree_new,
        container_of(*node, struct _item_node, node),
        store->parent.props.order_relation);
}

static void _list_store_free(struct list_store *store, struct rbnode **node) {
    if (*node == RBTREE_NULL) { return; }
    if ((*node)->left != RBTREE_NULL) {
        _list_store_free(store, &(*node)->left);
    }
    if ((*node)->right != RBTREE_NULL) {
        _list_store_free(store, &(*node)->right);
    }
    struct _item_node *it = container_of(*node, struct _item_node, node);
    // We can safely do this as we are destroying the tree anyway.
    (**node).parent = (**node).left = (**node).right = RBTREE_NULL;
    list_item_unref(&store->parent, &it->item);
    _item_node_destroy(&it);
}

/* ----------------------- list_model implementation ------------------------ */

static list_item *
_list_store_next(struct list_model *model, list_item *current) {
    struct rbnode *next =
        rbtree_next(&container_of(current, struct _item_node, item)->node);
    return next != RBTREE_NULL
        ? &container_of(next, struct _item_node, node)->item
        : NULL;
}

static list_item *
_list_store_prev(struct list_model *model, list_item *current) {
    struct rbnode *prev =
        rbtree_previous(&container_of(current, struct _item_node, item)->node);
    return prev != RBTREE_NULL
        ? &container_of(prev, struct _item_node, node)->item
        : NULL;
}

static list_item *_list_store_first(struct list_model *model) {
    struct list_store *store = container_of(model, struct list_store, parent);
    if (store->priv->size == 0) { return NULL; }
    struct rbnode *first = rbtree_first(&store->priv->root);
    return &container_of(first, struct _item_node, node)->item;
}

static list_item *_list_store_last(struct list_model *model) {
    struct list_store *store = container_of(model, struct list_store, parent);
    if (store->priv->size == 0) { return NULL; }
    struct rbnode *last = rbtree_last(&store->priv->root);
    return &container_of(last, struct _item_node, node)->item;
}

static list_item *_list_store_at(struct list_model *model, size_t position) {
    // XXX: Not implemented yet!
    return NULL;
}

static size_t _list_store_index_of(struct list_model *model, list_item *item) {
    size_t index_of = 0;
    const struct rbnode * const first = rbtree_first(
        &container_of(model, struct list_store, parent)->priv->root);
    struct rbnode *it = &container_of(item, struct _item_node, item)->node;
    for (; it != first; it = rbtree_previous(it)) { index_of++; }
    return index_of;
}

static size_t _list_store_size(struct list_model *model) {
    return container_of(model, struct list_store, parent)->priv->size;
}

/* -------------------------- list_store interface -------------------------- */

void list_store_init(struct list_store *store,
    struct list_item_properties props) {
    store->priv = xnew(struct list_store_priv);
    store->priv->size = 0;
    store->priv->root = RBTREE_INIT;
    store->parent = (struct list_model) {
        .next = _list_store_next,
        .prev = _list_store_prev,
        .first = _list_store_first,
        .last = _list_store_last,
        .at = _list_store_at,
        .index_of = _list_store_index_of,
        .size = _list_store_size,
        .props = props,
    };
    subject_init(&store->parent.updated,
        (notifier) _list_model_updated_notifier);
}

// XXX: For now a silent operation -- will require some moving of furniture to
// make it notify observers too.
void list_store_add(struct list_store *store, list_item item) {
    struct _item_node *new_node = _item_node_new_from_list_item(item);
    list_item_ref(item);
    _list_store_tree_add_node(&store->priv->root, new_node,
        store->parent.props.order_relation);
    store->priv->size++;
}

void list_store_add_items(struct list_store *store, struct vector *items) {
    list_item *it;
    vector_for_each_entry (it, list_item, *items) {
        list_store_add(store, *it);
    }
    struct list_model_updated_args args = {
        .self = &store->parent,
        .operation = LIST_MODEL_UPDATE_ITEMS_ADDED,
        .items = *items,
    };
    subject_notify_observers(&store->parent.updated, &args);
}

// XXX: For now a silent operation -- will require some moving of furniture to
// make it notify observers too.
void list_store_remove(struct list_store *store, list_item *item) {
    struct _item_node *to_del = container_of(item, struct _item_node, item);
    rbtree_delete(&store->priv->root, &to_del->node);
    list_item_unref(&store->parent, &to_del->item);
    _item_node_destroy(&to_del);
    store->priv->size--;
}

void list_store_remove_items(struct list_store *store, struct vector *items) {
    struct list_model_updated_args args = {
        .self = &store->parent,
        .operation = LIST_MODEL_UPDATE_ITEMS_ON_REMOVE,
        .items = *items,
    };
    subject_notify_observers(&store->parent.updated, &args);

    list_item *it;
    vector_for_each_entry (it, list_item, *items) {
        list_item *rm = list_store_find(store, *it);
        if (unlikely(rm == NULL)) {
            log_warn(&librarian.log, "List item not found!");
            continue;
        }
        list_store_remove(store, rm);
    }

    args.operation = LIST_MODEL_UPDATE_ITEMS_REMOVED;

    subject_notify_observers(&store->parent.updated, &args);
}

void list_store_clear(struct list_store *store) {
    _list_store_free(store, &store->priv->root.root);
    store->priv->root = RBTREE_INIT;
    store->priv->size = 0;
    subject_notify_observers(&store->parent.updated,
        &(struct list_model_updated_args) {
            .self = &store->parent,
            .operation = LIST_MODEL_UPDATE_ITEMS_CLEARED,
        });
}

list_item *list_store_find(struct list_store *store, list_item item) {
    struct rbnode **link = &store->priv->root.root;
    while (*link != RBTREE_NULL) {
        struct _item_node *curr = rbtree_entry(*link, struct _item_node, node);
        int cmp = store->parent.props.order_relation(item, curr->item);
        if (cmp == 0) { return &curr->item; }
        link = cmp > 0 ? &(**link).right : &(**link).left;
    }
    return NULL;
}

void list_store_reorder(struct list_store *store,
    int (*order)(const void *lhs, const void *rhs)) {
    store->parent.props.order_relation = order;
    struct rbtree old_tree = store->priv->root;
    struct rbtree new_tree = RBTREE_INIT;
    _list_store_reorder(store, &new_tree, &old_tree.root);
    store->priv->root = new_tree;
    subject_notify_observers(&store->parent.updated,
        &(struct list_model_updated_args) {
            .self = &store->parent,
            .operation = LIST_MODEL_UPDATE_ORDER_CHANGED,
        });
}

void list_store_cleanup(struct list_store *store) {
    subject_unsubscribe_all(&store->parent.updated);
    _list_store_free(store, &store->priv->root.root);
    xfree_ptr(&store->priv);
}

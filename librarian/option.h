/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_OPTION_H__
#define __LIBRARIAN_OPTION_H__

#include <string.h>

#include "compiler.h"
#include "types.h"
#include "u-string.h"

enum option_type {
    BOOL_OPTION,
    INT_OPTION,
    COLOR_OPTION,
    STRING_OPTION,
    ENUM_OPTION,
};

struct option {
    char *name;
    char *abbr;
    enum option_type type;
};

/** @extends option */
struct bool_option {
    struct option option;
    bool value;
};

/** @extends option */
struct string_option {
    struct option option;
    struct u_string value;
    bool (*validator)(const struct u_string *value);
};

/**
 * Option wrapper around an enum
 * @extends option
 */
struct enum_option {
    struct option option;
    /** A NULL-terminated array of const strings. */
    const char * const *allowed_values;
    /** A pointer to a current value in @ref allowed_values */
    const char * const *value;
};

/** @extends option */
struct int_option {
    struct option option;
    int64_t value;
};

/**
 * @related option
 * @{
 */

/**
 * A type-sensitive macro that returns the underlying value based on the type of
 * the option passed.
 */
#define option_get(option)                                                     \
    _Generic((option),                                                         \
        struct bool_option: (option).value,                                    \
        struct int_option: (option).value,                                     \
        struct enum_option: (option).value,                                    \
        struct string_option: (option).value)

/**
 * Checks if the option is named or abbreviated like value.
 */
static inline bool
option_named(const struct option *opt, const char * const value) {
    return (opt->name && strcmp(opt->name, value) == 0) ||
        (opt->abbr && strcmp(opt->abbr, value) == 0);
}

static inline void bool_option_set(const struct option *opt, bool value) {
    container_of(opt, struct bool_option, option)->value = value;
}

static inline void int_option_set(const struct option *opt, int64_t value) {
    container_of(opt, struct int_option, option)->value = value;
}

static inline void
string_option_set(struct option *opt, const struct u_string *value) {
    struct string_option *str_opt =
        container_of(opt, struct string_option, option);
    if (str_opt->validator && !str_opt->validator(value)) { return; }
    u_string_set(&str_opt->value, value);
}

static inline bool
enum_option_set(const struct option *opt, const char *value) {
    struct enum_option * const enum_opt =
        container_of(opt, struct enum_option, option);

    const char * const *enum_str;

    for (enum_str = &enum_opt->allowed_values[0]; *enum_str; enum_str++) {
        if (strcmp(*enum_str, value) == 0) { break; }
    }

    return *enum_str ? !!(enum_opt->value = enum_str) : false;
}

/**
 * @}
 */

#endif

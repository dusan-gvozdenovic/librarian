/* SPDX-License-Identifier: Apache-2.0 */

#include <test.h>

#include <librarian/hashmap.h>

struct vector3 {
    float x, y, z;
};

static hash vector3_hash(const void *element) {
    const struct vector3 *v = element;
    return *(uint32_t *) &v->x ^ *(uint32_t *) &v->y ^ *(uint32_t *) &v->z;
}

struct hashmap map;

const size_t INITIAL_CAPACITY = 8;

#define assert_hashmap_iterator_end(map, it)                                   \
    assert(hashmap_iterator_is_end(&(map), (it)))

#define assert_hashmap_iterator_not_end(map, it)                               \
    assert_false(hashmap_iterator_is_end(&(map), (it)))

void test_hashmap_setup(void) {
    hashmap_init(&map, INITIAL_CAPACITY, sizeof(struct vector3),
        sizeof(char *));
    map.hash = vector3_hash;
}

void test_hashmap_teardown(void) { hashmap_cleanup(&map); }

// Assumption: load_factor is the default 0.75
void test_reserve_under_capacity(void) {
    hashmap_reserve(&map, 0.5 * INITIAL_CAPACITY);
    assert_equal(map.capacity, INITIAL_CAPACITY);
}

void test_reserve_and_grow(void) {
    hashmap_reserve(&map, 2 * INITIAL_CAPACITY - 1);
    assert_equal(map.capacity, 2 * INITIAL_CAPACITY);
}

void test_insert_one(void) {
    struct vector3 key = { 1.0, 2.0, 3.0 };
    char *value = "test";
    hashmap_set(&map, &key, value);
    assert_equal(map.size, 1);
}

void test_insert_over_capacity(void) {
    char *value = "test";
    for (size_t i = 0; i < 32; i++) {
        struct vector3 v = { i, i + 1, i + 2 };
        hashmap_set(&map, &v, "test");
    }
    assert_equal(map.size, 32);
}

void test_insert_overlapping(void) {
    char *value = "test";
    for (size_t n = 0; n < 4; n++) {
        for (size_t i = 0; i < 32; i++) {
            struct vector3 v = { i, i + 1, i + 2 };
            hashmap_set(&map, &v, "test");
        }
    }
    assert_equal(map.size, 32);
}

void test_find(void) {
    for (int i = 0; i < 320; i++) {
        struct vector3 v = { i, i + 1, i + 2 };
        hashmap_set(&map, &v, "test");
        assert_hashmap_iterator_not_end(map, hashmap_find(&map, &v));
    }

    assert_equal(map.size, 320);

    for (int i = 0; i < 320; i++) {
        struct vector3 v = { i, i + 1, i + 2 };
        assert_hashmap_iterator_not_end(map, hashmap_find(&map, &v));
    }

    struct vector3 needle = { 10000, 20000, 30000 };
    hashmap_set(&map, &needle, "test2");
    struct hashmap_iterator it = hashmap_find(&map, &needle);
    assert_hashmap_iterator_not_end(map, it);
    assert_c_str_equal(hashmap_iterator_value(&map, it), "test2");
}

void test_remove(void) {
    for (size_t i = 0; i < 320; i++) {
        struct vector3 v = { i, i + 1, i + 2 };
        hashmap_set(&map, &v, "test");
    }

    assert_equal(map.size, 320);

    for (size_t i = 0; i < 320; i++) {
        struct vector3 v = { i, i + 1, i + 2 };
        hashmap_remove(&map, hashmap_find(&map, &v));
    }

    assert_equal(map.size, 0);
}

void test_remove_iterator(void) {
    for (size_t i = 0; i < 320; i++) {
        struct vector3 v = { i, i + 1, i + 2 };
        hashmap_set(&map, &v, "test");
    }
    assert_equal(map.size, 320);
    struct hashmap_iterator it = hashmap_begin(&map);
    while (!hashmap_iterator_is_end(&map, it)) {
        it = hashmap_remove(&map, it);
    }
    assert_equal(map.size, 0);
}

initialize_runner("test-hashmap");

register_test_suite(hashmap, test_hashmap_setup, test_hashmap_teardown);

register_test(hashmap, test_reserve_under_capacity);
register_test(hashmap, test_reserve_and_grow);
register_test(hashmap, test_insert_one);
register_test(hashmap, test_insert_over_capacity);
register_test(hashmap, test_insert_overlapping);
register_test(hashmap, test_find);
register_test(hashmap, test_remove);
register_test(hashmap, test_remove_iterator);

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_SERIALIZER_H__
#define __LIBRARIAN_SERIALIZER_H__

#include <stdio.h>

#include "types.h"
#include "u-string.h"

struct serializer {
    FILE *file;
    bool big_endian;
    long file_end;
};

void serializer_init(struct serializer *sz, FILE *file, bool big_endian);

bool serializer_write_int(struct serializer *sz, size_t size, intmax_t value);

bool serializer_write_string(struct serializer *sz, const struct u_string *str);

bool serializer_read_int(struct serializer *sz, size_t size, void *value);

bool serializer_write_bool(struct serializer *sz, bool value);

bool serializer_read_bool(struct serializer *sz, bool *value);

bool serializer_read_string(struct serializer *sz, struct u_string *str);

bool serializer_has_more(struct serializer *sz);

#endif

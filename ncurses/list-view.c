/* SPDX-License-Identifier: Apache-2.0 */

#include <stdio.h>
#include <string.h>

#include <ncurses.h>

#include <librarian/config.h>
#include <librarian/date.h>
#include <librarian/document.h>
#include <librarian/file.h>
#include <librarian/library.h>
#include <librarian/localization.h>
#include <librarian/observer.h>
#include <librarian/option.h>
#include <librarian/types.h>
#include <librarian/u-string.h>
#include <librarian/utf8.h>
#include <librarian/vector.h>

#include "librarian.h"
#include "list-box.h"
#include "list-store.h"
#include "list-view.h"
#include "normal-mode.h"

#define LIST_HEIGHT (LINES - 2)
// #define TITLE_LIMIT(WIDTH) (3 * (WIDTH) / 7)
#define TITLE_LIMIT(WIDTH) (WIDTH / 2)
#define SIZE_UNUSABLE 12
#define SIZE_SMALL 47

static void
_document_list_box_write_line(struct list_box *lb, list_item current);

// clang-format off
struct list_view list_view = {
    .ui_events = {
        .loop_func = list_view_loop,
        .paint_func = list_view_paint,
        .resize_func = list_view_handle_resize,
        .interpret_number = list_view_interpret_number
    },
    .document_list_box = {
        .pos_y = 1, // Start after title
        .pos_x = 0,
        .write_line = _document_list_box_write_line,
        .line_numbers = true
    }
};
// clang-format on

/* -------------------- implement list_box interface ------------------------ */

/**
 * TODO: While it works, it looks like a dumpster fire.
 *       Make it readable.
 */
static void
_document_list_box_write_line(struct list_box *lb, list_item current) {

    /* The window is too small to print anything. */
    if (lb->width <= SIZE_UNUSABLE) {
        wprintw(lb->window, "...");
        return;
    }

#define BORDER 2
#define PRINT_BORDER wprintw(lb->window, "  ")

    const struct document *doc = list_view_list_item_to_document(current);

    const char *title = doc->title.c_str;
    if (u_string_empty(&doc->title)) { path_basename(&doc->path, &title); }

    const int start = getcurx(lb->window);
    const int title_limit =
        (lb->width <= SIZE_SMALL ? lb->width - 1 : TITLE_LIMIT(lb->width)) -
        start;
    const int title_len =
        doc->title.c_str ? doc->title.length : u8_strlen(title);
    int print_width;
    int padding;

    if (title_len > title_limit) {
        print_width = u8_strnwidth(title, title_limit - 3);
        wprintw(lb->window, "%.*s...", print_width, title);
    } else {
        print_width = u8_strnwidth(title, title_len);
        padding = title_limit + print_width - title_len;
        wprintw(lb->window, "%-*.*s", padding, print_width, title);
    }

    /* The window is too small to print anything but title. */
    if (lb->width <= SIZE_SMALL) { return; }

    PRINT_BORDER;

    const int month_max_len = longest_month_name_length();
    const int date_limit = BORDER + month_max_len + BORDER + 4;
    const int author_limit =
        lb->width - start - title_limit - BORDER - date_limit - 1;

    if (doc->author.length == 0) {
        wprintw(lb->window, "%*s", author_limit, "");
    } else if (author_limit < doc->author.length) {
        print_width = u8_strnwidth(doc->author.c_str, author_limit - 3);
        wprintw(lb->window, "%.*s...", print_width, doc->author.c_str);
    } else {
        print_width = u8_strnwidth(doc->author.c_str, doc->author.length);
        padding = author_limit + print_width - doc->author.length;
        wprintw(lb->window, "%*.*s", padding, print_width, doc->author.c_str);
    }

    PRINT_BORDER;

    char * const month =
        is_no_date(doc->create_date) ? "" : month_name(doc->create_date.month);

    const int month_len = month_name_length(doc->create_date.month);
    print_width = month_name_width(doc->create_date.month);
    padding = month_max_len + print_width - month_len;
    wprintw(lb->window, "%-*.*s", padding, print_width, month);

    PRINT_BORDER;

    if (doc->create_date.year) {
        wprintw(lb->window, "%.4" PRIu32, doc->create_date.year);
    } else {
        wprintw(lb->window, "    ");
    }

#undef PRINT_BORDER
#undef BORDER
}

/* -------------------------- private functions ----------------------------- */

// For reasons beyond my current understanding the terminal on my work laptop
// the first 3-12 lines would not be refreshed or become garbled if I tried to
// scroll a bit more in the list. Different terminals and values for TERM were
// tried but I did not figure it out in the end.
//
// To mitigate that and avoid hurting performance call this function only when
// making "big" changes to the underlying list_box model.
//
// OS:              macOS Ventura 13.5
// ncurses:         6.4
// TERM:            xterm-256color
// LANG:            sr_RS.UTF-8
// Terminals tried: iTerm2, kitty
static inline void _list_view_clear_pre_paint(void) {
    (void) clearok(normal_mode.window, true);
}

/* ------------------------- list_view functions ---------------------------- */

static void _list_view_load_order_from_config(void) {
    const char * const *order = option_get(librarian.config.order);
    list_view.order = (strcmp(*order, "author") == 0)
        ? document_compare_by_author
        : (strcmp(*order, "date") == 0) ? document_compare_by_date
                                        : document_compare_by_title;
}

// TODO: Fix const-correctness
int document_order_compare(const void *lhs, const void *rhs) {
    return list_view.order(list_view_list_item_to_document((list_item) lhs),
        list_view_list_item_to_document((list_item) rhs));
}

static void _list_item_document_unref(list_item *item) {
    struct document *doc = list_view_list_item_to_document(*item);
    document_unref(&doc);
    if (doc == NULL) { *item = NULL; }
}

#define LIST_ITEM_PROPS                                                        \
    ((struct list_item_properties) {                                           \
        .order_relation = document_order_compare,                              \
        .unref = _list_item_document_unref,                                    \
    })

void list_view_init(void) {
    list_view.document_list_box.width = COLS;
    list_view.document_list_box.height = LIST_HEIGHT;
    list_view.document_list_box.window = normal_mode.window;
    observer_init(&list_view.library_observer, list_view_library_updated);
    observer_init(&list_view.config_observer, list_view_config_updated);
    observer_init(&list_view.library_cleared, list_view_library_cleared);
    observer_subscribe(&list_view.library_observer, &librarian.library.subject);
    observer_subscribe(&list_view.library_cleared, &librarian.library.cleared);
    observer_subscribe(&list_view.config_observer, &librarian.config.on_change);
    list_box_init(&list_view.document_list_box, LIST_ITEM_PROPS);
    list_view_populate();
}

static void _populate_list_view_callback(struct document *doc, void *args) {
    list_store_add(args, list_view_document_to_list_item(doc));
}

void list_view_populate(void) {
    register struct list_box * const lb = &list_view.document_list_box;
    register struct list_store * const st = &lb->store;
    _list_view_load_order_from_config();
    library_filter(&librarian.library, NULL, _populate_list_view_callback, st);
    lb->top = st->parent.first(&st->parent);
    lb->current = lb->top;
}

// TODO: Can we optimize this step somehow and remove copying???
void list_view_library_updated(struct library_args *args) {
    struct list_store *store = &list_view.document_list_box.store;

    // Convert vector of documents to vector of items.
    struct vector v;
    vector_init_default(&v, sizeof(list_item), args->docs.capacity);

    struct document **doc;
    vector_for_each_entry (doc, struct document *, args->docs) {
        list_item tmp = list_view_document_to_list_item(*doc);
        vector_push_back(&v, &tmp);
    }

    if (args->operation_type == LIBRARY_ARGS_OP_ADD) {
        list_store_add_items(store, &v);
    } else if (args->operation_type == LIBRARY_ARGS_OP_REMOVE) {
        list_store_remove_items(store, &v);
    }

    vector_cleanup(&v);
    // NOTE: Since both normal_mode and list_view observers are subscribed to
    // the library_observer, whether we add subscribers to front or back in
    // observer.h matters because we want _list_view_clear_pre_paint to execute
    // before list_view_paint itself. Since list_view one is registered after
    // normal_mode one, adding subscribes to front saves us from having to do an
    // extra paint.
    //
    // Keep it like that until a change is needed (not that it matters much
    // anyway).
    if (normal_mode_in_focus(&list_view.ui_events)) {
        _list_view_clear_pre_paint();
    }
}

void list_view_library_cleared(void) {
    list_box_cleanup(&list_view.document_list_box);
    list_box_init(&list_view.document_list_box, LIST_ITEM_PROPS);
    list_view_populate();
    if (normal_mode_in_focus(&list_view.ui_events)) { list_view_paint(); }
}

void list_view_config_updated(const struct config_args * const args) {
    if (!normal_mode_in_focus(&list_view.ui_events)) { return; }

    if (args->option == &librarian.config.order.option) {
        _list_view_load_order_from_config();
        list_store_reorder(&list_view.document_list_box.store,
            document_order_compare);
    }

    list_view_paint();
}

void list_view_handle_resize(void) {
    list_box_handle_resize(&list_view.document_list_box, LIST_HEIGHT, COLS);
}

void list_view_activate(void) {
    if (!list_view.document_list_box.current) { return; }
    struct document *doc =
        list_view_list_item_to_document(*list_view.document_list_box.current);
    struct u_string cmd = U_STRING_INIT;
    (void) u_string_printf(&cmd,
        option_get(librarian.config.open_command_format).c_str,
        doc->path.str.c_str);
    FILE *pipe = popen(cmd.c_str, "r");
    pclose(pipe);
    u_string_cleanup(&cmd);
}

void list_view_paint(void) {
    werase(normal_mode.window);
    wmove(normal_mode.window, 0, 0);

    wattron(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));

    for (int i = 0; i < COLS; i++) { wprintw(normal_mode.window, " "); }

    mvwprintw(normal_mode.window, 0, 1, "%s", _("List View"));

    wattroff(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));

    list_box_paint(&list_view.document_list_box);
    wattron(normal_mode.window, COLOR_PAIR(LINE_COLOR));

    wmove(normal_mode.window, LINES - 2, 0);
    whline(normal_mode.window, ACS_HLINE, COLS);

    wattroff(normal_mode.window, COLOR_PAIR(LINE_COLOR));

    if (option_get(librarian.config.list_view_debug)) {
        struct list_box * const lb = &list_view.document_list_box;

        mvwprintw(normal_mode.window, lb->height, 0,
            "%" PRIu32 " %" PRIu32 " %" PRIu64 " %" PRIu32, lb->height,
            lb->width, lb->top_pos, lb->sel_pos);
    }

    doupdate();
}

void list_view_loop(utf8_char ch) {
    struct list_box * const lb = &list_view.document_list_box;
    struct list_model * const lm = &lb->store.parent;

    switch (ch) {
        case KEY_UP:
        case 'k':
            list_box_up(lb);
            list_view_paint();
            break;
        case KEY_DOWN:
        case 'j':
            list_box_down(lb);
            list_view_paint();
            break;
        case 'G':
            list_box_bottom(lb);
            _list_view_clear_pre_paint();
            list_view_paint();
            break;
        case 'g':
            if (wgetch(normal_mode.window) == 'g') {
                list_box_top(lb);
                _list_view_clear_pre_paint();
                list_view_paint();
            }
            break;
        case ' ':
            if (lm->size(lm) > 0) {
                list_box_toggle_select(lb, *lb->current);
                list_view_paint();
            }
            break;
        case '\\':
            if (wgetch(normal_mode.window) == '\\') {
                list_box_clear_selection(lb);
                list_view_paint();
            }
            break;
        case 10:
            list_view_activate();
            break;
    }
}

void list_view_interpret_number(int64_t number) {
    list_box_goto_line(&list_view.document_list_box, number - 1);
    _list_view_clear_pre_paint();
    list_view_paint();
}

void list_view_cleanup(void) {
    observer_unsubscribe(&list_view.library_observer);
    observer_unsubscribe(&list_view.config_observer);
    observer_unsubscribe(&list_view.library_cleared);
    list_box_cleanup(&list_view.document_list_box);
}

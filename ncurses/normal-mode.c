/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>

#include <librarian/library.h>
#include <librarian/localization.h>
#include <librarian/log.h>
#include <librarian/observer.h>
#include <librarian/types.h>
#include <librarian/vector.h>

#include "author-view.h"
#include "command-line.h"
#include "librarian.h"
#include "list-view.h"
#include "normal-mode.h"
#include "search-mode.h"
#include "ui-events.h"
#include "welcome.h"

// clang-format off
struct normal_mode normal_mode = {
    .window = NULL,
    .focused_object = {
        .loop_func = NULL,
        .paint_func = NULL,
        .interpret_number = NULL
    }
};
// clang-format on

void normal_mode_init(void) {
    normal_mode.window = newwin(LINES - 1, COLS, 0, 0);
    observer_init(&normal_mode.library_observer, normal_mode_library_update);
    observer_subscribe(&normal_mode.library_observer,
        &librarian.library.subject);
    keypad(normal_mode.window, TRUE);

    author_view_init();
    list_view_init();
    welcome_init();
}

void normal_mode_handle_resize(void) {
    werase(normal_mode.window);
    wresize(normal_mode.window, LINES - 1, COLS);

    struct ui_events *focused_object = &normal_mode.focused_object;

    if (focused_object->resize_func) { focused_object->resize_func(); }

    if (focused_object->paint_func) { focused_object->paint_func(); }

    wrefresh(normal_mode.window);
}

void normal_mode_set_focus(struct ui_events events) {
    normal_mode.focused_object = events;
    if (events.paint_func) { events.paint_func(); }
}

bool normal_mode_in_focus(struct ui_events *events) {
    return ui_events_equal(&normal_mode.focused_object, events);
}

void normal_mode_loop(void) {
    int ch = wgetch(normal_mode.window);
    if (ch == ERR || !ch) { return; }

    if (cmd_line.has_message) {
        werase(cmd_line.window);
        wrefresh(cmd_line.window);
        cmd_line.has_message = false;
    }

    switch (ch) {
        case ':':
            set_mode(COMMAND);
            break;
        case '/':
            search_mode_set_search_backwards(false);
            set_mode(SEARCH);
            break;
        case '?':
            search_mode_set_search_backwards(true);
            set_mode(SEARCH);
            break;
        case '1':
            normal_mode_set_focus(author_view.ui_events);
            break;
        case '2':
            normal_mode_set_focus(list_view.ui_events);
            break;
    }

    if (normal_mode.focused_object.loop_func) {
        normal_mode.focused_object.loop_func(ch);
    }
}

void normal_mode_library_update(struct library_args *args) {
    if (args->docs.size > 0) {
        log_info(&librarian.log, "%zu %s.", args->docs.size,
            args->operation_type == LIBRARY_ARGS_OP_ADD ? _("files added")
                                                        : _("files removed"));
    }

    if (normal_mode.focused_object.paint_func) {
        normal_mode.focused_object.paint_func();
    }
}

void normal_mode_interpret_number(int64_t number) {
    if (normal_mode.focused_object.interpret_number) {
        normal_mode.focused_object.interpret_number(number);
    }
}

void normal_mode_cleanup(void) {
    delwin(normal_mode.window);
    // author_view_cleanup();
    list_view_cleanup();
    // welcome_cleanup();
}

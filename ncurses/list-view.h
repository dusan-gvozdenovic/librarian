/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIST_VIEW_H__
#define __LIST_VIEW_H__

#include <librarian/compiler.h>
#include <librarian/config.h>
#include <librarian/document.h>
#include <librarian/library.h>
#include <librarian/observer.h>
#include <librarian/types.h>
#include <librarian/utf8.h>

#include "list-box.h"
#include "list-store.h"
#include "ui-events.h"

#define LIST_VIEW_NAME "list-view"

struct list_view {
    struct ui_events ui_events;
    struct observer library_observer;
    struct observer library_cleared;
    struct observer config_observer;
    struct list_box document_list_box;
    int (*order)(const struct document *lhs, const struct document *rhs);
};

extern struct list_view list_view;

static inline list_item list_view_document_to_list_item(struct document *doc) {
    return &doc->ref;
}

static inline struct document *list_view_list_item_to_document(list_item item) {
    return container_of(item, struct document, ref);
}

void list_view_init(void);

void list_view_populate(void);

void list_view_library_updated(struct library_args *args);

void list_view_library_cleared(void);

void list_view_config_updated(const struct config_args *args);

void list_view_handle_resize(void);

void list_view_activate(void);

void list_view_paint(void);

void list_view_loop(utf8_char ch);

void list_view_interpret_number(int64_t number);

void list_view_cleanup(void);

#endif

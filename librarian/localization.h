/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_LOCALIZATION_H__
#define __LIBRARIAN_LOCALIZATION_H__

/** Directory where gettext should look for translations. */
#define LIBRARIAN_TEXTDOMAIN LIBRARIAN_SYSTEM_PREFIX "/share/locale"

#ifdef LIBRARIAN_LOCALIZATION_ENABLED

#include <libintl.h>

#include "types.h"

/**
 * Retrieves the translation if it exists.
 *
 * This macro serves two purposes:
 * 1. xgettext command looks for _(...) calls and extracts the provided string
 *    arguments that need to be translated.
 * 2. _ resolves to gettext() which searches for translated string.
 */
#define _(STRING) (gettext(STRING))
#else
#define _(STRING) (STRING)
#endif

void locale_init(void);

char *month_name(size_t month);

int32_t month_name_length(size_t month);

int32_t month_name_width(size_t month);

int32_t longest_month_name_length(void);

int32_t longest_month_name_width(void);

#endif

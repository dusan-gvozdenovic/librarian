/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_LOG_H__
#define __LIBRARIAN_LOG_H__

#include "types.h"

enum log_mode { LOG_INFO, LOG_WARN, LOG_ERROR };

#define log_info(log, ...) log_print((log), LOG_INFO, __VA_ARGS__)
#define log_warn(log, ...) log_print((log), LOG_WARN, __VA_ARGS__)
#define log_error(log, ...) log_print((log), LOG_ERROR, __VA_ARGS__)

struct log {
    char *buffer;
    bool empty;

    void (*flush)(struct log *log, enum log_mode mode);
};

void log_init(struct log *log);

void log_cleanup(struct log *log);

void log_print(struct log *log, enum log_mode mode, const char *format, ...);

#endif

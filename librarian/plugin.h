/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_PLUGIN_H__
#define __LIBRARIAN_PLUGIN_H__

#include "compiler.h"
#include "core.h"
#include "types.h"
#include "version.h"

struct plugin_dependency {
    const char *identifier;
    lib_version min_version;
    lib_version max_version;
};

struct plugin {
    const char *name;
    const char *maintainer;
    const char *email;
    const char *website;
    const char *license;
    lib_version version;
    lib_version_number api_version;
    exit_status (*load)(struct librarian *librarian);
    exit_status (*unload)(struct librarian *librarian);
    const struct plugin_dependency **dependencies; //!< NULL-terminated array
};

#define plugin_export(name)                                                    \
    export_symbol struct plugin *__librarian_plugin_decl = &(name)

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_TYPES_H__
#define __LIBRARIAN_TYPES_H__

// IWYU pragma: begin_exports
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <sys/types.h> // for ssize_t
// IWYU pragma: end_exports

typedef int32_t exit_status;
typedef uint64_t hash;

#endif

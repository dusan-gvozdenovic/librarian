/* SPDX-License-Identifier: Apache-2.0 */

#include <ctype.h>
#include <ncurses.h>
#include <stdio.h>

#include <librarian/command.h>
#include <librarian/config.h>
#include <librarian/document-types.h>
#include <librarian/hashmap.h>
#include <librarian/hashset.h>
#include <librarian/library.h>
#include <librarian/option.h>
#include <librarian/types.h>
#include <librarian/utf8.h>
#include <librarian/utils.h>
#include <librarian/vector.h>

#include "command-line.h"
#include "command-mode.h"
#include "list-box.h"
#include "list-store.h"
#include "list-view.h"
#include "normal-mode.h"
#include "ui-events.h"

#include "librarian.h" // struct librarian librarian

static struct command_parser command_parser;

static void _register_curses_commands(struct command_parser *parser);

void command_mode_init(void) {
    command_parser_init(&command_parser, &librarian);
    commands_core_register(&command_parser);
    _register_curses_commands(&command_parser);
    command_line_init();
}

void command_mode_cleanup(void) {
    command_parser_cleanup(&command_parser);
    command_line_cleanup();
}

void command_mode_activate(void) {
    command_line_activate();
    struct u_string *cmdtok = &option_get(librarian.config.command_line_token);
    wprintw(cmd_line.window, "%s", cmdtok->c_str);
    cmd_line.start_pos = cmdtok->length;
    cmd_line.executor = command_mode_execute_command;
}

bool command_mode_execute_command(char *cmd_buffer) {
    skip_spaces(&cmd_buffer);
    if (*cmd_buffer == '\0' || command_mode_number(cmd_buffer)) { return true; }
    return command_parser_execute(&command_parser, cmd_buffer);
}

bool command_mode_number(char *buffer) {
    for (char *k = buffer; *k != '\0'; k++) {
        if (!isdigit(*k)) { return false; }
    }
    int64_t num;
    if (sscanf(buffer, "%" SCNd64, &num) != 1) { return false; }
    normal_mode_interpret_number(num);
    return true;
}

/* ----------------------- Terminal-specific Commands ----------------------- */

static bool _command_deselect(struct command_parser *parser) {
    if (!command_parser_token_in(parser, "nos", "deselect")) { return false; }

    if (ui_events_equal(&normal_mode.focused_object, &list_view.ui_events)) {
        list_box_clear_selection(&list_view.document_list_box);
        list_view_paint();
    }

    return true;
}

static bool _command_select_all(struct command_parser *parser) {
    if (!command_parser_token_in(parser, "sa", "select_all")) { return false; }

    if (!ui_events_equal(&normal_mode.focused_object, &list_view.ui_events)) {
        return true;
    }

    list_box_select_all(&list_view.document_list_box);

    list_view_paint();

    return true;
}

// TODO: Make this more general and move it to core.
// Example:
//    remove f1.pdf f2.pdf f3.pdf
//    remove [<selection>]
static bool _command_remove(struct command_parser *parser) {
    if (!command_parser_token_in(parser, "rm", "remove")) { return false; }

    if (!ui_events_equal(&normal_mode.focused_object, &list_view.ui_events)) {
        return true;
    }

    struct hashset *selected_items =
        &list_view.document_list_box.selected_items;

    if (selected_items->parent.size == 0) { return true; }

    struct hashset_iterator it;

    struct vector selected;

    document_vector_init_default(&selected, selected_items->parent.size);

    // Ok to not ref_inc() as long as librarian is not multithreaded.
    hashset_for_each (*selected_items, it) {
        document_vector_push_back_val(&selected,
            list_view_list_item_to_document(
                *(list_item *) hashset_iterator_element(selected_items, it)));
    }

    library_remove_many(&librarian.library, &selected);

    vector_cleanup(&selected);

    library_save(&librarian.library);

    list_box_clear_selection(&list_view.document_list_box);
    list_view_paint();

    return true;
}

#define CURSES_COMMAND_LIST(CMD)                                               \
    CMD("deselect", _command_deselect)                                         \
    CMD("select_all", _command_select_all)                                     \
    CMD("remove", _command_remove)

static void _register_curses_commands(struct command_parser *parser) {
#define _command_make_new(name, execute)                                       \
    command_parser_register_command_simple_x_list(*parser, (name), (execute))
    CURSES_COMMAND_LIST(_command_make_new);
#undef command_make_new
}

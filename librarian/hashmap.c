/* SPDX-License-Identifier: Apache-2.0 */

/**
 * This file implements a hashmap with the following features:
 * 1. variable-sized keys and values
 * 2. Robin Hood hashing
 * 3. open addressing
 * 4. linear probing
 */

#include <assert.h>
#include <string.h>

#include "hashmap.h"
#include "types.h"
#include "xmalloc.h"

struct _hashmap_entry_meta {
    size_t psl;
};

// Hashmap Layout: | ENTRY_METADATA | KEY | VALUE |

/* --------------------------- Utility functions ---------------------------- */

static inline bool _is_power_of_two(uint64_t n) { return (n & (n - 1)) == 0; }

// BUG: Not working for some reason
// BUG: Handle overflow
static inline size_t _next_power_of_two(size_t size) {
    // Already a power of two
    if (_is_power_of_two(size)) { return size << 1; }

    size--;
    for (size_t i = 1; i < (sizeof(size_t) * 8); i <<= 1) {
        size = size | (size >> i);
    }
    return size + 1;
}

/* ------------------------ Hashmap entry utilities ------------------------- */

static inline hash _hashmap_hash_internal(hash hash) {
    hash ^= (hash >> 40) ^ (hash >> 24);
    hash ^= (hash >> 20) ^ (hash >> 12);
    return hash ^ (hash >> 7) ^ (hash >> 4);
}

static inline size_t _hashmap_entry_size(const struct hashmap *map) {
    return sizeof(struct _hashmap_entry_meta) + map->key_size + map->value_size;
}

static inline struct _hashmap_entry_meta *_hashmap_entry_meta(uint8_t *block) {
    return (struct _hashmap_entry_meta *) block;
}

static inline uint8_t *_hashmap_key(uint8_t *block) {
    return block + sizeof(struct _hashmap_entry_meta);
}

static inline uint8_t *
_hashmap_value(const struct hashmap *map, uint8_t *block) {
    return block + sizeof(struct _hashmap_entry_meta) + map->key_size;
}

static inline size_t
_hashmap_index_calc(const struct hashmap *map, size_t value) {
    return value & (map->capacity - 1);
}

static inline uint8_t *_hashmap_at(const struct hashmap *map, size_t idx) {
    return map->data + _hashmap_index_calc(map, idx) * _hashmap_entry_size(map);
}

static inline size_t
_hashmap_entry_index(const struct hashmap *map, const uint8_t *entry) {
    return (entry - map->data) / _hashmap_entry_size(map);
}

static inline size_t
_hashmap_index_from_key(const struct hashmap *map, uint8_t *key) {
    return _hashmap_index_calc(map, _hashmap_hash_internal(map->hash(key)));
}

// BUG: There is no difference between unoccupied field and a
// zero-psl:zero-key:zero-value field.
// Solution: Make psl start from 1.
static bool _hashmap_occupied(const struct hashmap *map, uint8_t *block) {
    static_assert(sizeof(void *) == sizeof(intptr_t),
        "(void *) is of different size than intptr_t."
        "This optimization will not work.");

    if (map->key_size == sizeof(void *)) {
        return (*(intptr_t *) _hashmap_key(block)) != 0;
    }

    const intmax_t *x = (intmax_t *) _hashmap_key(block);
    const intmax_t *end = (intmax_t *) _hashmap_value(map, block);
    for (; x < end - 1; ++x) {
        if (*x != 0) { return true; }
    }
    for (uint8_t *it = (uint8_t *) x; it < (uint8_t *) end; ++it) {
        if (*it != 0) { return true; }
    }
    return false;
}

static inline void _hashmap_swap_entries(struct hashmap *map,
    uint8_t *lhs_block, uint8_t *rhs_block) {
    const size_t entry_size = _hashmap_entry_size(map);
    uint8_t *tmp = xmalloc(entry_size);
    memmove(tmp, lhs_block, entry_size);
    memmove(lhs_block, rhs_block, entry_size);
    memmove(rhs_block, tmp, entry_size);
    xfree(tmp);
}

static inline bool
_hashmap_equal(const struct hashmap *map, void *lhs, void *rhs) {
    return (map->equal && map->equal(lhs, rhs)) ||
        (memcmp(lhs, rhs, map->key_size) == 0);
}

void _hashmap_grow(struct hashmap *map, size_t new_capacity) {
    const size_t entry_size = _hashmap_entry_size(map);

    uint8_t *old_buffer = map->data;
    uint8_t *cursor = map->data;
    uint8_t *cursor_end = map->data + map->capacity * entry_size;

    assert(_is_power_of_two(new_capacity));
    assert(new_capacity > map->capacity);

    map->capacity = new_capacity;
    map->data = xmalloc(map->capacity * entry_size);
    memset(map->data, 0, map->capacity * entry_size);

    map->size = 0;

    while (cursor < cursor_end) {
        while (!_hashmap_occupied(map, cursor) && cursor < cursor_end) {
            cursor += entry_size;
        }
        if (cursor == cursor_end) { break; }

        hashmap_set(map, _hashmap_key(cursor), _hashmap_value(map, cursor));

        cursor += entry_size;
    }

    xfree(old_buffer);
}

/* ------------------------ Hashmap implementation -------------------------- */

void hashmap_init(struct hashmap *map, size_t initial_capacity, size_t key_size,
    size_t value_size) {
    map->size = 0;
    map->capacity = initial_capacity;
    map->key_size = key_size;
    map->value_size = value_size;
    map->load_factor = 0.75;
    if (!_is_power_of_two(map->capacity)) {
        map->capacity = _next_power_of_two(map->capacity);
    }
    const size_t buffer_size = map->capacity * _hashmap_entry_size(map);
    map->data = xmalloc(buffer_size);
    memset(map->data, 0, buffer_size);
}

void *hashmap_iterator_key(const struct hashmap *map,
    struct hashmap_iterator iterator) {
    return _hashmap_key(iterator.current);
}

void *hashmap_iterator_value(const struct hashmap *map,
    struct hashmap_iterator iterator) {
    return _hashmap_value(map, iterator.current);
}

struct hashmap_iterator
hashmap_iterator_of_key(const struct hashmap *map, const uint8_t *key_segment) {
    struct hashmap_iterator it = { (uint8_t *) key_segment -
        sizeof(struct _hashmap_entry_meta) };
    return it;
}

struct hashmap_iterator hashmap_iterator_of_value(const struct hashmap *map,
    const uint8_t *value_segment) {
    struct hashmap_iterator it = { (uint8_t *) value_segment - map->key_size -
        sizeof(struct _hashmap_entry_meta) };
    return it;
}

struct hashmap_iterator hashmap_begin(const struct hashmap *map) {
    struct hashmap_iterator it = { map->data };
    const struct hashmap_iterator end = hashmap_end(map);

    if (map->size == 0) { return end; }

    const size_t entry_size = _hashmap_entry_size(map);

    while (!_hashmap_occupied(map, it.current)) { it.current += entry_size; }

    return it;
}

struct hashmap_iterator hashmap_end(const struct hashmap *map) {
    struct hashmap_iterator end = { map->data +
        map->capacity * _hashmap_entry_size(map) };
    return end;
}

struct hashmap_iterator
hashmap_next(const struct hashmap *map, struct hashmap_iterator it) {
    struct hashmap_iterator end = hashmap_end(map);

    if (map->size == 0 || it.current == end.current) { return end; }

    const size_t entry_size = _hashmap_entry_size(map);

    do {
        it.current += entry_size;
    } while (it.current < end.current && !_hashmap_occupied(map, it.current));
    return it;
}

void hashmap_reserve(struct hashmap *map, size_t entries) {
    if (entries < map->load_factor * map->capacity) { return; }
    if (!_is_power_of_two(entries)) {
        // BUG: Handle overflow
        entries = _next_power_of_two(entries);
    }
    _hashmap_grow(map, entries);
}

void hashmap_clear(struct hashmap *map) {
    map->size = 0;
    (void) memset(map->data, 0, map->capacity * _hashmap_entry_size(map));
}

void hashmap_set(struct hashmap *map, void *key, void *value) {
    const size_t entry_size = _hashmap_entry_size(map);

    if (map->size >= (map->load_factor * map->capacity)) {
        // BUG: Handle overflow
        _hashmap_grow(map, _next_power_of_two(map->capacity));
    }

    size_t index = _hashmap_index_from_key(map, key);
    uint8_t *curr = _hashmap_at(map, index);

    uint8_t *tbi = xnew_n(uint8_t, entry_size); /* tbi - to be inserted */

    _hashmap_entry_meta(tbi)->psl = 0;
    memcpy(_hashmap_key(tbi), key, map->key_size);
    memcpy(_hashmap_value(map, tbi), value, map->value_size);

    // XXX: Does this always terminate?
    while (_hashmap_occupied(map, curr)) {
        /* Did we find an existing key? */
        if (_hashmap_equal(map, _hashmap_key(tbi), _hashmap_key(curr))) {
            /** TODO: Maybe this needs to be optimized for pointer values.
             * Benchmark it. */
            memcpy(_hashmap_value(map, curr), value, map->value_size);
            goto cleanup;
        }
        if (_hashmap_entry_meta(tbi)->psl > _hashmap_entry_meta(curr)->psl) {
            _hashmap_swap_entries(map, tbi, curr);
        }
        index = _hashmap_index_calc(map, index + 1);
        curr = _hashmap_at(map, index);
        _hashmap_entry_meta(tbi)->psl++;
    }

    memmove(curr, tbi, entry_size);
    map->size++;

cleanup:
    xfree(tbi);
}

struct hashmap_iterator hashmap_find(const struct hashmap *map, void *key) {
    size_t initial_idx = _hashmap_index_from_key(map, key);
    size_t idx = initial_idx;
    size_t psl = 0;
    struct hashmap_iterator ret = hashmap_end(map);

    do {
        uint8_t *curr = _hashmap_at(map, idx);
        idx = _hashmap_index_calc(map, idx + 1);
        if (!_hashmap_occupied(map, curr) ||
            _hashmap_entry_meta(curr)->psl < psl) {
            break;
        }
        if (_hashmap_equal(map, _hashmap_key(curr), key)) {
            ret.current = curr;
            break;
        }
        ++psl;
    } while (idx != initial_idx);

    return ret;
}

struct hashmap_iterator
hashmap_remove(struct hashmap *map, struct hashmap_iterator it) {
    const struct hashmap_iterator end = hashmap_end(map);

    if (it.current == end.current || map->size == 0) { return end; }

    const size_t entry_size = _hashmap_entry_size(map);

    size_t index = _hashmap_entry_index(map, it.current);
    index = _hashmap_index_calc(map, index + 1);
    uint8_t *next = _hashmap_at(map, index);

    while (_hashmap_entry_meta(next)->psl > 0 && _hashmap_occupied(map, next)) {
        memmove(it.current, next, entry_size);
        _hashmap_entry_meta(it.current)->psl--;
        it.current = next;
        index = _hashmap_index_calc(map, index + 1);
        next = _hashmap_at(map, index);
    }

    --map->size;
    memset(it.current, 0, entry_size);

    if (map->size == 0) { return end; }

    it.current = next;
    while (!_hashmap_occupied(map, it.current)) {
        index = _hashmap_index_calc(map, index + 1);
        it.current = _hashmap_at(map, index);
    }

    return it;
}

void hashmap_cleanup(struct hashmap *map) {
    assert(map != NULL);
    xfree_ptr(&map->data);
    map->size = 0;
    map->capacity = 0;
    map->key_size = 0;
    map->value_size = 0;
    map->load_factor = 0;
    map->equal = NULL;
    map->hash = NULL;
}

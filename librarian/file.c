/* SPDX-License-Identifier: Apache-2.0 */

#include <errno.h>
#include <fts.h>
#include <libgen.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <wordexp.h>

#include "file.h"
#include "limits.h"
#include "localization.h"
#include "u-string.h"
#include "utf8.h"
#include "utils.h"
#include "xmalloc.h"

// TODO: Keep the path `normalized' - without no extraneous path separators.
// e.g.
// '/' -> '/'
// 'abc/' -> 'abc'
// '/////abc//' -> '/abc'
void path_set_c_str(struct path *path, const char *c_str, size_t length) {
    u_string_set_c_str_n(&path->str, c_str, length);
}

void path_cleanup(struct path *path) { u_string_cleanup(&path->str); }

int path_compare(const struct path *lhs, const struct path *rhs) {
    return u_string_compare(&lhs->str, &rhs->str);
}

void path_copy(struct path *destination, const struct path *source) {
    u_string_set(&destination->str, &source->str);
}

bool path_expand(struct path *path) {
    wordexp_t p;
    int status = wordexp(path->str.c_str, &p, 0);

    // https://www.gnu.org/software/libc/manual/html_node/Calling-Wordexp.html
    if (status != 0) { return false; }

    size_t length = 0;
    for (size_t i = 0; i < p.we_wordc; i++) { length += strlen(p.we_wordv[i]); }

    size_t new_capacity = max(length + 1, path->str.capacity);
    char *tmp_path = xnew_n(char, new_capacity);

    char *it = tmp_path;
    for (size_t i = 0; i < p.we_wordc; i++) {
        size_t wlen = strlen(p.we_wordv[i]);
        strcpy(it, p.we_wordv[i]);
        it += wlen;
    }

    if (new_capacity > path->str.capacity) {
        path->str.c_str = xrenew(char, path->str.c_str, new_capacity);
    }

    strncpy(path->str.c_str, tmp_path, length + 1);
    path->str.capacity = new_capacity;
    path->str.byte_length = length;
    path->str.length = u8_strlen(path->str.c_str);

    wordfree(&p);
    xfree(tmp_path);

    return true;
}

bool path_real(struct path *path) {
    char *tmp = xnew_n(char, PATH_MAX + 1);
    char *result = realpath(path->str.c_str, tmp);
    if (result != tmp) { return false; }
    u_string_set_c_str(&path->str, tmp);
    xfree(tmp);
    return true;
}

static inline bool _path_is_directory_c_str(const char *str) {
    struct stat st;
    return stat(str, &st) == 0 && S_ISDIR(st.st_mode);
}

static inline bool _path_is_relative_c_str(const char *str) {
    const char *begin, *end;
    c_str_strip_ws(str, &begin, &end);
    return *begin != '/';
}

bool path_is_directory(const struct path *path) {
    return _path_is_directory_c_str(path->str.c_str);
}

bool path_is_file(const struct path *path) {
    struct stat st;
    if (stat(path->str.c_str, &st)) { return false; }
    return S_ISREG(st.st_mode);
}

void path_append(struct path *path, const char *c_str) {
    if (c_str_null_or_blank(c_str)) { return; }
    u_string_append_c_str(&path->str, LIBRARIAN_SYSTEM_PATH_SEPARATOR,
        strlen(LIBRARIAN_SYSTEM_PATH_SEPARATOR));
    u_string_append_c_str(&path->str, c_str, strlen(c_str));
}

void path_basename(const struct path *path, const char **suffix) {
    *suffix = basename(path->str.c_str);
}

bool path_create_directory(const struct path *path) {
    register const char sep = LIBRARIAN_SYSTEM_PATH_SEPARATOR[0];
    register char *p = strchr(path->str.c_str + 1, sep);
    while (true) {
        if (p != NULL) { *p = '\0'; }
        if (mkdir(path->str.c_str, 0755) && errno != EEXIST) { return false; }
        if (p == NULL) { break; }
        *p = sep;
        p = strchr(p + 1, sep);
    }
    return true;
}

// TODO: Propagate the reason upwards.
bool walk_directory(const struct path *path,
    void (*callback)(const struct path *path, void *args), void *args) {
    char *paths[2] = { path->str.c_str, NULL };

    bool status = true;

    FTS *tree = fts_open(paths, FTS_NOCHDIR, NULL);

    if (!tree) {
        status = false;
        goto cleanup;
    }

    struct path current = PATH_INIT;

    FTSENT *node;

    while ((node = fts_read(tree)) != NULL) {
        if (node->fts_info & FTS_F) {
            path_set_c_str(&current, node->fts_path, node->fts_pathlen);
            callback(&current, args);
        }
    }

    if (errno) { status = false; }

    if (fts_close(tree)) { status = false; }

cleanup:
    path_cleanup(&current);
    return status;
}

/* ------------------------- Base directory support ------------------------- */

static bool _initialized = false;

static const char *_path_base_librarian_var_names[] = {
    "LIBRARIAN_CONFIG_DIR",
    "LIBRARIAN_DATA_DIR",
    "LIBRARIAN_CACHE_DIR",
    "LIBRARIAN_STATE_DIR",
};

static const char *_path_base_xdg_var_names[] = {
    "XDG_CONFIG_HOME",
    "XDG_DATA_HOME",
    "XDG_CACHE_HOME",
    "XDG_STATE_HOME",
};

static const char *_path_base_xdg_defaults[] = {
    "$HOME/.config",
    "$HOME/.local/share",
    "$HOME/.cache",
    "$HOME/.local/state",
};

static struct path _base_paths[] = { PATH_INIT, PATH_INIT, PATH_INIT,
    PATH_INIT };

// If a librarian-specific environment variable is set or an XDG one is unset
// and we fall back to the default, we do not care if that path actually exists
// or if it is really a directory. It is the caller's duty to handle any errors
// in that case.
void path_base_init_all() {
    if (_initialized) { return; }

    enum path_base base = PATH_BASE_CONFIG;
    struct path tmp = PATH_INIT;
    for (; base <= PATH_BASE_STATE; base++) {
        struct path *base_p = &_base_paths[base];
        // Before XDG, check librarian-specific directories.
        const char *var = getenv(_path_base_librarian_var_names[base]);
        if (!c_str_null_or_blank(var)) {
            path_set_c_str(&tmp, var, strlen(var));
            path_expand(&tmp);

            path_copy(base_p, &tmp);
            continue;
        }

#ifdef LIBRARIAN_XDG_BASE_DIR_SUPPORT_ENABLED
        var = getenv(_path_base_xdg_var_names[base]);
        // Variable is set and the path is absolute.
        if (!c_str_null_or_blank(var) && !_path_is_relative_c_str(var)) {
            path_set_c_str(base_p, var, strlen(var));
            path_append(base_p, "librarian");
            continue;
        }

        // Check for XDG Base Directory Specification defaults.
        path_set_c_str(&tmp, _path_base_xdg_defaults[base],
            strlen(_path_base_xdg_defaults[base]));
        path_expand(&tmp);
        path_copy(base_p, &tmp);
        path_append(base_p, "librarian");
#endif
    }
    path_cleanup(&tmp);

    _initialized = true;
}

bool path_base_get(struct path *path, enum path_base base, const char *name) {
    struct path *base_p = &_base_paths[base];
    if (base_p->str.c_str == NULL) { return false; }
    path_copy(path, base_p);
    path_append(path, name);
    return true;
}

bool path_base_ensure(enum path_base base, struct u_string *err) {
    struct path *base_p = &_base_paths[base];
    const char *base_dir_name[] = { _("Config"), _("Data"), _("Cache"),
        _("State") };
    if (base_p->str.c_str == NULL) {
        u_string_printf(err, _("%s directory is empty!"), base_dir_name[base]);
        return false;
    }

    if (!path_create_directory(&_base_paths[base])) {
        u_string_printf(err,
            _("Failed to create base directory \"%s\". Reason: %s."),
            _base_paths[base].str.c_str, strerror(errno));
        return false;
    }

    return true;
}

void path_base_cleanup(void) {
    path_cleanup(&_base_paths[PATH_BASE_CONFIG]);
    path_cleanup(&_base_paths[PATH_BASE_DATA]);
    path_cleanup(&_base_paths[PATH_BASE_CACHE]);
    path_cleanup(&_base_paths[PATH_BASE_STATE]);
}

/* SPDX-License-Identifier: Apache-2.0 */

#include <ncurses.h>

#include <librarian/library.h>
#include <librarian/localization.h>
#include <librarian/types.h>
#include <librarian/utf8.h>

#include "author-view.h"
#include "librarian.h"
#include "normal-mode.h"

// clang-format off
struct author_view author_view = {
    .position = 0,
    .ui_events = {
        .loop_func = author_view_loop,
        .paint_func = author_view_paint,
        .resize_func = NULL
    }
};
// clang-format on

void author_view_init(void) {
    // author_view.window = newwin(LINES - 1, COLS, 0, 0);
}

void author_view_paint(void) {
    werase(normal_mode.window);
    wattron(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));
    for (int i = 0; i < COLS; i++) { wprintw(normal_mode.window, " "); }
    mvwprintw(normal_mode.window, 0, 1, "%s", _("Authors"));
    int separator_pos = 2 * COLS / 5;

    mvwprintw(normal_mode.window, 0, separator_pos + 2, "%s", _("Titles"));

    wattroff(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));

    // begin: write authors list

    for (size_t i = 0; i < librarian.library.size; i++) {
        if (i == author_view.position) {
            wattron(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));
            mvwprintw(normal_mode.window, 1 + i, 0, " %zu. ", i + 1);
            int x = getcurx(normal_mode.window);
            while (x++ < 2 * COLS / 5) { wprintw(normal_mode.window, " "); }
            wattroff(normal_mode.window, COLOR_PAIR(HIGHLIGHT_COLOR));
        } else {
            mvwprintw(normal_mode.window, 1 + i, 0, " %zu. ", i + 1);
        }
    }

    // end: write authors list

    wattron(normal_mode.window, COLOR_PAIR(LINE_COLOR));

    wmove(normal_mode.window, 1, separator_pos);
    wvline(normal_mode.window, ACS_VLINE, LINES - 1);

    wmove(normal_mode.window, LINES - 2, 0);
    whline(normal_mode.window, ACS_HLINE, COLS);

    wmove(normal_mode.window, LINES - 2, separator_pos);
    waddch(normal_mode.window, ACS_BTEE);

    wattroff(normal_mode.window, COLOR_PAIR(LINE_COLOR));
}

void author_view_loop(utf8_char ch) { }

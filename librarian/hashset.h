/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_HASHSET_H__
#define __LIBRARIAN_HASHSET_H__

#include "hashmap.h"

struct hashset {
    struct hashmap parent;
};

struct hashset_iterator {
    struct hashmap_iterator parent;
};

/* --------------------------- Hashset interface ---------------------------- */

/**
 * Initializes the hashset structure.
 * @related hashset
 */
static inline void
hashset_init(struct hashset *set, size_t initial_capacity, size_t value_size) {
    hashmap_init(&set->parent, initial_capacity, value_size, 0);
}

/**
 * Returns the segment of memory where the element is stored.
 * @memberof hashset
 */
static inline void *hashset_iterator_element(const struct hashset *set,
    struct hashset_iterator iterator) {
    return hashmap_iterator_key(&set->parent, iterator.parent);
}

/**
 * Returns the iterator pointing to the containing entry of the provided element
 * segment.
 * @param element_segment - key segment that belongs to the hashmap.
 * @memberof hashset
 */
static inline struct hashset_iterator
hashset_iterator_of(const struct hashset *set, uint8_t *element_segment) {
    struct hashset_iterator it = { hashmap_iterator_of_key(&set->parent,
        element_segment) };
    return it;
}

/**
 * @return map.size > 0 - the iterator pointing to the first non-empty entry.
 * @return map.size == 0 - @see hashset_end().
 * @memberof hashset
 */
static inline struct hashset_iterator hashset_begin(const struct hashset *set) {
    struct hashset_iterator it = { hashmap_begin(&set->parent) };
    return it;
}

/**
 * Returns the address marking the end of hashset.
 * @memberof hashset
 */
static inline struct hashset_iterator hashset_end(const struct hashset *set) {
    struct hashset_iterator it = { hashmap_end(&set->parent) };
    return it;
}

/**
 * Returns the iterator to next occupied entry or end if it is the last.
 * @memberof hashset
 */
static inline struct hashset_iterator
hashset_next(const struct hashset *set, struct hashset_iterator it) {
    struct hashset_iterator next = { hashmap_next(&set->parent, it.parent) };
    return next;
}

/**
 * Checks if the supplied iterator is the end iterator.
 */
static inline bool hashset_iterator_is_end(const struct hashset *set,
    const struct hashset_iterator iterator) {
    return iterator.parent.current == hashset_end(set).parent.current;
}

static inline void hashset_reserve(struct hashset *set, size_t entries) {
    hashmap_reserve(&set->parent, entries);
}

static inline void hashset_clear(struct hashset *set) {
    hashmap_clear(&set->parent);
}

/**
 * Inserts a value into a set. Inserting a value that already exists in the set
 * is a no-op.
 * @memberof hashset
 */
static inline void hashset_insert(struct hashset *set, void *value) {
    hashmap_set(&set->parent, value, 0);
}

static inline struct hashset_iterator
hashset_find(struct hashset *set, void *value) {
    struct hashset_iterator it = { hashmap_find(&set->parent, value) };
    return it;
}

static inline bool hashset_contains(const struct hashset *set, void *value) {
    return hashmap_contains_key(&set->parent, value);
}

static inline struct hashset_iterator
hashset_remove(struct hashset *set, struct hashset_iterator iterator) {
    struct hashset_iterator next = { hashmap_remove(&set->parent,
        iterator.parent) };
    return next;
}

/**
 * Releases the underlying buffer and resets the structure.
 * @related hashset
 */
static inline void hashset_cleanup(struct hashset *set) {
    hashmap_cleanup(&set->parent);
}

/* ------------------------ Type-specific functions ------------------------- */

#define declare_hashset_type(NAME, KEY)                                        \
                                                                               \
    /**                                                                        \
     * @copydoc hashset_init                                                   \
     */                                                                        \
    attr_unused static inline void NAME##_hashset_init(struct hashset *set,    \
        size_t initial_capacity) {                                             \
        hashset_init(set, initial_capacity, sizeof(KEY));                      \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashset_iterator_key                                           \
     */                                                                        \
    attr_unused static inline KEY *NAME##_hashset_iterator_element(            \
        const struct hashset *set, struct hashset_iterator iterator) {         \
        return hashset_iterator_element(set, iterator);                        \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashset_insert                                                 \
     */                                                                        \
    attr_unused static inline void NAME##_hashset_insert(struct hashset *set,  \
        KEY *key) {                                                            \
        hashset_insert(set, key);                                              \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashset_find                                                   \
     */                                                                        \
    attr_unused static inline struct hashset_iterator NAME##_hashset_find(     \
        struct hashset *set, KEY *key) {                                       \
        return hashset_find(set, key);                                         \
    }                                                                          \
                                                                               \
    /**                                                                        \
     * @copydoc hashset_contains                                               \
     */                                                                        \
    attr_unused static inline bool NAME##_hashset_contains(                    \
        const struct hashset *set, KEY *key) {                                 \
        return hashmap_contains_key(&set->parent, key);                        \
    }

#define hashset_for_each(set, it)                                              \
    for ((it) = hashset_begin(&(set)); !hashset_iterator_is_end(&(set), (it)); \
        (it) = hashset_next(&(set), (it)))

#endif

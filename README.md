# Librarian
## A dead-simple document organizer.
Librarian is an ncurses-based program that helps you organize, tag, view and
search your documents. Heavily inspired by [vim](https://www.vim.org) and
[cmus](https://cmus.github.io/).

![Screenshot](docs/screenshot.png)
## Installation

Binary packages for popular linux distributions are planned but currently
unavailable and librarian must be built manually or obtained through
[homebrew](https://brew.sh).

```bash
brew tap dusan-gvozdenovic/software
brew install --HEAD librarian
```

## Building

### Dependencies

#### Building
|     Package     |    Version     |                   License                    |
|-----------------|----------------|----------------------------------------------|
| cmake           | 3.13 or later  | [BSD-3-Clause][cmake-license]                |
| gettext         | 0.19 or later  | [GPL-3.0+][gettext-license]                  |

You will also need a build system of your choice supported by CMake (e.g. GNU
Make, Ninja, ...)

#### Program
|     Package     |    Version     |                   License                    |
|-----------------|----------------|----------------------------------------------|
| libintl         | 0.19 or later  | [LGPL-2.1-only][libintl-license]             |
| ncurses         | 6.0 or later   | [Custom Permissive License][ncurses-license] |
| exempi          | 2.4.3 or later | [BSD-3-Clause][exempi-license]               |

[cmake-license]: https://gitlab.kitware.com/cmake/cmake/raw/master/Copyright.txt
[exempi-license]: https://raw.githubusercontent.com/hfiguiere/exempi/master/BSD-License.txt
[gettext-license]: https://raw.githubusercontent.com/autotools-mirror/gettext/master/COPYING
[ncurses-license]: https://raw.githubusercontent.com/mirror/ncurses/master/COPYING
[libintl-license]: https://raw.githubusercontent.com/autotools-mirror/gettext/master/gettext-runtime/intl/COPYING.LIB

### Procedure
```bash
git clone https://gitlab.com/dusan-gvozdenovic/librarian.git
cd librarian

cmake -B build
cmake --build build --config Release
cmake --install build
```

and to uninstall, assuming that the build directory is still present:

```
xargs rm -f < build/install_manifest.txt
```

### Tips

- If you are using CMake that is pre-packaged for your system, the default
  install prefix CMake uses will likely be correctly set to whatever the system
  expects. If needed, this can be configured by adding `--install-prefix=PREFIX`
  to the first command. On many linux distributions it is `/usr`, on FreeBSD it
  is `/usr/local`, etc.
- Build can be sped up by appending `--parallel $(nproc)` to the `cmake --build`
  command.

### Optional features

You may also pass the following options to CMake:
 
| Option                        | Purpose                                  | Default | Dependencies             |
| ------------------------------|------------------------------------------|---------|--------------------------|
| `ENABLE_DOCS`                 | Build documentation.                     | `OFF`   | Doxygen                  |
| `ENABLE_EXPERIMENTAL_PLUGINS` | Build experimental plugins.              | `OFF`   | N/A                      |
| `ENABLE_LOCALIZATION`         | Enable localization support.             | `ON`    | gettext                  |
| `ENABLE_MAN`                  | Build and install man pages.             | `ON`    | gzip                     |
| `ENABLE_TESTS`                | Build tests.                             | `OFF`   | CUnit, tmux, imagemagick |
| `ENABLE_XDG_BASE_DIR_SUPPORT` | Store application files as per XDG spec. | `ON`    | N/A                      |

### Troubleshooting
#### librarian: error while loading shared libraries: liblibrarian-core.so: cannot open shared object file: No such file or directory

This means that the system's dynamic linker cannot find the shared library in
its default list of search paths. On most GNU/linux systems these are `/lib`,
`/usr/lib` and paths listed under `/etc/ld.so.conf`.
See [man ld.so(8)](https://man7.org/linux/man-pages/man8/ld.so.8.html).

One solution is to pass `-DCMAKE_INSTALL_RPATH=<install-prefix>/lib` to CMake.

## Configuration

On UNIX-like systems librarian adheres to the
[XDG Base Directory Specification][xdg-spec].

User configuration is stored in `$XDG_CONFIG_HOME/librarian/config`. See the
[sample_config](data/sample_config) to get an idea of which features can be
configured in librarian.

Additionally, since all the format handlers have been implemented as plugins, to
be able to work with PDFs or DjVus your configuration file should contain the
following lines:

```
load_plugin pdf
load_plugin djvu
```

[xdg-spec]: https://specifications.freedesktop.org/basedir-spec/latest/

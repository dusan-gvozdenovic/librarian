/*
 * rbtree.c -- generic red black tree
 *
 * Copyright (c) 2001-2007, NLnet Labs. All rights reserved.
 *
 * This software is open source.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Neither the name of the NLNET LABS nor the names of its contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file
 * Implementation of a redblack tree.
 */

#include "rbtree.h"

// clang-format off

struct rbnode __rbtree_null_node = {
    &__rbtree_null_node,
    &__rbtree_null_node,
    &__rbtree_null_node,
    RBNODE_BLACK
};

// clang-format on

/** Fixup node colours when delete happened */
static void rbtree_delete_fixup(struct rbtree *rbtree, struct rbnode *child,
    struct rbnode *child_parent);

void rbnode_init(struct rbnode *node) {
    node->left = node->right = node->parent = RBTREE_NULL;
    node->color = RBNODE_RED;
}

void rbnode_link(struct rbnode *node, struct rbnode *parent,
    struct rbnode **link) {
    node->parent = parent;
    node->left = node->right = RBTREE_NULL;
    *link = node;
}

/*
 * Rotates the node to the left.
 *
 */
static void rbtree_rotate_left(struct rbtree *rbtree, struct rbnode *node) {
    struct rbnode *right = node->right;
    node->right = right->left;
    if (right->left != RBTREE_NULL) { right->left->parent = node; }

    right->parent = node->parent;

    if (node->parent != RBTREE_NULL) {
        if (node == node->parent->left) {
            node->parent->left = right;
        } else {
            node->parent->right = right;
        }
    } else {
        rbtree->root = right;
    }
    right->left = node;
    node->parent = right;
}

/*
 * Rotates the node to the right.
 *
 */
static void rbtree_rotate_right(struct rbtree *rbtree, struct rbnode *node) {
    struct rbnode *left = node->left;
    node->left = left->right;
    if (left->right != RBTREE_NULL) left->right->parent = node;

    left->parent = node->parent;

    if (node->parent != RBTREE_NULL) {
        if (node == node->parent->right) {
            node->parent->right = left;
        } else {
            node->parent->left = left;
        }
    } else {
        rbtree->root = left;
    }
    left->right = node;
    node->parent = left;
}

void rbtree_insert_fixup(struct rbtree *rbtree, struct rbnode *node) {
    struct rbnode *uncle;

    /* While not at the root and need fixing... */
    while (node != rbtree->root && node->parent->color == RBNODE_RED) {
        /* If our parent is left child of our grandparent... */
        if (node->parent == node->parent->parent->left) {
            uncle = node->parent->parent->right;

            /* If our uncle is red... */
            if (uncle && uncle->color == RBNODE_RED) {
                /* Paint the parent and the uncle black... */
                node->parent->color = RBNODE_BLACK;
                uncle->color = RBNODE_BLACK;

                /* And the grandparent red... */
                node->parent->parent->color = RBNODE_RED;

                /* And continue fixing the grandparent */
                node = node->parent->parent;
            } else { /* Our uncle is black... */
                /* Are we the right child? */
                if (node == node->parent->right) {
                    node = node->parent;
                    rbtree_rotate_left(rbtree, node);
                }
                /* Now we're the left child, repaint and rotate... */
                node->parent->color = RBNODE_BLACK;
                node->parent->parent->color = RBNODE_RED;
                rbtree_rotate_right(rbtree, node->parent->parent);
            }
        } else {
            uncle = node->parent->parent->left;

            /* If our uncle is red... */
            if (uncle && uncle->color == RBNODE_RED) {
                /* Paint the parent and the uncle black... */
                node->parent->color = RBNODE_BLACK;
                uncle->color = RBNODE_BLACK;

                /* And the grandparent red... */
                node->parent->parent->color = RBNODE_RED;

                /* And continue fixing the grandparent */
                node = node->parent->parent;
            } else { /* Our uncle is black... */
                /* Are we the right child? */
                if (node == node->parent->left) {
                    node = node->parent;
                    rbtree_rotate_right(rbtree, node);
                }
                /* Now we're the right child, repaint and rotate... */
                node->parent->color = RBNODE_BLACK;
                node->parent->parent->color = RBNODE_RED;
                rbtree_rotate_left(rbtree, node->parent->parent);
            }
        }
    }
    rbtree->root->color = RBNODE_BLACK;
}

/** helpers for delete: swap node colours */
static inline void
swap_rbnode_color(enum rbnode_color *x, enum rbnode_color *y) {
    enum rbnode_color t = *x;
    *x = *y;
    *y = t;
}

/** helpers for delete: swap node pointers */
static inline void swap_np(struct rbnode **x, struct rbnode **y) {
    struct rbnode *t = *x;
    *x = *y;
    *y = t;
}

/** Update parent pointers of child trees of 'parent' */
static void change_parent_ptr(struct rbtree *rbtree, struct rbnode *parent,
    struct rbnode *old, struct rbnode *_new) {
    if (parent == RBTREE_NULL) {
        // log_assert(rbtree->root == old);
        if (rbtree->root == old) rbtree->root = _new;
        return;
    }
    // log_assert(parent->left == old || parent->right == old
    //  || parent->left == _new || parent->right == _new);
    if (parent->left == old) parent->left = _new;
    if (parent->right == old) parent->right = _new;
}
/** Update parent pointer of a node 'child' */
static void change_child_ptr(struct rbnode *child, struct rbnode *old,
    struct rbnode *_new) {
    if (child == RBTREE_NULL) return;
    // log_assert(child->parent == old || child->parent == _new);
    if (child->parent == old) child->parent = _new;
}

void rbtree_delete(struct rbtree *rbtree, struct rbnode *to_delete) {
    struct rbnode *child;

    /* make sure we have at most one non-leaf child */
    if (to_delete->left != RBTREE_NULL && to_delete->right != RBTREE_NULL) {
        /* swap with smallest from right subtree (or largest from left) */
        struct rbnode *smright = to_delete->right;
        while (smright->left != RBTREE_NULL) smright = smright->left;
        /* swap the smright and to_delete elements in the tree,
         * but the struct rbnode is first part of user data struct
         * so cannot just swap the keys and data pointers. Instead
         * readjust the pointers left,right,parent */

        /* swap colors - colors are tied to the position in the tree */
        swap_rbnode_color(&to_delete->color, &smright->color);

        /* swap child pointers in parents of smright/to_delete */
        change_parent_ptr(rbtree, to_delete->parent, to_delete, smright);
        if (to_delete->right != smright) {
            change_parent_ptr(rbtree, smright->parent, smright, to_delete);
        }

        /* swap parent pointers in children of smright/to_delete */
        change_child_ptr(smright->left, smright, to_delete);
        change_child_ptr(smright->left, smright, to_delete);
        change_child_ptr(smright->right, smright, to_delete);
        change_child_ptr(smright->right, smright, to_delete);
        change_child_ptr(to_delete->left, to_delete, smright);
        if (to_delete->right != smright) {
            change_child_ptr(to_delete->right, to_delete, smright);
        }
        if (to_delete->right == smright) {
            /* set up so after swap they work */
            to_delete->right = to_delete;
            smright->parent = smright;
        }

        /* swap pointers in to_delete/smright nodes */
        swap_np(&to_delete->parent, &smright->parent);
        swap_np(&to_delete->left, &smright->left);
        swap_np(&to_delete->right, &smright->right);

        /* now delete to_delete (which is at the location where the smright
         * previously was) */
    }
    // log_assert(to_delete->left == RBTREE_NULL || to_delete->right ==
    // NULL);

    if (to_delete->left != RBTREE_NULL) {
        child = to_delete->left;
    } else child = to_delete->right;

    /* unlink to_delete from the tree, replace to_delete with child */
    change_parent_ptr(rbtree, to_delete->parent, to_delete, child);
    change_child_ptr(child, to_delete, to_delete->parent);

    if (to_delete->color == RBNODE_RED) {
        /* if node is red then the child (black) can be swapped in */
    } else if (child->color == RBNODE_RED) {
        /* change child to RBNODE_BLACK, removing a RBNODE_RED node is no
         * problem */
        if (child != RBTREE_NULL) child->color = RBNODE_BLACK;
    } else rbtree_delete_fixup(rbtree, child, to_delete->parent);

    /* unlink completely */
    to_delete->parent = RBTREE_NULL;
    to_delete->left = RBTREE_NULL;
    to_delete->right = RBTREE_NULL;
    to_delete->color = RBNODE_BLACK;
}

static void rbtree_delete_fixup(struct rbtree *rbtree, struct rbnode *child,
    struct rbnode *child_parent) {
    struct rbnode *sibling;
    int go_up = 1;

    /* determine sibling to the node that is one-black short */
    if (child_parent->right == child) {
        sibling = child_parent->left;
    } else {
        sibling = child_parent->right;
    }

    while (go_up) {
        if (child_parent == RBTREE_NULL) {
            /* removed parent==black from root, every path, so ok */
            return;
        }

        if (sibling->color == RBNODE_RED) {
            /* rotate to get a black sibling */
            child_parent->color = RBNODE_RED;
            sibling->color = RBNODE_BLACK;
            if (child_parent->right == child) {
                rbtree_rotate_right(rbtree, child_parent);
            } else {
                rbtree_rotate_left(rbtree, child_parent);
            }
            /* new sibling after rotation */
            if (child_parent->right == child) {
                sibling = child_parent->left;
            } else {
                sibling = child_parent->right;
            }
        }

        if (child_parent->color == RBNODE_BLACK &&
            sibling->color == RBNODE_BLACK &&
            sibling->left->color == RBNODE_BLACK &&
            sibling->right->color ==
                RBNODE_BLACK) { /* fixup local with recolor of sibling */
            if (sibling != RBTREE_NULL) sibling->color = RBNODE_RED;

            child = child_parent;
            child_parent = child_parent->parent;
            /* prepare to go up, new sibling */
            if (child_parent->right == child) {
                sibling = child_parent->left;
            } else {
                sibling = child_parent->right;
            }
        } else go_up = 0;
    }

    if (child_parent->color == RBNODE_RED && sibling->color == RBNODE_BLACK &&
        sibling->left->color == RBNODE_BLACK &&
        sibling->right->color == RBNODE_BLACK) {
        /* move red to sibling to rebalance */
        if (sibling != RBTREE_NULL) { sibling->color = RBNODE_RED; }
        child_parent->color = RBNODE_BLACK;
        return;
    }
    // log_assert(sibling != RBTREE_NULL);

    /* get a new sibling, by rotating at sibling. See which child
       of sibling is red */
    if (child_parent->right == child && sibling->color == RBNODE_BLACK &&
        sibling->right->color == RBNODE_RED &&
        sibling->left->color == RBNODE_BLACK) {
        sibling->color = RBNODE_RED;
        sibling->right->color = RBNODE_BLACK;
        rbtree_rotate_left(rbtree, sibling);
        /* new sibling after rotation */
        if (child_parent->right == child) {
            sibling = child_parent->left;
        } else {
            sibling = child_parent->right;
        }
    } else if (child_parent->left == child && sibling->color == RBNODE_BLACK &&
        sibling->left->color == RBNODE_RED &&
        sibling->right->color == RBNODE_BLACK) {
        sibling->color = RBNODE_RED;
        sibling->left->color = RBNODE_BLACK;
        rbtree_rotate_right(rbtree, sibling);
        /* new sibling after rotation */
        if (child_parent->right == child) {
            sibling = child_parent->left;
        } else {
            sibling = child_parent->right;
        }
    }

    /* now we have a black sibling with a red child. rotate and exchange
     * colors.
     */
    sibling->color = child_parent->color;
    child_parent->color = RBNODE_BLACK;
    if (child_parent->right == child) {
        // log_assert(sibling->left->color == RBNODE_RED);
        sibling->left->color = RBNODE_BLACK;
        rbtree_rotate_right(rbtree, child_parent);
    } else {
        // log_assert(sibling->right->color == RBNODE_RED);
        sibling->right->color = RBNODE_BLACK;
        rbtree_rotate_left(rbtree, child_parent);
    }
}

/*
 * Finds the first element in the red black tree
 *
 */
struct rbnode *rbtree_first(struct rbtree *rbtree) {
    struct rbnode *node;
    for (node = rbtree->root; node->left != RBTREE_NULL; node = node->left) { }
    return node;
}

struct rbnode *rbtree_last(struct rbtree *rbtree) {
    struct rbnode *node;
    for (node = rbtree->root; node->right != RBTREE_NULL; node = node->right) {
    }
    return node;
}

/*
 * Returns the next node...
 *
 */
struct rbnode *rbtree_next(struct rbnode *rbtree) {
    struct rbnode *node = rbtree;
    struct rbnode *parent;

    if (node->right != RBTREE_NULL) {
        /* One right, then keep on going left... */
        for (node = node->right; node->left != RBTREE_NULL; node = node->left) {
        }
    } else {
        parent = node->parent;
        while (parent != RBTREE_NULL && node == parent->right) {
            node = parent;
            parent = parent->parent;
        }
        node = parent;
    }
    return node;
}

struct rbnode *rbtree_previous(struct rbnode *rbtree) {
    struct rbnode *node = rbtree;
    struct rbnode *parent;

    if (node->left != RBTREE_NULL) {
        /* One left, then keep on going right... */
        for (node = node->left; node->right != RBTREE_NULL;
            node = node->right) { }
    } else {
        parent = node->parent;
        while (parent != RBTREE_NULL && node == parent->left) {
            node = parent;
            parent = parent->parent;
        }
        node = parent;
    }
    return node;
}

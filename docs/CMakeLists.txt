if(ENABLE_MAN)
    find_program(GZIP gzip REQUIRED)
    execute_process(
        COMMAND git --no-pager log -n 1 --format="%cs" -- librarian.1.in
        OUTPUT_VARIABLE LIBRARIAN_MAN_CHANGE_TIMESTAMP
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    configure_file(librarian.1.in librarian.1 @ONLY)
    add_custom_target(librarian-gz ALL DEPENDS librarian.1.gz)
    add_custom_command(
        OUTPUT librarian.1.gz
        DEPENDS librarian.1
        COMMAND ${GZIP} < librarian.1 > librarian.1.gz
    )
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/librarian.1.gz
        DESTINATION ${CMAKE_INSTALL_PREFIX}/share/man/man1)
endif()

if(NOT ENABLE_DOCS)
    return()
endif()

find_package(Doxygen REQUIRED)

set(DOXYGEN_PROJECT_NAME Librarian)

set(DOXYGEN_OPTIMIZE_OUTPUT_FOR_C YES)
set(DOXYGEN_CLASS_DIAGRAMS YES)
set(DOXYGEN_CLASS_GRAPH YES)
set(DOXYGEN_COLLABORATION_GRAPH YES)
set(DOXYGEN_DOT_GRAPH_MAX_NODES 100)
set(DOXYGEN_DOT_TRANSPARENT YES)
set(DOXYGEN_EXTRACT_ALL YES)
set(DOXYGEN_EXTRACT_LOCALMETHODS YES)
set(DOXYGEN_EXTRACT_PRIVATE YES)
set(DOXYGEN_EXTRACT_PACKAGE YES)
set(DOXYGEN_EXTRACT_STATIC YES)
set(DOXYGEN_GENERATE_TREEVIEW YES)
set(DOXYGEN_GENERATE_HTML YES)
set(DOXYGEN_GENERATE_XML NO)
set(DOXYGEN_GENERATE_BUGLIST NO)
set(DOXYGEN_HAVE_DOT YES)
set(DOXYGEN_DOT_IMAGE_FORMAT svg)
set(DOXYGEN_INTERACTIVE_SVG YES)
set(DOXYGEN_HIDE_UNDOC_RELATIONS NO)
set(DOXYGEN_MAX_DOT_GRAPH_DEPTH 0)
set(DOXYGEN_TEMPLATE_RELATIONS YES)
set(DOXYGEN_UML_LOOK YES)
set(DOXYGEN_UML_LIMIT_NUM_FIELDS 50)
set(DOXYGEN_DYNAMIC_MENUS NO)
set(DOXYGEN_LAYOUT_FILE doxygen/DoxygenLayout.xml)

set(DOXYGEN_PROJECT_LOGO ${CMAKE_SOURCE_DIR}/data/icon.png)
set(DOXYGEN_HTML_HEADER doxygen/header.html)
set(DOXYGEN_HTML_EXTRA_STYLESHEET
    doxygen/doxygen-awesome/doxygen-awesome.css
    doxygen/doxygen-awesome/doxygen-awesome-sidebar-only.css
    doxygen/doxygen-awesome/doxygen-awesome-sidebar-only-darkmode-toggle.css
)
set(DOXYGEN_HTML_EXTRA_FILES
    doxygen/doxygen-awesome/doxygen-awesome-darkmode-toggle.js
    doxygen/doxygen-awesome/doxygen-awesome-paragraph-link.js
)
# HTML_COLORSTYLE must be set to LIGHT since Doxygen 1.9.5!
set(DOXYGEN_HTML_COLORSTYLE LIGHT)
set(DOXYGEN_STRIP_FROM_PATH ${CMAKE_SOURCE_DIR})
set(DOXYGEN_IMAGE_PATH ${CMAKE_CURRENT_SOURCE_DIR})

if(ENABLE_LOCALIZATION)
    list(APPEND DOXYGEN_PREDEFINED LIBRARIAN_LOCALIZATION_ENABLED)
endif()

# TODO: Integrate man pages with doxygen
# cat docs/librarian.1 | preconv | groff -mandoc -Tutf8 -Thtml > out.html
doxygen_add_docs(docs
    main.md
    user-manual.md
    developers-corner/developers-corner.md
    ../CONTRIBUTING.md
    developers-corner/design-philosophy.md
    developers-corner/architecture.md
    developers-corner/plugin-system.md
    developers-corner/library-management.md
    developers-corner/thread-management.md
    ./screenshot.png
    ../LICENSE
    ${LIBRARIAN_DOXYGEN_HEADERS}
)

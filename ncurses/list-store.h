/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIST_STORE_H__
#define __LIST_STORE_H__

#include <librarian/observer.h>
#include <librarian/ref.h>
#include <librarian/types.h>
#include <librarian/u-string.h>
#include <librarian/vector.h>

struct list_model;

/**
 * @brief list_item could technically be any reference-counted object.
 */
typedef struct ref *list_item;

struct list_item_properties {
    size_t (*columns)(const struct list_model *model);
    void (*value)(const struct list_model *model, const char *key,
        struct u_string *out);
    int (*order_relation)(const void *lhs, const void *rhs);
    void (*unref)(list_item *item);
};

// Interface
//
// list_item   - generic pointer.
// list_item * - member of the list_model involved in bookkeeping.
struct list_model {
    // Returns NULL if the current element is last in the list.
    list_item *(*next)(struct list_model *self, list_item *current);
    // Returns NULL if the current element is first in the list.
    list_item *(*prev)(struct list_model *self, list_item *current);
    // Returns NULL if list is empty.
    list_item *(*first)(struct list_model *self);
    // Returns NULL if list is empty.
    list_item *(*last)(struct list_model *self);
    // Returns the (list_item *) at the given position.
    list_item *(*at)(struct list_model *self, size_t position);
    // Returns the index of the given (list_item *).
    size_t (*index_of)(struct list_model *self, list_item *item);
    size_t (*size)(struct list_model *self);

    struct subject updated;

    struct list_item_properties props;
};

static inline void list_item_ref(list_item item) { ref_inc(item); }

static inline void list_item_unref(struct list_model *model, list_item *item) {
    model->props.unref(item);
}

enum list_model_update_operation {
    LIST_MODEL_UPDATE_ITEMS_ADDED,
    LIST_MODEL_UPDATE_ORDER_CHANGED,
    LIST_MODEL_UPDATE_ITEMS_ON_REMOVE,
    LIST_MODEL_UPDATE_ITEMS_REMOVED,
    LIST_MODEL_UPDATE_ITEMS_CLEARED
};

struct list_model_updated_args {
    struct list_model *self;
    enum list_model_update_operation operation;
    struct vector items;
};

// BUG: IWYU does not like this explicitly declared??
// struct list_store_priv;

// Concrete implementation of a list model.
struct list_store {
    struct list_model parent;
    struct list_store_priv *priv;
};

void list_store_init(struct list_store *store,
    struct list_item_properties props);

void list_store_add(struct list_store *store, list_item item);

/**
 * @param items - vector of list_item *;
 */
void list_store_add_items(struct list_store *store, struct vector *items);

void list_store_remove(struct list_store *store, list_item *item);

/**
 * @param items - vector of list_item *;
 */
void list_store_remove_items(struct list_store *store, struct vector *items);

void list_store_clear(struct list_store *store);

list_item *list_store_find(struct list_store *store, list_item item);

void list_store_reorder(struct list_store *store,
    int (*order)(const void *lhs, const void *rhs));

void list_store_cleanup(struct list_store *store);

#endif

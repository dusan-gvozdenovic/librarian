#!/bin/sh

set -x

SCAN_BUILD="${SCAN_BUILD:-scan-build}"

if ! [ -x $(command -v "${SCAN_BUILD}") ]; then
    echo "clang-format not found!" >&2
    exit 2
fi

SCN_EXEC="${SCAN_BUILD} -enable-checker alpha,core,deadcode,security,unix"

if ! ${SCN_EXEC} $@; then
    echo "Failed to run scan-build" >2
    exit 1
fi

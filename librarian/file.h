/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_FILE_H__
#define __LIBRARIAN_FILE_H__

#include "types.h"
#include "u-string.h"

#ifndef LIBRARIAN_SYSTEM_PATH_SEPARATOR
#define LIBRARIAN_SYSTEM_PATH_SEPARATOR "/"
#endif

struct path {
    struct u_string str;
};

#define PATH_INIT ((struct path) { .str = U_STRING_INIT_STATIC })

void path_set_c_str(struct path *path, const char *c_str, size_t length);

void path_cleanup(struct path *path);

int path_compare(const struct path *lhs, const struct path *rhs);

void path_copy(struct path *destination, const struct path *source);

bool path_expand(struct path *path);

bool path_real(struct path *path);

bool path_is_directory(const struct path *path);

bool path_is_file(const struct path *path);

void path_append(struct path *path, const char *c_str);

/**
 * Returns the basename of the provided path.
 *
 * @param path
 * @param suffix[out] a pointer which will be set to the beginning of the
 * basename suffix.
 */
void path_basename(const struct path *path, const char **suffix);

/**
 * Recursively creates directories from path.
 *
 * @return a boolean value indicating if creation was successful or not.
 * @remark If the directory exists this function will still return true.
 */
bool path_create_directory(const struct path *path);

bool walk_directory(const struct path *path,
    void (*callback)(const struct path *path, void *args), void *args);

/* --------------------------- Base path utilities -------------------------- */

enum path_base {
    PATH_BASE_CONFIG,
    PATH_BASE_DATA,
    PATH_BASE_CACHE,
    PATH_BASE_STATE
};

/**
 * @brief Loads base directory strings according to the following priority:
 * 1. LIBRARIAN_CONFIG_DIR, LIBRARIAN_DATA_DIR, LIBRARIAN_CACHE_DIR,
 *    LIBRARIAN_STATE_DIR environmental variables -- if set.
 * 2. If librarian is compiled with XDG Base Directory Specification enabled,
 *    base directory will be initialized with $XDG_VAR_HOME/librarian if
 *    $XDG_VAR_HOME is a valid absolute path or the corresponding defaults
 *    exist (e.g. "$HOME/.config" in lieu of $XDG_CONFIG_HOME).
 * 3. System-specific directories. e.g. ~/Library/Cache on macOS.
 *
 * See: https://specifications.freedesktop.org/basedir-spec/latest/index.html
 */
void path_base_init_all(void);

bool path_base_ensure(enum path_base base, struct u_string *err);

bool path_base_get(struct path *path, enum path_base base, const char *name);

void path_base_cleanup(void);

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_COMMAND_H__
#define __LIBRARIAN_COMMAND_H__

#include "core.h"
#include "types.h"
#include "u-string.h"
#include "utils.h"
#include "vector.h"

struct command_parser {
    char *tokenizer_state;
    struct u_string buffer;
    struct vector token;
    struct vector commands;

    // TODO: Make commands contextful.
    struct librarian *librarian;
};

struct command {
    const char *name;
    void (*init)(struct command *self);
    bool (*execute)(struct command_parser *parser);
    void (*cleanup)(struct command *self);
};

/** @related command_parser
 * @{
 */

void command_parser_init(struct command_parser *parser,
    struct librarian *librarian);

void command_parser_cleanup(struct command_parser *parser);

void command_parser_register_command(struct command_parser *parser,
    struct command *command);

bool command_parser_execute(struct command_parser *parser,
    const char *expression);

bool command_parser_token_matches(const struct command_parser *parser,
    const char *c_str);

bool command_parser_identifier(struct command_parser *parser);

bool command_parser_string(struct command_parser *parser);

bool command_parser_symbol(struct command_parser *parser, const char *symbol);

bool command_parser_end(struct command_parser *parser);

#define command_parser_token_in(parser, tokens...)                             \
    ({                                                                         \
        const char *__tokens[] = { tokens };                                   \
        bool __in = false;                                                     \
        for (size_t __i = 0; !__in && __i < array_static_length(__tokens);     \
            __i++) {                                                           \
            __in |= command_parser_token_matches((parser), __tokens[__i]);     \
        }                                                                      \
        __in;                                                                  \
    })

/** @} */

void commands_config_register(struct command_parser *parser);

void commands_core_register(struct command_parser *parser);

/** @related command
 * @{
 */

void command_init(struct command *command);

void command_cleanup(struct command *command);

/** @} */

#define command_parser_register_command_simple_x_list(m_parser, m_name,        \
    m_exec)                                                                    \
    do {                                                                       \
        struct command __cmd;                                                  \
        command_init(&__cmd);                                                  \
        __cmd.name = (m_name);                                                 \
        __cmd.execute = (m_exec);                                              \
        __cmd.cleanup = NULL;                                                  \
        command_parser_register_command(&(m_parser), &__cmd);                  \
    } while (0);

#endif

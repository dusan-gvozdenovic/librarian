/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __LIBRARIAN_DOCUMENT_H__
#define __LIBRARIAN_DOCUMENT_H__

#include "date.h"
#include "file.h"
#include "rbtree.h"
#include "ref.h"
#include "serializer.h"
#include "types.h"
#include "u-string.h"

struct document {
    struct path path;
    struct u_string title;
    struct u_string author;
    struct date create_date;
    struct rbnode tree_node;
    struct ref ref;
};

/** @related document
 * @{
 */

/**
 * Dynamically allocates a new document.
 */
struct document *document_new(void);

/**
 * Destroys dynamically allocated document.
 */
void document_destroy(struct document **doc);

declare_ref_type(document, struct document, ref, document_destroy);

/**
 * Compares two documents by author, title, path.
 *
 * @param doc1 The first document.
 * @param doc2 The second document.
 * @return -1 if doc1 is greater than doc2, 0 if they are equal and 1 otherwise.
 */
int document_compare_by_author(const struct document *doc1,
    const struct document *doc2);

/**
 * Compares two documents by date and path.
 *
 * @param doc1 The first document.
 * @param doc2 The second document.
 * @return -1 if doc1 is greater than doc2, 0 if they are equal and 1 otherwise.
 */
int document_compare_by_date(const struct document *doc1,
    const struct document *doc2);

/**
 * Compares two documents by title, author, path.
 *
 * @param doc1 The first document.
 * @param doc2 The second document.
 * @return -1 if doc1 is greater than doc2, 0 if they are equal and 1 otherwise.
 */
int document_compare_by_title(const struct document *doc1,
    const struct document *doc2);

/**
 * Serializes a document.
 *
 * @param doc Document to be serialized.
 * @param sz Serializer interface.
 * @return true on success, false otherwise.
 */
bool document_serialize(struct document *doc, struct serializer *sz);

bool document_deserialize(struct document *doc, struct serializer *sz);

/** @related document
 * @}
 */

#endif

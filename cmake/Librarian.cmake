set(LIBRARIAN_RECOMMENDED_C_FLAGS "-Wall -Werror -Wpointer-arith"
    CACHE STRING "Recommended compilation flags for the C programming language."
)
mark_as_advanced(LIBRARIAN_RECOMMENDED_C_FLAGS)

set(LIBRARIAN_RECOMMENDED_LINK_FLAGS "--gc-sections --print-gc-sections -flto"
    CACHE STRING "Recommended linker flags for the C programming language."
)
mark_as_advanced(LIBRARIAN_RECOMMENDED_LINK_FLAGS)

set(LIBRARIAN_LOCALIZATION_LIBS
    CACHE STRING "Localization libraries"
)
mark_as_advanced(LIBRARIAN_LOCALIZATION_DIRS)

set(LIBRARIAN_LOCALIZATION_DIRS
    CACHE STRING "Localization include directories."
)
mark_as_advanced(LIBRARIAN_LOCALIZATION_DIRS)

# ----------------------------- Enable clang-tidy ---------------------------- #

find_program(CLANG_TIDY clang-tidy)
find_program(IWYU include-what-you-use)

if(NOT "${CLANG_TIDY}" STREQUAL "CLANG_TIDY-NOTFOUND")
    set(CMAKE_C_CLANG_TIDY ${CLANG_TIDY})
endif()

if(NOT "${IWYU}" STREQUAL "IWYU-NOTFOUND")
    set(CMAKE_C_INCLUDE_WHAT_YOU_USE ${IWYU}
        -Xiwyu --no_fwd_decls
        -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/.iwyu-libc.imp)
endif()

set(CMAKE_LINK_WHAT_YOU_USE TRUE)

# ---------------------- Documentation and Localization ---------------------- #

if(ENABLE_LOCALIZATION)
    find_package(Intl REQUIRED)
    list(APPEND LIBRARIAN_LOCALIZATION_LIBS ${Intl_LIBRARIES})
    list(APPEND LIBRARIAN_LOCALIZATION_DIRS ${Intl_INCLUDE_DIRS})
endif()

macro(translate_files)
    if(ENABLE_LOCALIZATION)
        set(TRANSLATE_SOURCES ${TRANSLATE_SOURCES} ${ARGV} PARENT_SCOPE)
    endif()
endmacro()

macro(doxygen_include_headers)
    if(ENABLE_DOCS)
        set(LIBRARIAN_DOXYGEN_HEADERS ${LIBRARIAN_DOXYGEN_HEADERS}
            ${ARGV} PARENT_SCOPE)
    endif()
endmacro()

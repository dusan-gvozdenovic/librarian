/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __NORMAL_MODE_H__
#define __NORMAL_MODE_H__

#include <ncurses.h>

#include <librarian/library.h>
#include <librarian/observer.h>
#include <librarian/types.h>

#include "ui-events.h"

struct normal_mode {
    WINDOW *window;
    struct ui_events focused_object;
    struct observer library_observer;
};

extern struct normal_mode normal_mode;

void normal_mode_init(void);

void normal_mode_handle_resize(void);

void normal_mode_set_focus(struct ui_events events);

bool normal_mode_in_focus(struct ui_events *events);

void normal_mode_loop(void);

void normal_mode_library_update(struct library_args *args);

void normal_mode_interpret_number(int64_t number);

void normal_mode_cleanup(void);

#endif

/* SPDX-License-Identifier: Apache-2.0 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core.h"
#include "localization.h"
#include "xmalloc.h"

static void _xmalloc_fail(void);

void *xmalloc(size_t size) {
    void *mem_ptr = malloc(size);
    if (!mem_ptr) { _xmalloc_fail(); }
    return mem_ptr;
}

void *xrealloc(void *ptr, size_t size) {
    void *mem_ptr = realloc(ptr, size);
    if (!mem_ptr) { _xmalloc_fail(); }
    return mem_ptr;
}

void _xmalloc_fail(void) {
    fprintf(stderr, "%s",
        _("librarian failed to allocate requested memory. "
          "Exiting now.\n"));
    librarian_panic(ENOMEM);
}

char *xstrdup(const char *c_str) {
    char *mem_ptr = strdup(c_str);
    if (mem_ptr == NULL) {
        fprintf(stderr, "%s\n",
            _("Failed to duplicate string \"%s\". Exiting now."));
        librarian_panic(ENOMEM);
    }
    return mem_ptr;
}

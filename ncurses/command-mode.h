/* SPDX-License-Identifier: Apache-2.0 */

#ifndef __COMMAND_MODE_H__
#define __COMMAND_MODE_H__

#include <librarian/types.h>

void command_mode_init(void);

void command_mode_cleanup(void);

void command_mode_activate(void);

bool command_mode_execute_command(char *cmd_buffer);

bool command_mode_number(char *token);

#endif

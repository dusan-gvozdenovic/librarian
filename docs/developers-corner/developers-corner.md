# Developer's Corner

@note
This page is a stub. See more in the [README](../README).

Contents:
- \subpage design-philosophy "Design Philosophy"
- \subpage architecture "Architecture"
- \subpage plugin-system "Plugin System"
- \subpage library-management "Library Management"
